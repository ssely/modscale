//---------------------------------------------------------------------------
#ifndef WVK2000ControllerInterface_H
#define WVK2000ControllerInterface_H
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

    // ����������� � �������
    //-------------------------------------------------------------------------
#define    VK2000_IO_STATWEIGH_START	201
    // ��� ����� (Windows) --> ��2000 (��� ������� ����������� � �������)

#define    VK2000_IO_STATWEIGH_STOP	202
    // ��� ����� (Windows) --> ��2000 (��� �������� ����������� � �������)

#define    VK2000_IO_STATWEIGH_DONE	203
    // ��2000 --> ��� ����� (Windows) (��� ���������� ����������� � �������)

#define    VK2000_IO_STATWEIGH_ZERO	204
    // ��� ����� (Windows) --> ��2000 (��� ������� ���������)

#define    VK2000_IO_STATWEIGH_ZERO_DONE 205
    // ��2000 --> ��� ����� (Windows) (��� ���������� ���������)
    // TWZeroData

#define VK2000_IO_STATWEIGH_RESET	206
    // ��� ����� (Windows) --> ��2000 (��� ������ ����������� ����������� � �������)

#define VK2000_IO_STATWEIGH_SAVE 207
    // ��� ����� (Windows) --> ��2000 (��� ���������� ����������� ����������� � �������)
    // TMVanData

#define VK2000_IO_STATWEIGH_INIT 208
    // ��� ����� (Windows) --> ��2000 (��� ��������� ������ ��������, ��, �������, ��������� ��� ����������� � ������� ��� ��������� � �������)
    // ��������� ������������� � TWInitData

#define VK2000_IO_STATWEIGH_PARAMS 209
    // ��2000 --> ��� ����� (Windows) (��� ������� ����������� � ��������)
    // ������� ����������� � TWeighParams

#define VK2000_IO_STATWEIGH_RESULT 210
    // ��2000 --> ��� ����� (Windows) (� �������� ����������� � �������)
    // ��������� ����������� ������ � TVanWeighResult

#define VK2000_IO_STATWEIGH_VANPARAMS 211
    // ��� ����� (Windows) --> ��2000 (����� ������������ � �������)
    // ��������� ������ � TVanStatParams

#define VK2000_IO_STATWEIGH_CLOSE 220
    // ��� ����� (Windows) --> ��2000 (��� ���������� ������ ��2000 � ������ ����������� � �������)

#define VK2000_IO_STATWEIGH_DISCR 230
    // ��2000 --> ��� ����� (Windows)
    // �������� ���������� ������� TDiscrData

    //-------------------------------------------------------------------------
    // �����
    //-------------------------------------------------------------------------
#define VK2000_IO_SYSTEM_STATUS 301
    // �� --> ��� ����� (Windows) (��������� �����������)
    // wParam = 0  - ����������� �����������
    // wParam = 1  - ���������� ����������� (����������, ��������� ����������, ���������)
    // wParam = 2  - ����������� ����������
    // wParam = -1 - ������ ��� �����������

#define VK2000_IO_WEIGH_STATUS 302
    // ��2000 --> ��� ����� (Windows)
    // wParam - ��������� ���� TWeighStatus (����� 32 ����)

#define VK2000_IO_DIAGN_MESSAGE 303
    // ��2000 --> ��� ����� (Windows) (� �������� ����������� � �������� � � �������)
    // ��������� ��������� � TDiagnMessage

#define VK2000_IO_RESTART_WEIGH_MODULE 304
    // ��� ����� (Windows) --> ��2000 ���������� ������ ����������� �� ��2000

  // ���� ����������� (VK2000_IO_WEIGH_STATUS, ��������� TWeighStatus)
#define VK2000_WEIGH_PHASE_IDLE 100        // ������� �������� �� �������� (�������� ���)
#define VK2000_WEIGH_PHASE_NORM 101        // ����������
#define VK2000_WEIGH_PHASE_ZERO 102        // ���������
#define VK2000_WEIGH_PHASE_TEMP 103        // ��������� ����������
#define VK2000_WEIGH_PHASE_NORMTEMP 104        // ���������� � ��������� ����������
#define VK2000_WEIGH_PHASE_TEMPZERO 105        // ��������� ���������� � ���������
#define VK2000_WEIGH_PHASE_WEIGHING 106         // �����������

#define MAX_MCH 32
#define NAX8	8

#pragma pack(push,4)

  // ��� ������� VK2000_IO_STATWEIGH_INIT, VK2000_IO_STATWEIGH_ZERO
  typedef struct {                          // ��������� ������������� �����������
    long iWGroup;                            // ����� ������ �������� (��������, ...
                                            //                       (�������, ��� ������ ������ n iWGroup = n,..., iWUnit = 0, iSensor = 0)
    long iWUnit;                             // ����� �� (�������, ��� ������ �� n iWUnit = n,..., ��� ���� ������ = 0)
    long iSensor;                            // ����� ������� (�������, ��� ������ ������� n iSensor = n,..., ��� ���� ������ = 0)
    long iNObject;                           // ��� ������������� ������� (��������)
    long iLocPos;                            // ��������� ���������� (��������)
    long iSaveAfterStop;                     // ������� ���������� ���������� ����������� ����������� ����� ����� �������� (1)
  } TWInitData, *PWInitData;

  // ��� ������� VK2000_IO_STATWEIGH_ZERO_DONE
  typedef struct {                          // ���� ��������
    long   StatZero [MAX_MCH];               // ������� ���� � �������
    long   InitZero [MAX_MCH];               // ��������� ���� � �������
  } TWZeroData, *PWZeroData;

  // ��� ������� VK2000_IO_STATWEIGH_PARAMS -- ���� ��� �� ������������
  typedef struct {                          // ������� �����������
    long                 iScales;            // ����� �����
    long                 iWGroup;            // ����� ������ ��������
    long                 iWUnit;             // ����� ��
    long                 iSensor;            // ����� �������

    float               faTemp [12];        // ����������� ��
    float               fP1;                // ����������� ���������
    float               fQ1;                // ����������� ���������
    float               fTbase1;            // ����������� ���������
    float               fP2;                // ����������� ���������
    float               fQ2;                // ����������� ���������
    float               fTbase2;            // ����������� ���������
  } TWeighParams, *PWeighParams;

  // ��� ������� VK2000_IO_STATWEIGH_RESULT
  typedef struct {                                // ���������� ����������� ������
    unsigned long        uiTrainBWtime;            // Unix-����� ������ ����������� ������

    long                 iLocCount;                // ������� �����������
    long                 iVanCount;                // ������� �������

    long                 iObjectNum;               // � ������� � ������� �������
    long                 iObjectCode;              // ��� ������� � ������� �������

    unsigned long        uiBWtime;                 // Unix-����� ������ ����������� ������
    unsigned long        uiEWtime;                 // Unix-����� ��������� ����������� ������
    //char                caBdatetime [20];       // ����� ������ �����������
    //char                caEdatetime [20];       // ����� ��������� �����������

    unsigned long        uiBrutto;                 // ������ (��)
    unsigned long        uiBrutto_b1;              // ������, ������� ����, ��
    unsigned long        uiBrutto_b2;              // ������, ������� ����, ��
    unsigned long        uiBrutto_c1;              // ������, ������� 1 (��)
    unsigned long        uiBrutto_c2;              // ������, ������� 2 (��)
    unsigned long        uiBrutto_Ax [NAX8];       // ������ �� ��� (���� ���� ������ ������) (��) (NAX8 = 8)
    unsigned long        uiBrutto_Wheels [NAX8 * 2];// ������ �� ������ (��)
    float               fMass;                    // ����� ��� �������������

    float               fVelocity;                // ��������, > 0 - �������� ������, < 0 - �������� ����� (��/�)
    float               fAcceleration;            // ���������� (�/�2)

    float               faTemp [12];              // ����������� ��

    unsigned long        uiNaxis;                  // ����� ����
    unsigned long        uiStatus;                 // ������
  } TVanWeighResult, *PVanWeighResult;

  // ��� ������ VK2000_IO_WEIGH_STATUS
  typedef struct {                          // ��������� �����������
    unsigned long uiPhase;                   // ���� �����������
                                            // 101 - ����������
                                            // 102 - ���������
                                            // 103 - ��������� ����������
                                            // 104 - ���������� � ��������� ����������
                                            // 105 - ��������� ���������� � ���������
                                            // 106 - �����������

  } TWeighStatus, *PWeighStatus;

  // ��� ������ VK2000_IO_STATWEIGH_VANPARAMS
  typedef struct {                          // ��������� ����������� � �������
    char caInvNum [9];                      // ����� ������
    long iTare;                              // ����� ���� ������ (��)
    long iTareIndex;                         // ��� ���� (0 - �� ���������, 1 - ��������, 2 - �������)
    long iCarrying;                          // ���������������� (��)
    long iLoadNorm;                          // ����� �������� (��)
    long iHBase;                             // ������� ���� ������ (��)
  } TVanStatParams, *PVanStatParams;

  // ��� ������ VK2000_IO_DIAGN_MESSAGE
  typedef struct {                          // ��������������� ���������
    long  iCode;                             // ���
    char caMessage [1024];                  // �����
  } TDiagnMessage, *PDiagnMessage;                          //
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#pragma pack (pop)
#endif //WVK2000ControllerInterface_H


