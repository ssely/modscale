//---------------------------------------------------------------------------
#pragma pack(push, 1)
//---------------------------------------------------------------------------
// --> ����
typedef struct {                    // ������� �����
  unsigned long uiTime;              // Unix-����� �������� �������
  long iCommand;                     // �������
} TWWeighCommand, ScaleCommand, *pScaleCommand;

//<-- ����
typedef struct {                    // ���������� ����������� � ��������� �����
  unsigned long uiTime;              // Unix-����� �������� ���������

  long iBrutto;                      // ����� ������ (��)
  long iBrutto_C [2];                // ����� ������� (0 - �����, 1 - ������) (��)
  long iBrutto_B [2];                // ����� �� ������ (0 - �������, 1 - �������) (��)
  long iState;                       // ��������� �����
  long iErrorCode;                   // ��� ������
} TWWeighData, ScaleData, *pScaleData;

#define CmdStartScale 201L
#define CmdStopScale 202L
#define CmdSendZero 204L
//---------------------------------------------------------------------------
#pragma pack(pop)