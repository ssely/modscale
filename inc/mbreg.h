#ifndef MBREG_H
#define MBREG_H

#pragma pack (push,1)
typedef struct{
WORD ID; // ����� ����� 1
WORD SensorIDX[4]; // ������� �������� 2-5
WORD TimeRTU; // ����� ������ �������� 6
WORD TimeVT; // ����� ������ �����   7
WORD wMode; // ����� ������ ����� 0- ����� ���� �������� 1-������ ��������� 2-������ ��������� 8
WORD WegLimit;// ����� ��� ������������ ����� 9
WORD SensorIN; // ��������� ������� �������� 10
WORD CarCount; // ���������� ���������� �������  11
WORD AxisCNT; //����� ���������� ���� ��������� �� ����� 12
WORD AxisWEG; // ���������� ���� �� ��������� 13
WORD Dir; // ����������� �������� 14
WORD isSpeed; //�������� �������� 15
unsigned short Sign:1; // ������������� ��������� VT-200
unsigned short Motion:1; // �������� �� ���������
unsigned short isBusy:1; // ���� ������
unsigned short isLink:1; // ����� � ������ VT200;
unsigned short mbLink:1; // ����� � ������� icp-7055D
unsigned short CarOnWeg:1; // ����� �� �����
unsigned short isFirstCar:1; // 1-� ������� �� �����
unsigned short lockFirstCar:1; // ���� ����������� ������ 1-�� �������
unsigned short ReservFlag:8; // ������ 16
WORD codeRTU; // ��������� ������ � M-7055 17
DWORD tu; // ����� ������ ����������� � �������� 18-19
DWORD Weight[2]; //��������� ����� �� ���������� 20-23
DWORD WeightSUM; // ��������� ��������� ����� 24-25
DWORD CurrTime; // ������� ����� 26-27
DWORD SetTime;  // ��������� �������, ������ ��� =1 - ��������� ���� 28-29
DWORD CarWeight[2];  // ��� ������ �� �������� 30-33
DWORD CarSum; // ��� ������ 34-35
}MBINFO, *pMBINFO;
#pragma pack (pop)
#endif //MBREG_H
