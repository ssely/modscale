#include "version.h"

#define COUNTCAR 128

#pragma pack (push,1)
typedef struct{
DWORD BeginTime; // ����� ������ �����������
DWORD CarSum;   // ��� ������;
DWORD CarWeight_1,CarWeight_2;// ��� ������ �����������
DWORD Dirs; // ����������� �����������
DWORD VT; // ����� ������������ ����
DWORD Speed; // ������� ��������
WORD Error; // ������ �����������
}CARINFO, *pCARINFO;

typedef struct{
WORD ID; // ����� ����� 1
WORD TimeVT; // ����� ������ �����   7
WORD mbPort; // ���� ������ �����
WORD cmdPort; // ���� ��� ������
WORD WegLimit;// ����� ��� ������������ ����� 8
WORD SensorIN; // ��������� ������� �������� 9
WORD CarCount; // ���������� ���������� �������  10
WORD AxisCNT; //����� ���������� ���� ��������� �� ����� 11
WORD AxisWEG; // ���������� ���� �� ��������� 12
WORD Dir; // ����������� �������� 13
WORD isSpeed; //�������� �������� 14
WORD rBell; // ����� ������ �� ioLogik
WORD rSema; // ����� ������� ��������
WORD rLight; // ����� ������� ����������
BYTE LinkErrCount; //������� ������ �����
BYTE isPolling; // ���� ������ �����
BYTE Motion; // �������� �� ���������
BYTE isBusy; // ���� ������
BYTE isLink; // ����� � ������ VT200;
BYTE isWay; // ������ ������
BYTE CarOnWeg; // ����� �� �����
BYTE ZeroKey; // ��������� �����
BYTE CarKey; // ������ ������ �������� ����
BYTE lnkLogik; // ���� ����� � ioLogik
BYTE Request; // ���� ������ �� �������
BYTE ReqComplit; // ���� ��������� ��������� �������
BYTE Quit; // ���� ��������� ������ ������ �����
BYTE Bell; // ���� ������
BYTE Semaphore; // ���� ���������
BYTE Light; // ���� ���/���� ����������
BYTE ReservFlag; // ������ 15
WORD VT_Num; // ����� ������� VT200
WORD endWalk; // ����� �� ����� ������ � ���
WORD CoreNum; // ���������� ����
WORD tmrBell; // ����� ������ ������ � �������������.
DWORD tu; // ����� ������ ����������� � �������� 17-18
DWORD Weight[2]; //��������� ����� �� ���������� 19-22
DWORD WeightSUM; // ��������� ��������� ����� 23-24
DWORD StartTime; // ����� ��������� �����
DWORD SetTime;  // ��������� �������, ������ ��� =1 - ��������� ���� 27-28
DWORD CarWeight[2];  // ��� ������ �� �������� 29-32
DWORD CarSum; // ��� ������ 33
DWORD SensError; // ����� �������
DWORD mCycle; // ����� ����� �������� ���������
CHAR devName[0x20]; // ��� ����������
DWORD Reserv[0x10]; // ������
}WEGINFO, *pWEGINFO;

typedef struct{
  HANDLE hFile; // ����������  �����
  HANDLE hMapping; // ����������  ����� � ������
  size_t fsize;  // ������ ����� � ������
  PBYTE dataPtr; // ��������� �� ������ ��� ����������� ����������� �����
} FileMap, *pFileMap;

#pragma pack (pop)

#define malloc_w(m) HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (m))
#define free_w(m)  HeapFree(GetProcessHeap(),0,(LPVOID) (m))

#ifdef SCALE_C_
WEGINFO WI={0};
DWORD CarWeight,tCycle;
extern struct mg_mgr mgr;
extern int server(char *, char * );
#endif // SCALE_C_
#ifndef MISC_C
extern PVOID _createExchangeObj(int, pFileMap);
extern void _destroyExchangeObj(int, pFileMap);
extern BOOL _chkConfig(LPSTR );
extern int  _find_cmd_variable(PCHAR, PCHAR, PCHAR);
extern void _openConsole(LPSTR);
extern void _closeConsole(void);
extern void WriteLogWeb(LPSTR,int,BOOL);
extern void WriteLogLog(LPSTR,int,LPSTR, BOOL);
extern BYTE InterlockedExchange8(CHAR volatile *, CHAR );
extern SHORT InterlockedExchange16(SHORT volatile *, SHORT Value);
extern BYTE R_TRIG32(BYTE,BYTE);
extern BYTE F_TRIG32(BYTE,BYTE);
extern INT  _DeleteLog(int ,int );
extern HANDLE _chkIsRun(LPSTR);
extern BOOL isNoLogin(LPSTR);
extern int _copy_str_until(PCHAR,PCHAR,CHAR);
extern UINT16 hash_rev(PCHAR);
extern UINT16 UpDownFlag(UINT8, UINT16);
extern UINT16 TOF(UINT8,UINT32,UINT32);
extern UINT8 ScrollScreen(HANDLE,CONSOLE_SCREEN_BUFFER_INFO,int);
#endif //MISC_C
