#ifndef _udp2mat_h
#define _udp2mat_h

extern int UDP_Init(void);
extern int UDP_Close(void);
extern int UDP_Send(PCHAR,WORD,PVOID,int);
extern int UDP_OpenSend(void);
extern int UDP_OpenReceive(void);
extern int UDP_Receive(WORD,int,PVOID);
extern DWORD UDP_Receive_Timed(WORD,int,PVOID,DWORD,PWORD);
#endif //_udp2mat_h
