#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef UDPSPY_C
#define UDPSPY_C

#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    FileMap fm;
	pWEGINFO wi,pWi;
    PBYTE dataExchange;
    HANDLE isRun;
    int dataport;  // ���� ��� ��������� ������ � �����
    int cc, num,diff,i,rc;
    DWORD tout,now; // ������� ������
	time_t last,isNow;
    PCHAR pool,ip,cfg,addr;
    BOOL quit=0; // ���������� �������� ������

    cfg = (PCHAR) malloc_w(0x200);
    if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
        {
        free_w(cfg);
        return -1;
        }

    pool = (PCHAR) malloc_w(0x1000);
    GetCurrentDirectory(0x100,pool);

    lstrcat(pool,"\\");
    lstrcat(pool,cfg);
    lstrcpy(cfg,pool);

    num = GetPrivateProfileInt("scale","num",0,(LPCSTR)cfg);

    wsprintf((LPSTR)pool,"Local\\UdpSpy%03d",num);
    if((isRun = _chkIsRun((LPSTR)pool))==NULL)
    {
        free_w(pool);
        free_w(cfg);
        return -2;
    }

    dataExchange=(PBYTE)_createExchangeObj(num, &fm);
    wi=(pWEGINFO)fm.dataPtr;

    ip = (PCHAR) malloc_w(0x200);
	pWi = (pWEGINFO) malloc_w(sizeof(WEGINFO));
	isNow=last = time(NULL);
    while(!quit)
        {
        if(_chkConfig((LPSTR) cfg))
            {
            GetPrivateProfileString("udpspy","ip","",ip,0x100,(LPCSTR)cfg);
            tout = (DWORD)GetPrivateProfileInt("udpspy","timeout",5000,(LPCSTR)cfg);
            dataport = GetPrivateProfileInt("udpspy","dport",4000,(LPCSTR)cfg);
            }
		rc = memcmp((const void *)pWi,(const void *)wi,(size_t)sizeof(WEGINFO));
		isNow=time(NULL);
		if((rc!=0) || (isNow!=last))
			{
	        lstrcpy((LPSTR) pool,(LPCSTR) ip);
    	    addr = strtok(pool,",");
        	now = GetTickCount();
	        i=0;
		    cc=UDP_Init();
    		cc=UDP_OpenSend();

        	while(addr!=NULL)
            	{
	            cc=UDP_Send(addr,dataport, (PVOID) wi, sizeof(WEGINFO));
    	        addr = strtok(NULL,",");
				if(++i > 8)
					break;
            	}
		    cc=UDP_Close();
			memcpy((PVOID)pWi,(const PVOID)wi,(size_t)sizeof(WEGINFO));
			last = time(NULL);
			}

        diff = (int)(GetTickCount()-now);

        if(diff<tout)
            now = tout-(long)diff;
        else
            now = tout;

        Sleep(now);

        wsprintf((LPSTR)pool,".udpspy%02d",num);
        if((GetFileAttributes(pool)!=INVALID_FILE_ATTRIBUTES))
            {
            DeleteFile((LPCTSTR)pool);
            quit=1;
            }
        }

    cc=UDP_Close();
    CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    free_w(ip);
    free_w(pool);
    free_w(cfg);
    return 0;
}

#endif //UDPSPY
