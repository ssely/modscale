@@echo off
IF [%1] == [] GOTO usage
echo "Reset Scale %1 run as %USERNAME%, wait...."
rem psexec64.exe \\Zsmk-task-002.sib.evraz.com  -nobanner -s c:\WinApp\ScaleX%1\stoptask.cmd %COMPUTERNAME%\%USERNAME%
psexec64.exe \\Zsmk-task-002.sib.evraz.com  -nobanner -s c:\WinApp\ScaleX%1\scaleUp%1.cmd %COMPUTERNAME%\%USERNAME%
GOTO end
:usage
echo Usage: reset scale_number [01-99]
:end