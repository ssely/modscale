#include <stdio.h>
#include <stdlib.h>
#include <mem.h>
#include <windows.h>

#define malloc_w(m) HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (m))
#define free_w(m)  HeapFree(GetProcessHeap(),0,(LPVOID) (m))

int _copy_str_until(PCHAR dst,PCHAR src, CHAR chr)
{
 int i, rc=0;
        for(i=0;i<64; i++)
        {
            if((src[i]==chr) || (!src[i]))
            {
                dst[i]=0;
                rc=i;
                break;
            }
         dst[i]=src[i];
        }
return rc;
}

INT16 _scan_cmd_line(PCHAR data, PCHAR var, PCHAR value)
{
INT16 Idx=0,rc=0,sLen,i=0;
CHAR test[0x20];
    while(data[i])
    {
        if(data[i]=='/' || data[i]=='-' || data[i]=='\\')
        {
            sLen = lstrlen((LPCSTR) var);
            lstrcpyn((LPSTR) test,(LPCSTR) data+i,sLen+1);
            if(!lstrcmpi((LPCSTR) (test), (LPCSTR) var))
                {
                i+=2;
                while((data[i]==0x20) || (data[i]=='=')) i++;
                if((data[Idx]!='\'') || (data[Idx]!='\"'))
                    rc=_copy_str_until(value,data+i,' ');
                else
                    rc=_copy_str_until(value,data+i+1,data[i]);
                break;
                }
        }
        i++;
    }
return rc;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
PCHAR inFile,outFile,Buf;
HANDLE f;
INT16 rc;
DWORD fSize,fRead,bRSRC=0UL,mask=0x530040;
PDWORD sMask,pMask;
PIMAGE_DOS_HEADER dhead;
PIMAGE_NT_HEADERS64 nthead;
PIMAGE_SECTION_HEADER Section;
WORD i,w=0;
CHAR num[0x10]={0},cMask[0x10]={'@',0,'S',0,'C',0},a,b;

inFile = malloc_w(0x100);
if(w  = _scan_cmd_line((PCHAR) lpszCmdline, "-p", inFile))
    {
    for(i=0;i<w;i++)
        cMask[i<<1]=inFile[i];
    w <<=1;
    }
else
    w =6;

mask = *(PDWORD)cMask;

rc  = _scan_cmd_line((PCHAR) lpszCmdline, "-i", inFile);
    if(( f = CreateFile((LPCTSTR) inFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL))==INVALID_HANDLE_VALUE)
        {
        free_w(inFile);
        return -1;
        }
fSize = GetFileSize(f,NULL);
Buf = malloc_w(fSize <<1);
ReadFile(f,(LPVOID)Buf,fSize,(LPDWORD)&fRead,NULL);
CloseHandle((HANDLE)f);
if(fRead!=fSize)
    {
    free_w(Buf);
    free_w(inFile);
    return -2;
    }
dhead = (PIMAGE_DOS_HEADER)Buf;
nthead = (PIMAGE_NT_HEADERS64)(Buf+dhead->e_lfanew);
Section = IMAGE_FIRST_SECTION(nthead);
for(i = 0; i < nthead->FileHeader.NumberOfSections; i++)
    {
    if(!lstrcmp((LPCSTR)Section->Name,(LPCSTR)".rsrc"))
        {
        bRSRC = Section->PointerToRawData;
        break;
        }
    Section++;
    }
rc = 0;

if(bRSRC)
    {
    for(bRSRC; bRSRC < fSize-4; bRSRC++ )
        {
        sMask = (PDWORD)(Buf+bRSRC);
        if((*sMask)==mask)
            if(!memcmp((const void *)cMask,(const void *)sMask,(size_t)w))
                {
                rc=1;
                break;
                }
        }
    }

    if(rc)
        {
        rc  = _scan_cmd_line((PCHAR) lpszCmdline, "-n", num);
        num[2]=0;
        Buf[bRSRC]=' ';
        Buf[bRSRC+2]=num[0];
        Buf[bRSRC+4]=num[1];
        outFile=malloc_w(0x100);
        if(!(rc  = _scan_cmd_line((PCHAR) lpszCmdline, "-o", outFile)))
            {
            _copy_str_until(outFile,inFile,'.');
            wsprintf(inFile,"%s%s.exe",outFile,num);
            }
        else
            wsprintf(inFile,"%s",outFile);
        f = CreateFile(inFile,GENERIC_READ | GENERIC_WRITE,0,(LPSECURITY_ATTRIBUTES) NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,(HANDLE) NULL);
        if(f!=INVALID_HANDLE_VALUE)
            {
            WriteFile(f,Buf,(DWORD)fSize, &fRead,NULL);
            CloseHandle((HANDLE)f);
            }
        free_w(outFile);
        }
    
free_w(Buf);
free_w(inFile);
return 1;
}
