#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\modbus.h"
#include "..\inc\scale.h"
#include "..\inc\mbreg.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wpointer-sign"

#define VT_WEIGHT 1
#define VT_ZERO 2

#define BusyStat 8 // ����� ��������� �����
#define KeyStat 9 // �������� ������ ���������

CHAR ReqString[0x100];

//--------------

modbus_t * mb_close(modbus_t *mb)
{
if(mb!=(modbus_t *)NULL)
	{
	modbus_close(mb);
	modbus_free(mb);
	}
mb=NULL;
return mb;
}

modbus_t * mb_connect( char * ipaddr,int p,int slave)
{
modbus_t * mb;
mb=modbus_new_tcp(ipaddr, p);
	if (modbus_connect(mb) == -1)
		{
	    modbus_free(mb);
		mb=(modbus_t *)NULL;
    	return mb;
		}
modbus_set_slave(mb,slave);
return mb;
}

unsigned short _getRegister(pMBINFO wi,PWORD param)
{
return(((PBYTE)param - (PBYTE)wi)>>1);
}

int _isMove(long Summ, long limit, DWORD timeout)
{
static BOOL isFirst=TRUE;
static DWORD to;
static long Sm;
DWORD isNow;
BOOL Interval;
int cc=0;
if(isFirst)
	{
	to=GetTickCount();
	Sm=Summ;
	isFirst=FALSE;
	}
isNow =  GetTickCount()-to;
Interval = (isNow > timeout);
if(abs(Sm-Summ) > limit)
	{
	if(!Interval)
		cc=1;

	to=GetTickCount();
	Sm=Summ;
	}
return cc;
}

int _get_param(PCHAR ip_addr,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];
/*
int cmdport;    //���� ��� ������ ������ �� ������ VT220
int dataport;  // ���� ��� ��������� ������ � VT220
DWORD timeout; //������� �������� ������ � ����� � �������������
DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
BYTE num;   //����� ������������ �����
BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("scale","dataport",502,(LPCSTR)CurDir);

*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
*vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug ModBus Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ ModBus Auto �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("������� �������� �������:\t%lu ms\n", *cmdtimeout);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
return rc;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
	    {
    	    cc = i+1;
        	break;
	    }
return cc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD BusyTick,summ=0UL;
    CHAR ipaddr[0x10]; // ����� ����
    int dataport;  // ���� ��� ��������� ������ � Modbus
    DWORD cmdtimeout; //������� ������ ������ �� Modbus ���������� � �������������
    DWORD timeout; //������� �������� ������ � ����� � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i=0,ErrorLink=0,b,e;
    BYTE Idx,inMiss=0,BusyLock=0;
    FileMap fm;
	pWEGINFO wi;
	MBINFO mb;
    long weg_1,weg_2;
    PBYTE dataExchange;
    CHAR cfg[0x40];
	HANDLE isRun;
    modbus_t *ctx;
    WORD CountCar=0;
	struct tm *nt;
	time_t isNow,dbgNow;

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&dataport,&timeout,&cmdtimeout, &num, &vtnum,&debug, &quit,&limit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Local\\MbReq%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug ModBus Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� MbReq -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
wi=(pWEGINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(wi->ID!=num)
    {
    InterlockedExchange16((PWORD) &wi->ID,(WORD)num);
    }
	InterlockedExchange16((volatile PWORD) &wi->VT_Num,(WORD)vtnum);
	InterlockedExchange8((volatile PCHAR) &wi->Request,1);
lstrcpy(wi->devName,"ModBus Dev");

if((ctx=mb_connect( (char *)ipaddr,(int) dataport,(int) vtnum))==NULL)
{
    if(debug)
        printf("%lu | ����:%2u | Modbus �����:%2u | ��� ����������\n",(DWORD)time(NULL),num,vtnum);
    InterlockedExchange8((PBYTE) &wi->isLink,0);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return FALSE;
}

b = sizeof(MBINFO)>>1;
cc = modbus_read_registers(ctx,0,b,(PWORD)&mb);
wi->CarCount=mb.CarCount;

isNow=time(NULL);

F_TRIG32(KeyStat,(BYTE)0);
F_TRIG32(BusyStat,(BYTE)0);
_chkConfig((LPSTR) cfg);
while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug,&quit,&limit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug ModBus Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("ModBus �����:%s  ModBus ���� :%u\n",ipaddr,dataport);
                }
                else
					_closeConsole();
            }
        }

        b = sizeof(MBINFO)>>1;
        cc=0;
        if(ctx!=NULL)
        {
         cc = modbus_read_registers(ctx,0,b,(PWORD)&mb);
		 wi->CarWeight[0] = mb.Weight[0];
		 wi->CarWeight[1] = mb.Weight[1];
		 wi->CarSum = mb.WeightSUM;
         if(((time(NULL)-isNow) >=21UL) || (mb.isBusy < wi->isBusy))
            {
             isNow=time(NULL);
			 nt = localtime(&isNow);
             if(debug)
                printf("%02u:%02u:%02u.\tTime: %u Count: %02u-->%02u\tSum: %lu\tBusy: %u\tDir: %u%c",nt->tm_hour,nt->tm_min,nt->tm_sec,mb.TimeVT,CountCar,mb.CarCount,mb.CarSum,BusyLock,mb.Dir,0xD);
            }
        }

        if(cc!=b)
        {
            mb_close(ctx);
			ctx=mb_connect( (char *)ipaddr,(int) dataport,(int) vtnum);
			isNow=time(NULL);
			if(debug)
                printf("%lu.\tReconnect...\n",isNow);
			ErrorLink++;
        }
        else{
				ErrorLink=0;
//				cc = modbus_read_input_registers(ctx, (int) (CountCar<<1), 4, (uint16_t *) wi->Weight);
				if(mb.isBusy)
						BusyTick=time(NULL);


				if(R_TRIG32(BusyStat,(BYTE)mb.isBusy))
						CountCar=0;
				BusyLock=F_TRIG32(BusyStat,(BYTE)mb.isBusy);

// ���� ���� ������, �� ��� mb.Motion==1 (��������� �����������)
                if(mb.isBusy || BusyLock )
                    {
                    isNow=time(NULL);
	                if((mb.Motion > wi->Motion) || inMiss || BusyLock)
						{
						inMiss=0; // ���������� ��� �������� ������.
						cc = modbus_read_input_registers(ctx, (int) (CountCar<<2), 4, (uint16_t *) wi->Weight);
	                    if(debug){
								dbgNow=time(NULL);
				 				nt = localtime(&dbgNow);
    	    	                printf("\n%02u:%02u:%02u CarCount change %02u --> %02u\t",nt->tm_hour,nt->tm_min,nt->tm_sec,CountCar+1,mb.CarCount);
        	    	            printf("PL_1: %lu\tPL_2: %lu\tSUM: %lu\t",wi->Weight[0],wi->Weight[1],wi->Weight[0]+wi->Weight[1]);
            	    	        printf("Dir: %u\n",mb.Dir);
		                        }
	                	    wi->WeightSUM=wi->Weight[0]+wi->Weight[1];
	                    	wi->Dir=mb.Dir;
	    	                wi->isSpeed=mb.isSpeed;
							wi->CarCount=mb.CarCount;
							wi->ReqComplit=(BYTE)mb.CarCount;
							if(!wi->CarKey && mb.Dir){
    	    	                InterlockedExchange8((volatile PCHAR) &wi->CarKey,(CHAR)1);
								CountCar++;
								inMiss=(BYTE)(mb.CarCount>CountCar);
								}
//							if(CountCar>mb.CarCount)
							if(BusyLock)
									CountCar=0;
						}
                    }
				else{
                    wi->Weight[0]=(mb.Weight[0]>>1);
                    wi->Weight[1]=(mb.Weight[0]>>1);
                    wi->WeightSUM=mb.WeightSUM;
                    wi->Dir=mb.Dir;
                    wi->isSpeed=mb.isSpeed;
					wi->TimeVT=mb.TimeVT;
				}

        	}
		wi->Motion=mb.Motion;//���������� ��� �����������
		InterlockedExchange((volatile LONG *) &weg_1,wi->WeightSUM);
//		InterlockedExchange8((PBYTE) &wi->Motion,_isMove((long) weg_1, 10UL, 500UL));
		if(ErrorLink<5)
            InterlockedExchange8((PBYTE) &wi->isLink,1);
        else
            InterlockedExchange8((PBYTE) &wi->isLink,0);

		InterlockedExchange8((PBYTE) &wi->isBusy,mb.isBusy);
		if(wi->ZeroKey)
			InterlockedExchange8((volatile PBYTE) &wi->ZeroKey,0);

    Sleep(cmdtimeout);
	wsprintf((LPSTR)ReqString,".mbreq%02d",num);
    if((GetFileAttributes(ReqString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)ReqString);
        quit=1;
        }
	}

	if(!debug)
        _closeConsole();

	mb_close(ctx);
	InterlockedExchange8((PBYTE) &wi->isLink,0);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
