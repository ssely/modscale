#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef VTSEND_C
#define VTSEND_C

#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"

#define VT_WEIGHT 1
#define VT_ZERO 2


BYTE Query[7]= {0x2,0x41,0x3F,0x30,0x3C,0x34,0x3};
BYTE Zero[9]= {0x2,0x41,0x4B,0x30,0x33,0x30,0x3C,0x33,0x3};

WORD _getBCS(PBYTE pb,BYTE cmd)
{
    PWORD pq;
    WORD BCS;
    BYTE bcs;
    if(cmd==1)
    {
        pq=(PWORD)&pb[4];
        bcs=((pb[0] ^ pb[1])^pb[2])^pb[3];
    }
    if(cmd==2)
    {
        pq=(PWORD)&pb[6];
        bcs=((((pb[0] ^ pb[1])^pb[2])^pb[3])^pb[4])^pb[5];
    }
    BCS=(((WORD)(bcs &0xF0))<<4) | (bcs &0xF);
    BCS|=0x3030;
    *pq=BCS;
    return BCS;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD ttick,summ=0UL,isNow;
    WORD limit;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ VT220
    int dataport;  // ���� ��� ��������� ������ � VT220
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i=0,ErrorLink=0;
    BYTE Idx;
    CHAR cfg[0x40];





cc=UDP_Init();
printf("Udp Init:%s\n",cc>0 ? "Yes":"No");
cc=UDP_OpenSend();
printf("Udp Send Init:%s\n",cc>0 ? "Yes":"No");
cc=UDP_OpenReceive();
printf("Udp Receive Init:%s\n",cc>0 ? "Yes":"No");


    if(!(cc = _scan_cmd_line((PCHAR)lpszCmdline,(PCHAR) "a" , (PCHAR) cfg)))
        {
		MessageBox(NULL,"������� �������� -a ip_addr -p send_port -d receive_port -n vt_num -c cmd [reset]","Error", MB_OK);
        return FALSE;
        }
    lstrcpy(ipaddr,cfg);
    if(!(cc = _scan_cmd_line((PCHAR)lpszCmdline,"p" , (PCHAR) cfg)))
        {
		MessageBox(NULL,"������� �������� -a ip_addr -p send_port -d receive_port -n vt_num -c cmd [reset]","Error", MB_OK);
        return FALSE;
        }
    cmdport = atol(cfg);
    if(!(cc = _scan_cmd_line((PCHAR)lpszCmdline,"d" , (PCHAR) cfg)))
        {
		MessageBox(NULL,"������� �������� -a ip_addr -p send_port -d receive_port -n vt_num -c cmd [reset]","Error", MB_OK);
        return FALSE;
        }
    dataport = atol(cfg);

    if(!(cc = _scan_cmd_line((PCHAR)lpszCmdline,"n" , (PCHAR) cfg)))
        {
		MessageBox(NULL,"������� �������� -a ip_addr -p send_port -d receive_port  -n vt_num -c cmd [reset]","Error", MB_OK);
        return FALSE;
        }
    Query[1]=Zero[1]=(BYTE)atol(cfg);
//Query[1]=Zero[1]=(BYTE)vtnum;
    if(!(cc = _scan_cmd_line((PCHAR)lpszCmdline,"c" , (PCHAR) cfg)))
        {
		MessageBox(NULL,"������� �������� -a ip_addr -p send_port -d receive_port -n vt_num -c cmd [reset]","Error", MB_OK);
        return FALSE;
        }

    if(!lstrcmpi((LPCSTR) cfg,(LPCSTR)"reset"))
        {
        Query[2]='@';
        cc=_getBCS(Query,VT_WEIGHT);
        printf("Addr:%s Port:%u VT_Addr:%u Command:\'%c\'\n",ipaddr,cmdport,Query[1],Query[2]);
		for(cc=0;cc<20;cc++)
            {
			Sleep(50UL);
            Idx=UDP_Send(ipaddr,cmdport, (PVOID) Query, 7);
            }
        }
    if(!lstrcmpi((LPCSTR) cfg,(LPCSTR)"setzero"))
        {
		Zero[5]='1';
		cc=_getBCS(Zero,VT_ZERO);
		printf("Addr:%s Port:%u VT_Addr:%u Command:\'%c\'\n",ipaddr,cmdport,Zero[1],Zero[5]);
			for(cc=0;cc<20;cc++)
                {
                Sleep(50UL);
                Idx = UDP_Send(ipaddr,cmdport, (PVOID) Zero, 9);
                }

        }
Zero[5]='0';
cc=_getBCS(Zero,VT_ZERO);
Idx = UDP_Send(ipaddr,cmdport, (PVOID) Zero, 9);

Query[2]='?';
cc=_getBCS(Query,VT_WEIGHT);
printf("Receive Port:%u Command:\'%c\'\n",dataport,Query[2]);
while((i++) < 10)
    {
    cc=UDP_Send(ipaddr,cmdport, (PVOID) Query, 7);
    cc=UDP_Receive_Timed(dataport,18, (PVOID) cfg, 200,(PWORD) &limit);
    if((cc > 1))
        {
		if((cfg[5]=='+') || (cfg[5]=='-'))
			{
	        i=i*1000;
    	    break;
			}
        }
    Sleep(900UL);
    }
cc=UDP_Close();
if(cfg[0]==2)
{
    printf("TimeOut:%lums Addr:%02u Packet:",(DWORD)i,cfg[1]);
    for(i=3;;i++)
        {
        if(cfg[i]==0x3)
            {
            printf("\n");
            break;
            }
        printf("%c",cfg[i]);
        }
}
else
    printf("No answer from %u\n",Query[1]);
printf("Bye! Wait about 2 sec...\n");
Sleep(2000UL);
//while(!kbhit());
return cc;
}
#endif // VTSEND_C
