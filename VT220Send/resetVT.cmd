@echo off
SET IP=
SET CPORT=
SET DPORT=
SET VTNUM=

IF [%1] == [] GOTO USAGE

if [%1] == [70] (
SET IP=10.5.17.221
SET CPORT=4001
SET DPORT=4021
SET VTNUM=70
)
if [%1] == [1] (
SET IP=10.7.17.110
SET CPORT=4001
SET DPORT=4011
SET VTNUM=71
)
if [%1] == [9] (
SET IP=10.7.17.115
SET CPORT=4001
SET DPORT=4009
SET VTNUM=79
)
if [%1] == [32] (
SET IP=10.7.17.120
SET CPORT=4001
SET DPORT=4032
SET VTNUM=77
)
if [%1] == [46] (
SET IP=10.7.17.100
SET CPORT=4001
SET DPORT=4046
SET VTNUM=71
)

IF [%IP%] == [] GOTO USAGE

VT220Send.exe -a %IP% -p %CPORT% -d %DPORT% -n %VTNUM% -c reset
VT220Send.exe -a %IP% -p %CPORT% -d %DPORT% -n %VTNUM% -c setzero
VT220Send.exe -a %IP% -p %CPORT% -d %DPORT% -n %VTNUM% -c setzero
VT220Send.exe -a %IP% -p %CPORT% -d %DPORT% -n %VTNUM% -c setzero
GOTO END
:USAGE
@echo Usage: resetVT vt_number
:END