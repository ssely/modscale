#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define ScanKey 0 // ����� ������ ����������� ������
#define BellKey 1 // ����� ������ ��������� ������
#define SemaKey 2 // ����� ������ ��������� ��������
#define LightKey 3 // ����� ������ ��������� ����������
#define BusyKey 4 // ����� ��������� �����
#define LinkKey 5 // ����� ������� ����� � ������� ��������
#define ioLogikKey 6 // ����� ������� ����� � �������� �������
#define ZrKey 7 // ����� ��������� �����
#define WriteKey 8 // ����� ��� ������������ xml �����

CHAR LogString[0x100];


int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
    {
        cc = i+1;
        break;
    }
return cc;
}

void _setgetWeg(PDWORD w1, PDWORD w2, PDWORD wS, BOOL set)
{
static DWORD weight[3];
if(set)
	{
	weight[0] = *w1;
	weight[1] = *w2;
	weight[2] = *wS;
	}

*w1	= weight[0];
*w2	= weight[1];
*wS	= weight[2];
}


void WriteLogFile(LPSTR Buf, pWEGINFO wi, BOOL en)
{
/*
#define ScanKey 0 // ����� ������ ����������� ������
#define BellKey 1 // ����� ������ ��������� ������
#define SemaKey 2 // ����� ������ ��������� ��������
#define LightKey 3 // ����� ������ ��������� ����������
#define BusyKey 4 // ����� ��������� �����
#define LinkKey 5 // ����� ������� ����� � ������� ��������
#define ioLogikKey 6 // ����� ������� ����� � �������� �������
*/
static BYTE isFirst=1;
static DWORD WeightSUM=0UL;
DWORD dwRet,w1,w2,wS;

struct tm *nt;
time_t lt;
FILE * logFile;
// ������� ���� ������ ���������
if(!en) return;

lt =  time(NULL);
nt = localtime(&lt);

	wsprintf(Buf,"%02d/%02d%02d%04d.log",wi->ID,nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900);
	logFile=fopen(Buf,"a+");
	wsprintf(Buf,"%02d.%02d.%04d %02d:%02d:%02d ",nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec);

    if(R_TRIG32(ScanKey,(BYTE)wi->CarKey))
        {
        fprintf(logFile,Buf);
//		����� ����������� ���
		_setgetWeg(&w1,&w2,&wS,FALSE);
        fprintf(logFile,"��������.\t�����  %03d: ��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", wi->CarCount, w1,w2,wS);
        }

    if(R_TRIG32(BellKey,(BYTE)wi->Bell))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"������ ���.  ioLink:%u\t��������� 1: %06li  ��������� 2: %06li  ����� ���: %06li\n",wi->lnkLogik, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(F_TRIG32(BellKey,(BYTE)wi->Bell))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"������ ����. ioLink:%u\t��������� 1: %06li  ��������� 2: %06li  ����� ���: %06li\n",wi->lnkLogik, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }

    if(R_TRIG32(SemaKey,(BYTE)wi->Semaphore))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"�������� ���.  ioLink:%u\t��������� 1: %06li  ��������� 2: %06li  ����� ���: %06li\n",wi->lnkLogik, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(F_TRIG32(SemaKey,(BYTE)wi->Semaphore))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"�������� ����. ioLink:%u\t��������� 1: %06li  ��������� 2: %06li  ����� ���: %06li\n",wi->lnkLogik, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }

    if(R_TRIG32(LightKey,(BYTE)wi->Light))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"��������� ���.  ioLink:%u\t��������� 1: %06li  ��������� 2: %06li  ����� ���: %06li\n",wi->lnkLogik, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(F_TRIG32(LightKey,(BYTE)wi->Light))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"��������� ����. ioLink:%u\t��������� 1: %06li  ��������� 2: %06li  ����� ���: %06li\n",wi->lnkLogik, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }

    if(R_TRIG32(BusyKey,(BYTE)wi->isBusy))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"���� %02u ������.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n",wi->ID, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(F_TRIG32(BusyKey,(BYTE)wi->isBusy))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"���� %02u ��������.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n",wi->ID, wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }

    if(R_TRIG32(LinkKey,(BYTE)wi->isLink))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"����� � ������� �������� �������������.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(F_TRIG32(LinkKey,(BYTE)wi->isLink))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"���������� ����� � ������� ��������.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }

    if(R_TRIG32(ioLogikKey,(BYTE)wi->lnkLogik))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"����� � ioLogik �������������.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(F_TRIG32(ioLogikKey,(BYTE)wi->lnkLogik))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"���������� ����� � ioLogik.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }

	if(R_TRIG32(ZrKey,(BYTE)wi->ZeroKey))
        {
        fprintf(logFile,Buf);
        fprintf(logFile,"��������� �����.\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", wi->Weight[0],wi->Weight[1],wi->WeightSUM);
        }
    if(WeightSUM != wi->WeightSUM)
	{
        fprintf(logFile,Buf);
        fprintf(logFile,"��������� ����. %06li --> %06li\t��������� 1: %06li  ��������� 2: %06li  ��� ������: %06li\n", (long)WeightSUM,(long)wi->WeightSUM,(long)wi->Weight[0],(long)wi->Weight[1],(long)wi->WeightSUM);
        WeightSUM = wi->WeightSUM;
	}
	if(isFirst)
		{
		fprintf(logFile,Buf);
		isFirst=(BYTE)isNoLogin((LPSTR) Buf);
		fprintf(logFile,"������ ���������.\t%s %s\n", isFirst ? "������������ �� ���������������.\t��� ����������:" : "������������ ���������������.\t��� ������������:", Buf);
		isFirst=0;
		}
    if(Buf[0xf0]=='.')
        {
        fprintf(logFile,Buf);
		isFirst=(BYTE)isNoLogin((LPSTR) Buf);
		fprintf(logFile,"������� ���������.\t%s %s\n", isFirst ? "������������ �� ���������������.\t��� ����������:" : "������������ ���������������.\t��� ������������:", Buf);
		isFirst=0;
        }
fflush(logFile);
fclose(logFile);
}

int _get_param( PBYTE num, long *tzone, PDWORD tout, PBYTE logsave, BOOL *log, BOOL *vtemu, BOOL *dioemu,BOOL* debug, BOOL* quit, BOOL *dyn, PCHAR IniFile)
{
static BOOL isFirst=FALSE;
PCHAR CurDir;
int rc=0,cc;
CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);

*num=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);

*tzone=(long)GetPrivateProfileInt("logic","tzone",0,(LPCSTR)CurDir)*3600L;
*tout=(DWORD)GetPrivateProfileInt("logic","timeout",0,(LPCSTR)CurDir)*60UL;
*logsave=(BYTE)GetPrivateProfileInt("logic","logsave",0,(LPCSTR)CurDir);
*log=(BOOL)GetPrivateProfileInt("logic","logfile",0,(LPCSTR)CurDir);
*vtemu=(BOOL)GetPrivateProfileInt("logic","VTEmulation",0,(LPCSTR)CurDir);
*dioemu=(BOOL)GetPrivateProfileInt("logic","DIOEmulation",0,(LPCSTR)CurDir);
*dyn=(BOOL)GetPrivateProfileInt("logic","dynamics",0,(LPCSTR)CurDir);

cc=GetPrivateProfileInt("logic","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("logic","exit",0,(LPCSTR)CurDir);

isFirst=isNoLogin((LPSTR) LogString);
if(!isFirst)
{
	wsprintf(LogString,"Debug Logic Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) LogString );
	printf("������������ Logic �� �����: %s\n",(PCHAR)CurDir);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("������ � ���-����:\t\t%s\n", *log ? "���" : "����");
	printf("�������� ������������ ��������:\t%+ld\n", *tzone / 3600L);
	printf("��������� �������� �����:\t%ld ���.\n", *tout / 60L);
	printf("�������� ������:\t\t%d ���.\n", (int)*logsave);
	printf("�������� �������� �������:\t%s\n", *vtemu ? "���" : "����");
	printf("�������� ������ �����-������:\t%s\n", *dioemu ? "���" : "����");
	printf("������������ �����:\t\t%s\n", *dyn ? "���" : "����");
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
}
if(!(isFirst=*debug))
		_closeConsole();

free_w(CurDir);
return rc;
}

int UpDownFlag(int dst, int src)
{
    if(src > dst) return 1;
    if(src < dst) return -1;
return 0;
}

int _makeProtocol(pWEGINFO dst,pWEGINFO src,long zone,time_t t_out, BOOL dy)
{
static CHAR pFile[0x80]={0};
static time_t lt=0UL;
static int CarNum=0,ZeroNum=0;
FILE * fh=NULL;
struct tm *nt;
long fPos;
DWORD w1,w2,wS;
int cc;
WORD Err=0;


//if(UpDownFlag((int)dst->CarKey,(int)src->CarKey)>0)
  if(R_TRIG32(WriteKey,(BYTE)src->CarKey))
    {
        lt=time(NULL);
        nt = localtime(&lt);
        if(!CarNum)
            {
            wsprintf(pFile,"%02d/%02d%02u%02u%02u%02u.xml",src->ID,nt->tm_year-100,nt->tm_mon+1,nt->tm_mday,nt->tm_hour,nt->tm_min);
            fh=fopen(pFile,"w+t");
#ifdef NEWLOG
            fprintf(fh,"<?xml version=\"1.0\" ?><RailWay_Feed ID=\"%02u%02u%02u%02u%02u\">\r\n",nt->tm_year-100,nt->tm_mon+1,nt->tm_mday,nt->tm_hour,nt->tm_min); //(DWORD)(lt+zone));
#else
			fprintf(fh,"<?xml version=\"1.0\" ?><RailWay_Feed ID=\"%lu\">\r\n",(DWORD)(lt+zone));
#endif
            fflush(fh);
            fclose(fh);
            }
        fh=fopen(pFile,"r+t");
        if(CarNum)
            cc=fseek(fh, -15L, SEEK_END);
        else
            cc=fseek(fh, 0L, SEEK_END);
// ����������  ����������� ���
		InterlockedExchange((volatile unsigned long *) &w1,(DWORD)src->Weight[0]);
		InterlockedExchange((volatile unsigned long *) &w2,(DWORD)src->Weight[1]);
		InterlockedExchange((volatile unsigned long *) &wS,(DWORD)src->WeightSUM);
   		_setgetWeg(&w1,&w2,&wS,TRUE);

        fprintf(fh,"<Vagon N=\"%d\" Number=\"%d\" DateTimeStamp=\"%02d.%02d.%04d %02d:%02d:%02d\" ",CarNum+1,CarNum+1,nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec);
		fprintf(fh,"VesTime=\"%lu\" VideoTime=\"%lu\" Ves=\"%lu.%03lu\" ",(unsigned long)(lt+zone),(unsigned long)(lt+zone),wS/1000,wS%1000);
		fprintf(fh,"Ves1=\"%ld\" ",(long)w1);
		fprintf(fh,"Ves2=\"%ld\" ",(long)w2);

		if(!dy)
			fprintf(fh,"Error=\"%d\" Speed=\"%lu.%lu\" Direct=\"%d\" TW=\"%lu\"/>\r\n",0,0/10UL,0%10UL,0,0UL);
		else{
			if(src->isSpeed > 35)
				Err|=0x200;
			fprintf(fh,"Error=\"%d\" Speed=\"%lu.%lu\" Direct=\"%d\" TW=\"%u\"/>\r\n",Err,src->isSpeed/10UL,src->isSpeed%10UL,src->Dir-1,(short)src->ReqComplit);
			}
        fprintf(fh,"</RailWay_Feed>");
        fflush(fh);
        fclose(fh);
        CarNum++;
        if(!dst->isBusy)
            ZeroNum++;
    }
if(((time(NULL)-lt) > t_out) && (!dst->isBusy))
        {
        if(CarNum)
            {
            if(CarNum == ZeroNum)
                {
                  if(pFile[0])
                      DeleteFile((LPCTSTR)pFile);
                }
            pFile[0]=0;
            CarNum=ZeroNum=0;
            }
        }
return CarNum;
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    DWORD ttick,summ=0UL,isNow,dwRet;
    DWORD tmout; //������� (� �������) ����� �������� ������� ������ �����������.
	long tzone; // �������� (� �����) ��������� ���� ������������ ��������(UTC)
    BYTE num;   //����� ������������ �����
    BYTE logsave; //����� (� �������) �������� ��� ������ � ������ �������� �� �������.
	BOOL isLog; // 0/1 ���/���� ������� ��� �����
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL VTEmu=FALSE,DIOEmu=FALSE; // �������� �������� ������� � ������ �����-������
	BOOL quit=0; // ���������� �������� ������
	BOOL dyn=0; // ����� ����������� � ��������
    HANDLE isRun;
    int cc,wait=0;;
    WORD Idx=0,CarNum;
    FileMap fm;
    pWEGINFO pWi;
	WEGINFO wi;
    PBYTE dataExchange;
    CHAR cfg[0x20];


if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    {
    return FALSE;
    }

cc=_get_param(&num,&tzone,&tmout,&logsave,&isLog,&VTEmu,&DIOEmu,&debug,&quit,&dyn,cfg);

wsprintf((LPSTR)LogString,"Local\\Logic%03d",num);
if((isRun = _chkIsRun((LPSTR)LogString))==NULL)
    {
		wsprintf(LogString,"Debug Logic Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) LogString );
        printf("��������� Logic -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pWi=(pWEGINFO)fm.dataPtr;

if(pWi->ID!=num)
	InterlockedExchange16((volatile SHORT *) &pWi->ID,(SHORT)num);

// ������ ������� ��� ������� � Web �������
	wsprintf(LogString,".\\%02d",num);
	if((GetFileAttributes(LogString)==INVALID_FILE_ATTRIBUTES))
			CreateDirectory(LogString, NULL);


    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param(&num,&tzone,&tmout,&logsave,&isLog,&VTEmu,&DIOEmu,&debug,&quit,&dyn,cfg);
            if(cc & 0x2){
                if(debug ){
					sprintf(LogString,"Debug Logic Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) LogString );
                    printf("���� �����:%u\t������ ��� �����:%u\n",num,(WORD)isLog);
                }
                else
					_closeConsole();
            }
        }
        CarNum=_makeProtocol(&wi,pWi,(long) tzone,(time_t) tmout, dyn);
        InterlockedExchange16((volatile SHORT *) &pWi->CarCount,(SHORT)CarNum);

		memmove((void *) &wi, (const void *) pWi, (size_t) sizeof(wi));
		WriteLogFile((LPSTR) LogString, (pWEGINFO) &wi, isLog);

// ������ ������� ������ ���������� 1 ���, ����� ����������
        if(wi.CarKey)
			{
			if((++wait) > 10)
				{
                InterlockedExchange8((volatile PCHAR) &pWi->CarKey,(CHAR)0);
				wait=0;
				}
             }

		if(!Idx)
			ttick=GetTickCount();

		if((Idx++)>1000)
		{
			summ=(GetTickCount()-ttick) / 1000UL;
			InterlockedExchange((volatile long *) &pWi->mCycle,(long)summ);
			Idx=0;
		}
		isNow=time(NULL);

			if(debug && (Idx==500))
                printf("%lu | ����:%02u | ���������:%+07li | %+07li | %08li | ���� ���������: %u ms\n",isNow,num,pWi->Weight[0],pWi->Weight[1],(long)pWi->WeightSUM,(int)summ);

		_DeleteLog((int) logsave,(int) num);

		if((!pWi->isLink) && (VTEmu))
                InterlockedExchange8((volatile PCHAR) &pWi->isLink,(CHAR)1);

		if((!pWi->lnkLogik) && (DIOEmu))
                InterlockedExchange8((volatile PCHAR) &pWi->lnkLogik,(CHAR)1);
// ���������� ������� ������ ���� DIO �� ���������� ��� ��������
		if(pWi->Bell && DIOEmu)
				InterlockedExchange8((volatile PCHAR) &pWi->Bell,(CHAR)0);	
        Sleep(100UL);
        // ������ ������� ��� ������� � Web �������
	wsprintf(LogString,".quit%02d",num);
	if((GetFileAttributes(LogString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)LogString);
        LogString[0xf0]='.';
        WriteLogFile((LPSTR) LogString, (pWEGINFO) &wi, isLog);
        quit=1;
        }
    }
if(!debug)
	_closeConsole();

	InterlockedExchange((volatile long *) &pWi->mCycle,(long) 0UL);
    InterlockedExchange16((volatile SHORT *) &pWi->CarCount,0);

    if(VTEmu)
        InterlockedExchange8((volatile PCHAR) &pWi->isLink,(CHAR)0);
    if(DIOEmu)
        InterlockedExchange8((volatile PCHAR) &pWi->lnkLogik,(CHAR)0);
    CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
