///
/// <meta lang="ru" charset="windows-1251" emacsmode='-*- markdown -*-'>
///

#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "..\inc\MQTTClient.h"
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"


#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#define HASHSIZE 1023

/// # ������
///
/// ������ ������ ������������ ��� ������ � �������� �������� ��-24/VT220
/// �� Ethernet ���������� � ������� �������.
/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// ����������� � �� �������������� �� �� �����
///
/// ************************************************************************
/// *                            .-.
/// *                         .-+   |
/// *                     .--+       '---.                   .-----------.
/// *                    | ���� ��������� |                 | ��-24/VT220 |
/// *                     '--------------'                   '-----------'
/// *                          ^   ^                               ^
/// *                          |   |                               |
/// *  .------.   Ethernet     |   | Ethernet                      |
/// *  |  ��  |<--------------'	    '------------------------------'
/// *  '------'
/// *
/// *
/// ************************************************************************


/// ## �������������
///
/// ������ ������ ������������ �� ��������� ������
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
///  We21R64X.exe -conf=���_ini_�����
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// � __ini__ ����� ������ ������������ ������ __[scale]__.
/// ������ ������ ������� ����:
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// [scale]
/// ip="10.9.2.100"
/// dataport=8234
/// cmdport=8232
/// timeout=2000
/// cmdtimeout=100
/// pktlen=39
/// num=1
/// vtnum=1
/// limit=1000
/// dbgtime=1
/// debug=1
/// exit=0
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
///
/// �������� � ������ | ��������
/// ------------------|--------------------------------------------------------------
/// __ip__            | ����� �������� ��������� ��-24/VT220
/// __dataport__      | UDP ���� ����� ������� ������� ��� � ��
/// __cmdport__       | UDP ���� ����� ������� ���������� ������� � �������� �������
/// __timeout__       | ������� �������� ������ �� ���������� ��������� � �������������
/// __cmdtimeout__    | ������� ������ ������ �� ������� ������ � �������������
/// __pktlen__        | ����� ������ ������ �������� ������� ��-24/VT220
/// __num__           | ����� ������������ �����
/// __vtnum__         | ����� ������� ��-24/VT220 ��� ������ �� �������
/// __limit__         | ��� (��) ��� ���������� �������� ���� ��������� ��������.
/// __dbgtime__       | �������� ������ (���) ���������� ���������� ���� ���������� ���� debug
/// __debug__         | ������/������� (1/0) ����������� ����
/// __exit__          | 1 - ���������� ������ ������
///
/// !!! Tip
///     ��� �������� ��������� ��-24/VT220 ���� ��� ������� � ������, � ������� __XXX.XXX__ &mdash; ����� ������ __37 ����__.<br/>
///     ���� ��� ������� � �����������, � ������� __XXXXXX__, �� ����� ������ __39 ����__
///
///
CHAR ReqString[0x100];
BYTE num;   //����� ������������ �����
BYTE _z;    //���� ��������� �������
BOOL quit=0; // ���������� �������� ������


/// ## __�������__
///
///
/// ### _������� �������������� ������ �� ������� ��-24/VT220_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// long _str2long(PBYTE Buffer,PINT16 cnt)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|-------------------------------------------------------------------------------------
/// __Buffer__  | ��������� �� ����� ������ �� ������� ��-24/VT220
/// __cnt__     | ��������� �� ���������� ������������ ��������
///
/// __����������__: ��������� ������� ��-24/VT220 � ���� �������� ������
///

long _str2long(PBYTE Buffer,PINT16 cnt)
{
    long rc=0, pw=10,sign=1;
    int i=0;
    char cc;
    *cnt=0;
    while((Buffer[i]!=',') && (Buffer[i]!=0xD) && (Buffer[i]!=0xA) && (Buffer[i]))
    {
        cc=Buffer[i++];
        (*cnt)++;
        if(cc == '-')
        {
            sign*=-1L;
            continue;
        }
        if(isdigit(cc))
        {
            rc*=pw;
            rc+=(cc-0x30);
        }
    }
    return (rc*sign);
}

///
/// ### _������� ���������� ���������� ��������� ������� ��-24/VT220 �� ini �����_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PINT16 pktlen, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim, PDWORD dt,PCHAR IniFile)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// �������� � ������ | ��������
/// ------------------|--------------------------------------------------------------
/// __ip_addr__       | ����� �������� ��������� ��-24/VT220
/// __cmdport__       | UDP ���� ����� ������� ���������� ������� � �������� �������
/// __dataport__      | UDP ���� ����� ������� ������� ��� � ��
/// __timeout__       | ������� �������� ������ � ����� � �������������
/// __cmdtimeout__    | ������� ������ ������ �� ������� ������ � �������������
/// __pktlen__        | ����� ������ ������ �������� ������� ��-24/VT220
/// __num__           | ����� ������������ �����
/// __vtnum__         | ����� ������� ��-24/VT220 ��� ������ �� �������
/// __debug__         | ������/������� (1/0) ����������� ����
/// __exit__          | 1 - ���������� ������ ������
/// __lim__           | ��� (��) ��� ���������� �������� ���� ��������� ������.
/// __dt__            | �������� ������ (���) ���������� ���������� ���� ���������� ���� debug
/// __IniFile__       | ��� ini ����� ��� ��������� �����
///
/// __���������__ ������������� ���������� ���������� �� _ini_ �����, _��. &sect;1.1_
///

int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PINT16 pktlen, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim,PDWORD dt,PCHAR IniFile)
{
    CHAR ip[0x10]= {0}; // ip ����� ����
    CHAR CurDir[0x100];

    static BOOL isFirst=FALSE;
    int rc=0,cc;

    GetCurrentDirectory(0x100,CurDir);
    lstrcat((LPSTR)CurDir,"\\");
    lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
    GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
    *dataport=GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
    *cmdport=GetPrivateProfileInt("scale","cmdport",8001,(LPCSTR)CurDir);
    *timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
    *cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
    *pktlen=(INT16)GetPrivateProfileInt("scale","pktlen",37,(LPCSTR)CurDir);
    *num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
    *vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
    *lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
    *dt=(DWORD)GetPrivateProfileInt("scale","dbgtime",5,(LPCSTR)CurDir);
    cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
    if(*debug != (BOOL)cc)
    {
        rc|=2;
        *debug = (BOOL)cc;
    }
    if(!isFirst)
    {
        wsprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
        _openConsole((LPSTR) ReqString );
        printf("������������ VT220 Auto �� �����: %s\n",(PCHAR)CurDir);
        printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
        printf("���� ������:\t\t\t%d\n",(INT) *dataport);
        printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
        printf("������� �������� ������:\t%lu ms\n", *timeout);
        printf("������� ����� ���������:\t%lu ms\n", *cmdtimeout);
        printf("����� ������ ������:\t\t%u ����\n", *pktlen);
        printf("����� �����:\t\t\t%03d\n",(INT) *num);
        printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
        printf("����� ���������:\t\t%lu ��.\n", *lim);
        printf("�������� ������ ���������� ���:\t%lu ���.\n", *dt);
        printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
        Sleep(5000UL);

    }

    if(!(isFirst=*debug))
        _closeConsole();

    *quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
    return rc;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
    int i,cc=0;
    for(i=0; i<36; i++)
        if(Buff[i]==chr)
        {
            cc = i+1;
            break;
        }
    return cc;
}

///
/// ### _������� �������������� ������ �� ������� ��-24/VT220_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// INT16 _pv_request(PCHAR ip,UINT16 wenum,DWORD cmd,WORD lst,DWORD tout,INT16 pkt,long *w0,long *w1,long *w2, PBYTE mv, PBYTE zero)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|-------------------------------------------------------------------------------------
/// __ip__      | ����� �������� ��������� ��-24/VT220
/// __wenum__   | ����� ������� ��-24/VT220
/// __cmd__     | ����� UDP ����� ��� ������ ������ �� ������ ��-24/VT220
/// __lst__     | ����� UDP ����� ��� ��������� ������ � ������� ��-24/VT220
/// __tout__    | ������� ��������� ������ �� ������� ��-24/VT220 � ms
/// __pkt__     | ����� ������ �� ������� ��-24/VT220 � ������
/// __w0__      | ��������� �� �������� ���������� ���� �� ����������
/// __w1__      | ��������� �� �������� ���� �� ��������� �1
/// __w2__      | ��������� �� �������� ���� �� ��������� �2
/// __mv__      | ��������� �� ���� motion
/// __zero__    | ��������� �� �������� ����� ��������� ��������� ������� ��-24/VT220
///
/// __����������__: ������������� �������� ��� ���������� ����� � �������� ��-24/VT220 � ������������� �������� � ������ ��������� ������ �� ������� ��-24/VT220
///
/// !!! Tip
///     � ���������� _w0,w1,w2_ ������������, ��������������: ����� ���, ��� �� ��������� �1 � ��� �� ��������� �2.
///

INT16 _pv_request(PCHAR ip,UINT16 wenum,DWORD cmd,WORD lst,DWORD tout,INT16 pkt,long *w0,long *w1,long *w2, PBYTE mv, PBYTE zero)
{
    PCHAR _cmd,_answ;
    INT16 cc=0,len,rc,slen,cnt=0;

    _cmd = (PCHAR) malloc_w(0x100);
    _answ = (PCHAR) malloc_w(0x100);
    if(*zero)
    {
        slen = wsprintf(_cmd,"S%02u;CDL;",wenum);
        *zero=0;
        /*for(rc=0; rc < 5;rc++)
            {
            UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);
            rc = (INT16) UDP_Receive_Timed(lst,3, (PVOID) _answ, tout,(PWORD) &len);
            if(_answ[0]==0x30)
        		{
        		*zero=0;
        		cc+=rc;
                break;
        		}
            if(_answ[0]==0x32) // ��� ���������
                {
                cc+=rc;
                break;
                }

            }*/
        /*
        slen = wsprintf(_cmd,"S%02u;BUT2;",wenum);
        UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);
        cc += (INT16) UDP_Receive_Timed(lst,3, (PVOID) _answ, tout,(PWORD) &len);
        *zero=0;
        */
    }
//       slen = wsprintf(_cmd,"S%02u;BUT2;",wenum);
//       UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);

    slen = wsprintf(_cmd,"S%02u;MSV?1,2;",wenum);
    UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);
    cc += (INT16) UDP_Receive_Timed(lst,(int) pkt, (PVOID) _answ, tout,(PWORD) &len);
    if((cc>0) && (_answ[8]==',') && (len) && (_answ[len-1]==0xA))
    {
        *w0 = _str2long((PBYTE)_answ,&cnt);
        rc=cnt+1;
        _str2long((PBYTE)_answ+rc,&cnt);
        rc+=(cnt+1);
        // �������� bit_1 - ���������� ����� � ����������� ��� �������� motion
        *mv = (BYTE)((((_str2long((PBYTE)_answ+rc,&cnt)) &2) ^ 2) >0);
        // ___________________________________________________________________
        rc+=(cnt+1);
        *w1 = _str2long((PBYTE)_answ+rc,&cnt);
        rc+=(cnt+1);
        *w2 = _str2long((PBYTE)_answ+rc,&cnt);
        if(labs(*w1)>labs(*w0))
            *w1=(*w1+5)/10L;
        if(labs(*w2)>labs(*w0))
            *w2=(*w2+5)/10L;
    }
    else
        cc = -1;
    free_w(_answ);
    free_w(_cmd);
    return cc;
}

UINT16 hash(PCHAR s)
{
    UINT16 hashval;

    for (hashval = 0; *s != '\0'; s++)
        hashval = *s + 31 * hashval;

    return hashval % HASHSIZE;
}
UINT16 hash_rev(PCHAR s)
{
    UINT16 hashval;
    UINT16 len;
    len=(UINT16)lstrlen((LPCSTR) s)-1;
    for (hashval = 0; s[len] != '/'; len--)
    {
        hashval = s[len] + 31 * hashval;
        if(!len)
            break;
    }
    return hashval % HASHSIZE;
}

UINT16 _mk_mqtt(MQTTClient  mqc, MQTTClient_message pmsg, BYTE num,PCHAR name, PVOID val, UINT16 q )
{
    UINT16 mc=0;
    int len;
    UINT16 hval;
    PCHAR top,payload;

    top = malloc_w(0x100);
    payload = malloc_w(0x100);
    wsprintf(top,"/Scale%02u/%s",num,name);
    hval = hash_rev(name);
    switch(hval)
    {
    case 410: // Motion
    case 281: // ZeroKey
    case 822: //Busy
        len = wsprintf(payload,"%u",*((PUINT8)val));
        break; //Motion, Busy, ZeroKey
    case 807: //LostConnect
        len = wsprintf(payload,"%u",*((PINT16)val));
        break; // LostConnect
    case 426: // PlatformSum
    case 336: // Platform_1
    case 587: // Platform_2
    case 468: // Sum_12
        len = wsprintf(payload,"%ld",*((PINT32)val));
        break; // PlatformSum,Platform_1,Platform_2
    case 166: //ErrorLinkVT
    case 343:
        len = wsprintf(payload,"%u",*((PUINT16)val));
        break; //TimeVT, ErrorLinkVT
    default:
        len=0;
    }
    if(len)
    {
        len=lstrlen(payload);
        pmsg.payload = payload;
        pmsg.payloadlen=(int)len;
        pmsg.qos = q;
        pmsg.retained = 0;
        mc = MQTTClient_publishMessage(mqc, top, &pmsg, NULL);
        //mc = MQTTClient_waitForCompletion(mqc, (MQTTClient_deliveryToken) 0, 5UL);
    }
    free_w(payload);
    free_w(top);
    return mc;
}

BOOL _blink(DWORD High,DWORD Low, BOOL init)
{
static DWORD H=100UL,L=100UL,eH,eL,isNow;

isNow=GetTickCount();
    if(init)
    {
        H=High;
        L=Low;
        eH=isNow+H;
        eL=eH+L;
        return FALSE;
    }
    if(isNow <= eH)
        return TRUE;
    if((isNow > eH) && (isNow < eL))
        return FALSE;
    if(isNow>=eL)
    {
        eH=isNow+H;
        eL=eH+L;
        return FALSE;
    }
return FALSE;
}
/*
void subst_callback(void** unused, struct mqtt_response_publish *published)
{

    PCHAR topic_name,value;
    UINT16 hval;
    topic_name = (PCHAR) malloc_w(published->topic_name_size + 1);
    memcpy(topic_name, published->topic_name, published->topic_name_size);
    topic_name[published->topic_name_size] = '\0';
    hval = hash_rev((PCHAR)topic_name);
    value = (PCHAR)published->application_message;
    value[published->application_message_size]=0;
    switch(hval)
    {
    case 281:
        _z=value[0]-0x30;
        break; //ZeroKey
    case 965:
        quit=(BOOL )value[0]-0x30;
        break;// Quit ���������� �������� ������
//Quit
    }
    printf("Received publish('%s'): %s\n", topic_name, (const char*) published->application_message);

    free_w(topic_name);
}
*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    //DWORD isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ VT220
    int dataport;  // ���� ��� ��������� ������ � VT220
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
    DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    INT16 pk; // ����� ������ ������
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    DWORD dbgtime; // �������� ������ ���������� ����������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����

    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    PBYTE sendbuf;
    PBYTE recvbuf;
    const char* client_id;
    UINT16 qos = 0;



    INT16 cc,pcc,rc;
    UINT16 ErrorLink=0,pErrorLink,Err=0;
    DWORD laps,pubs;
    int state;
    SYSTEMTIME lt;
    time_t isNow, isRelease;
    BYTE _mv;
    UINT32 _cnt=0,_sum=0;
    FileMap fm;
    pWEGINFO wi;
    long value[3],pvalue[3]= {0L};

    PBYTE dataExchange;
    CHAR cfg[0x40];
    HANDLE isRun;
    BOOL isLock=FALSE,setZero=FALSE,setPub;




    if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf", (PCHAR) cfg)))
        return FALSE;


    cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &pk, &num, &vtnum,&debug, &quit,&limit,&dbgtime,cfg);
    debug=0;

    wsprintf((LPSTR)ReqString,"Local\\VT220Req%03d",num);
    if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
        wsprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
        _openConsole((LPSTR) ReqString );
        printf("��������� Pv24R64X -conf=%s  ��� ��������. �������...",cfg);
        Sleep(5000UL);
        _closeConsole();
        return FALSE;
    }

    if(debug)
        printf("Mutex %s is open\n",ReqString);

    dataExchange=(PBYTE)_createExchangeObj(num, &fm);
    wi=(pWEGINFO)fm.dataPtr;

    if(debug)
        printf("Exchange Object is open:%p\n",dataExchange);

    if(wi->ID!=num)
        InterlockedExchange16((SHORT volatile *) &wi->ID,(SHORT)num);

    lstrcpy(wi->devName,"��24/VT220");

    cc=UDP_Init();
    if(debug)
        printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
    if(debug)
        printf("Udp Send Init:%d\n",cc);

    cc=UDP_OpenReceive();
    if(debug)
        printf("Udp Reseive Init:%d\n",cc);

    InterlockedExchange16((SHORT volatile *) &wi->VT_Num,(WORD)vtnum);
    InterlockedExchange8((CHAR volatile *) &wi->Request,1);

    _pv_request(ipaddr,num,cmdport,dataport,timeout,pk,&value[0],&value[1],&value[2], &_mv, &_z);

    sendbuf = malloc_w(0x100000);
    recvbuf = malloc_w(0x100000);
    client_id = malloc_w(0x100);
    wsprintf((LPSTR)client_id,"MQTT_Publisher_%03u",num);

    laps = 0UL;
    pubs=GetTickCount()+2000L;

    if((rc = MQTTClient_create(&client, "mqtt://10.1.0.238:1883",client_id,MQTTCLIENT_PERSISTENCE_NONE, NULL))== MQTTCLIENT_SUCCESS)
    {
            conn_opts.keepAliveInterval = 10;
            conn_opts.cleansession = 1;
            conn_opts.connectTimeout=2;
            rc = MQTTClient_connect(client, &conn_opts);
            rc = _mk_mqtt(client, pubmsg, num,"LostMQTT", (PVOID) &Err, qos );
    }
    _blink(200UL,800UL, TRUE);
    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg))
        {
            cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &pk, &num, &vtnum,&debug,&quit,&limit,&dbgtime,cfg);
            if(cc & 0x2)
            {
                if(debug)
                {
                    wsprintf(ReqString,"Debug WE2110 Request Console: %s",SC_UBUNTU_FULLVERSION);
                    _openConsole((LPSTR) ReqString );
                    printf("��24/VT220 �����:%s ��24/VT220 ���� ������:%u ��24/VT220 ��������� ����:%u\n",ipaddr,dataport,cmdport);
                }
                else
                    _closeConsole();
            }
        }

//        InterlockedExchange8((CHAR volatile *) &_z,wi->ZeroKey);
        setZero=(BOOL)(_z>0);
        setPub = (BOOL)(GetTickCount() > pubs);
        if((cc = _pv_request(ipaddr,num,cmdport,dataport,timeout,pk,&value[0],&value[1],&value[2], &_mv, &_z))>0)
        {
            InterlockedExchange((volatile LONG *) &wi->WeightSUM,(long)value[0]);
            InterlockedExchange((volatile LONG *) &wi->Weight[0],(long)value[1]);
            InterlockedExchange((volatile LONG *) &wi->Weight[1],(long)value[2]);
            InterlockedExchange8((CHAR volatile *) &wi->Motion,_mv);
            InterlockedExchange16((SHORT volatile *) &wi->TimeVT,(WORD)cc);

            rc = _mk_mqtt(client, pubmsg, num,"PlatformSum", (PVOID) &value[0], qos );
            rc = _mk_mqtt(client, pubmsg, num,"Platform_1", (PVOID) &value[1], qos );
            rc = _mk_mqtt(client, pubmsg, num,"Platform_2", (PVOID) &value[2], qos );
            pvalue[0]=value[1]+value[2];
            rc = _mk_mqtt(client, pubmsg, num,"Sum_12", (PVOID) &pvalue[0], qos );
            rc = _mk_mqtt(client, pubmsg, num,"Motion", (PVOID) &_mv, qos );

            if((!_z) && setZero)
            {
                InterlockedExchange8((CHAR volatile *) &wi->ZeroKey,0);
            }

            ErrorLink=0;
            _sum+=cc;
            _cnt++;
        }
            else{
                ErrorLink++;
                rc = _mk_mqtt(client, pubmsg, num,"ErrorLinkVT", (PVOID) &ErrorLink, qos );
            }

            InterlockedExchange8((CHAR volatile *) &wi->isLink,(BYTE)(ErrorLink<10));

// ������ � ���������� ���� ���� ���������� ���� debug
            if(dbgtime && debug)
            {
                isNow=time(NULL);
                if(!(isNow % dbgtime))
                {
                    if(!isLock)
                    {
                        GetLocalTime(&lt);
                        printf("%02u:%02u:%02u | ����:%2u | ������:%2u | ���������:%05ld:%05ld:%05ld:%u | ������� ������: %d ms| ���-�� ������� � ���: %lu\n",lt.wHour,lt.wMinute,lt.wSecond,num,vtnum,value[0],value[1],value[2],_mv,_cnt ? (INT16)(_sum/_cnt): -1,_cnt/dbgtime);
                        isRelease=isNow;
                        _cnt=_sum=0;
                    }
                }
                isLock = (BOOL) (isNow == isRelease);
            }
//_______________________________________________________

            if((long)wi->WeightSUM > (long)limit)
                InterlockedExchange8((CHAR volatile *) &wi->isBusy,1);
            else
                InterlockedExchange8((CHAR volatile *) &wi->isBusy,0);


// ��������� ������� ��� ���� � 2 ���
            if(!wi->CarKey)
            {
                if(cmdtimeout>cc)
                    Sleep(cmdtimeout-cc);
                else
                    Sleep(cmdtimeout);
            }
            else
            {
                Sleep(2000UL);
                InterlockedExchange8((CHAR volatile *) &wi->CarKey,0);
            }

            if(_blink(0UL,0UL, FALSE))
                {
                rc = _mk_mqtt(client, pubmsg, num,"TimeVT", (PVOID) &cc, qos );
                rc = _mk_mqtt(client, pubmsg, num,"ErrorLinkVT", (PVOID) &ErrorLink, qos );
                rc = _mk_mqtt(client, pubmsg, num,"Busy", (PVOID) &wi->isBusy, qos );
                }
//_______________________________________________
// ����� ��. ����������
            if(rc == MQTTCLIENT_DISCONNECTED)
            {
                if(setPub)
                {
                    if((rc = MQTTClient_connect(client, &conn_opts))!=MQTTCLIENT_SUCCESS)
                        pubs=GetTickCount()+2000L;
                    else{
                        Err++;
                        rc = _mk_mqtt(client, pubmsg, num,"LostMQTT", (PVOID) &Err, qos );
                    }
                }
            }

// ����� ��� ������������
            wsprintf((LPSTR)ReqString,".vtreq%02d",num);
            if((GetFileAttributes(ReqString)!=INVALID_FILE_ATTRIBUTES))
            {
                DeleteFile((LPCTSTR)ReqString);
                quit=1;
            }
        }


        if(debug)
            _closeConsole();
        MQTTClient_destroy(&client);
        InterlockedExchange8((CHAR volatile *) &wi->isLink,0);
        free_w(client_id);
        free_w(recvbuf);
        free_w(sendbuf);
        cc=UDP_Close();
        CloseHandle(isRun);
        _destroyExchangeObj(num, &fm);
        return cc;
    }
#endif // EHCHANGE_C
