#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef HOOKSPY_C
#define HOOKSPY_C

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <windows.h>
#include <windowsx.h>
#include <gdiplus.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"
#include "..\inc\udp2mat.h"
#include "resource.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define GCL_HICON (-14)
#define Direct 0
#define Left 1
#define Right 2
#define Center 3


BYTE bAlpha = 0xFF;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
char szClassName[] = "CG_WEIGHT_Analyse";
HICON hIcon;
PCHAR WrkArea;
PLONG wSum,p1,p2;
UINT16 port;
INT Xtop=0,Ytop=0;
HWND ExitHwnd; // ������ �����

INT16 l2str(PCHAR b,long num, long div)
{
INT16 Idx=0;
if(num<0L)
    {
    b[Idx++]='-';
    num=-num;
    }
wsprintf(b+Idx,"%ld.%03ld",num/div,num%div);
return(lstrlen(b));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    int cc,cnt=0;
    DWORD t,period,summ;
    BOOL rc;
    HWND hWnd;
    MSG lpMsg;
    WNDCLASS wc;
    int i;
    LRESULT cret;
    LPSTR Err;
    LPVOID lpMap;
    COLORREF clref;
    CHAR isRunName[0x40];
	HANDLE isRun;

	GpStatus gs;
	GDIPCONST GdiplusStartupInput gi={1,NULL,FALSE,FALSE};
	GdiplusStartupOutput go;
    ULONG_PTR gToken;


    if((gs = GdiplusStartup(&gToken,&gi,NULL))!=Ok)
        {
        MessageBox(NULL,"������ ����������� �������!","Error", MB_OK);
        return FALSE;
        }



    WrkArea = malloc_w(0x1000);

    if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"p" , (PCHAR) WrkArea)))
        {
		MessageBox(NULL,"������� �������� -p=udp_port","Error", MB_OK);
		GdiplusShutdown(gToken);
        free_w(WrkArea);
        return FALSE;
        }

    lstrcpy(isRunName,"Local\\Port");
    lstrcat(isRunName,WrkArea);

	if((isRun = _chkIsRun((LPSTR)isRunName))==NULL)
    	{
		lstrcpy(isRunName,"���� ");
	    lstrcat(isRunName,WrkArea);
	    lstrcat(isRunName," ��� ������!");
		MessageBox(NULL,isRunName,"Error", MB_OK);
		GdiplusShutdown(gToken);
        free_w(WrkArea);
    	return FALSE;
    	}

    port = (UINT16)atol(WrkArea);
    if(cc = _scan_cmd_line((PCHAR)lpszCmdline,"t" , (PCHAR) WrkArea))
	    period = (DWORD)atol(WrkArea);
	else
		period = 5000;

    cc=UDP_Init();
    cc=UDP_OpenReceive();

    cc = 0;
    summ=0;
    wSum = malloc_w(0x8000);
    p1 = malloc_w(0x8000);
    p2 = malloc_w(0x8000);

    cc = _scan_cmd_line((PCHAR)lpszCmdline,"a" , (PCHAR) WrkArea);
    switch(WrkArea[0])
        {
            case '1': Xtop=Ytop=0;break;
            case '2': Xtop=GetSystemMetrics(SM_CXSCREEN)>>1;Ytop=0;break;
            case '3': Xtop=0;Ytop=GetSystemMetrics(SM_CYSCREEN)>>1;break;
            case '4': Xtop=GetSystemMetrics(SM_CXSCREEN)>>1;Ytop=GetSystemMetrics(SM_CYSCREEN)>>1;break;
            default:Xtop=Ytop=0;break;
        }
              // ��������� ��������� ������ ����
      wc.style         = CS_HREDRAW | CS_VREDRAW;
      wc.lpfnWndProc   = WndProc;
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInstance;
      wc.hIcon         = NULL;
      wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
      wc.lpszMenuName  = NULL;
      wc.lpszClassName = szClassName;

      if (!RegisterClass(&wc))
      {
            MessageBox(NULL, "�� ���� ���������������� ����� ����!", "������", MB_OK);
            GdiplusShutdown(gToken);
            free_w(WrkArea);
            return 0;
      }
//  hWnd = CreateWindowEx(WS_EX_TOOLWINDOW|WS_EX_LAYERED,szClassName,TEXT("Analyse Weight"),  WS_POPUP | WS_DLGFRAME,
//    0,0,740,465 /*GetSystemMetrics(SM_CYSCREEN)*/, NULL, NULL, (HINSTANCE)hInstance, NULL);
//clref = RGB(0xc0, 0xc0, 0xc0);

hWnd = CreateWindowEx(WS_EX_TOOLWINDOW|WS_EX_LAYERED,szClassName,TEXT("Analyse Weight"),  WS_POPUP | WS_DLGFRAME,
    Xtop,Ytop,GetSystemMetrics(SM_CXSCREEN)>>1,GetSystemMetrics(SM_CYSCREEN)>>1, NULL, NULL, (HINSTANCE)hInstance, NULL);

SetLayeredWindowAttributes( hWnd, RGB(0xFF,0x0,0xFF), bAlpha, LWA_COLORKEY |  LWA_ALPHA);

      // ���������� ���� ����
      ShowWindow(hWnd, nCmdShow);
      UpdateWindow(hWnd);



    while (GetMessage(&lpMsg, NULL, 0, 0))
      {
            TranslateMessage(&lpMsg);
            DispatchMessage(&lpMsg);
      }
cc=UDP_Close();
CloseHandle(isRun);
free_w(p1);
free_w(p2);
free_w(wSum);
free_w(WrkArea);
GdiplusShutdown(gToken);
UnregisterClass(szClassName, hInstance);
return (lpMsg.wParam);
}

BOOL AnsiToUnicode16(CHAR *in_Src, WCHAR *out_Dst, INT in_MaxLen)
{
    INT lv_Len;
if (in_MaxLen <= 0)
    return FALSE;

  // let windows find out the meaning of ansi
  // - the SrcLen=-1 triggers MBTWC to add a eos to Dst and fails if MaxLen is too small.
  // - if SrcLen is specified then no eos is added
  // - if (SrcLen+1) is specified then the eos IS added
  lv_Len = MultiByteToWideChar(CP_ACP, 0, in_Src, -1, out_Dst, in_MaxLen);
  if (lv_Len < 0)
    lv_Len = 0;

  // ensure eos, watch out for a full buffersize
  // - if the buffer is full without an eos then clear the output like MBTWC does
  //   in case of too small outputbuffer
  // - unfortunately there is no way to let MBTWC return shortened strings,
  //   if the outputbuffer is too small then it fails completely
  if (lv_Len < in_MaxLen)
    out_Dst[lv_Len] = 0;
  else if (out_Dst[in_MaxLen-1])
    out_Dst[0] = 0;
  return TRUE;
}

BOOL makeGpString(GpGraphics *G,LPSTR fn,REAL fs, FontStyle st,LinearGradientMode gm,ARGB ag,ARGB ag1,float X,float Y,float W, BYTE Mode,LPSTR str)
{
    GpPen *gpPen;
    ARGB argb;
    GpImage * gpi;
    GpFont *gpf;
    GpFontFamily *FontFamily;
    GpStringFormat *gpSFormat;
    RectF rf={0.};
    GpStatus CC;
    GpBrush *gpBrush;
    PWCHAR wsout;
    LOGFONTW lf;
    REAL sFnt;

    //CC = GdipCreateSolidFill(ag,&gpBrush);
    wsout=(PWCHAR) GdipAlloc(0x100);
    AnsiToUnicode16((CHAR *) fn, wsout, 0x80);
    CC = GdipCreateFontFamilyFromName(wsout,NULL, &FontFamily);
    CC = GdipCreateFont(FontFamily,fs,st,UnitMillimeter,&gpf);
    CC = GdipCreateStringFormat(0,LANG_RUSSIAN,&gpSFormat);
    CC = GdipGetLogFontW(gpf,G,&lf);



    rf.X=X; rf.Y=Y;
    AnsiToUnicode16((CHAR *) str, wsout,0x80);
    CC = GdipMeasureString(G,wsout,lstrlen(str),gpf,&rf,gpSFormat,&rf,NULL,NULL);
    switch(Mode)
    {
        case Left: rf.X+=5.;break;
        case Right: rf.X=X+W - rf.Width-5.; break;
        case Center: rf.X =X+(W/2. - rf.Width/2.); break;
        default: ;
    }

    GdipCreateLineBrushFromRect(&rf,ag,ag1,gm,WrapModeTile,&gpBrush);
    CC = GdipDrawString(G,wsout,-1,gpf,&rf,gpSFormat,gpBrush);
    CC = GdipDeleteStringFormat(gpSFormat);
    CC = GdipDeleteFontFamily(FontFamily);
    CC = GdipDeleteFont(gpf);
    CC = GdipDeleteBrush((GpBrush *) gpBrush);
GdipFree((PVOID)wsout);
return TRUE;
}

void _mkRect(INT X, INT Y, INT dx,INT dy, RECT *rc)
{
rc->left=X;
rc->top=Y;
rc->right=rc->left+dx;
rc->bottom=rc->top+dy;
}

void _DrawRoundRectangle(GpGraphics *Gr,ARGB ag,ARGB ag1, LinearGradientMode gm, float X, float Y, float Width, float Height, float rz)
{
GpPen *gpPen;
GpPath *gpPath;
GpBrush *gpBrush;
RectF rf={0.};

float RoundSize=5.f;

if(rz!=0.0)
    RoundSize=rz;
if(rz > (Height/2.))
    RoundSize=Height/2.f;

GdipCreatePen1(0x0FF000000,.5,UnitPixel,&gpPen);
GdipCreatePath(1,&gpPath);


GdipAddPathArc(gpPath,X,Y,RoundSize * 2.,RoundSize * 2.,180.,90.);
GdipAddPathLine(gpPath,X + RoundSize, Y, X + Width - RoundSize, Y);
GdipAddPathArc(gpPath,X + Width - RoundSize * 2., Y, RoundSize * 2., RoundSize * 2., 270., 90.);
GdipAddPathLine(gpPath,X + Width, Y + RoundSize, X + Width, Y + Height - RoundSize);
GdipAddPathArc(gpPath,X + Width - RoundSize * 2., Y + Height - RoundSize * 2., RoundSize * 2., RoundSize * 2., 0., 90.);
GdipAddPathLine(gpPath,X + RoundSize, Y + Height, X + Width - RoundSize, Y + Height);
GdipAddPathArc(gpPath,X, Y + Height - RoundSize * 2., RoundSize * 2., RoundSize * 2., 90., 90.);
GdipAddPathLine(gpPath,X, Y + RoundSize, X, Y + Height - RoundSize);

rf.X=X;rf.Y=Y;rf.Width=Width;rf.Height=Height;
GdipCreateLineBrushFromRect(&rf,ag,ag1,gm,WrapModeTile,&gpBrush);
GdipDrawPath(Gr,gpPen,gpPath);
GdipFillPath(Gr,gpBrush,gpPath);

GdipDeleteBrush(gpBrush);
GdipDeletePath(gpPath);
GdipDeletePen(gpPen);
}

INT _getpercentXY(RECT rc,float pc,BOOL isX)
{
if(isX)
    return(INT)(((float)(rc.right - rc.left) / 100.)*pc);
else
    return (INT)(((float)(rc.bottom - rc.top) / 100.)*pc);
}

float _getpercentF(RECT rc,float pc,BOOL isX)
{
if(isX)
    return(((float)(rc.right - rc.left)) / 100.f*pc);
else
    return(((float)(rc.bottom - rc.top)) / 100.f*pc);
}

INT PaintDot(HDC mDC,INT X, INT Y,INT R, BOOL b)
{
HPEN hPen,wPen;
HBRUSH hBrush,nBrush;
COLORREF clrf;
HRGN hRgn;
RECT rect;

GRADIENT_RECT gRect;
TRIVERTEX vertex[2] ;

int r1,r2,g1,g2,b1,b2;

hPen=GetStockObject(DC_PEN);
hBrush=GetStockObject(DC_BRUSH);
nBrush=GetStockObject(NULL_BRUSH);

rect.left   = (LONG)X-R;
rect.top    = (LONG)Y-R;
rect.right  = (LONG)X+R;
rect.bottom = (LONG)Y+R;

SelectObject(mDC,(HGDIOBJ)hPen);
SelectObject(mDC,(HGDIOBJ)hBrush);
SetDCBrushColor(mDC,hBrush);
SetDCPenColor(mDC,RGB(0,0xFF,0xFF));
hRgn = CreateEllipticRgn(rect.left,rect.top,rect.right,rect.bottom);
vertex[0].x     = rect.left;
vertex[0].y     = rect.top;
vertex[1].x     = rect.right;
vertex[1].y     = rect.bottom;
gRect.UpperLeft  = 0;
gRect.LowerRight = 1;
if(!b)
    {
    vertex[0].Red   = 0x0;
    vertex[0].Green = 0x1111;
    vertex[0].Blue  = 0x0;
    vertex[0].Alpha = 0x0000;

    vertex[1].Red   = 0x0;
    vertex[1].Green = 0xCCCC;
    vertex[1].Blue  = 0x0;
    vertex[1].Alpha = 0x0;
    }
else{
    vertex[0].Red   = 0xCCCC;
    vertex[0].Green = 0x0;
    vertex[0].Blue  = 0x0;
    vertex[0].Alpha = 0x0000;

    vertex[1].Red   = 0x1111;
    vertex[1].Green = 0x0;
    vertex[1].Blue  = 0x0;
    vertex[1].Alpha = 0x0;
}

GradientFill(mDC, vertex,2,&gRect,1,GRADIENT_FILL_RECT_V);
/*wPen = CreatePen(PS_SOLID,3,RGB(0,0xFF,0xFF));
hPen=SelectObject(mDC,(HGDIOBJ)wPen);
Ellipse(mDC,rect.left,rect.top,rect.right,rect.bottom);
SelectObject(mDC,(HGDIOBJ)hPen);
DeleteObject((HGDIOBJ)wPen);*/
return 1;
}

INT PaintGraph(HDC mDC,INT X, INT Y,INT dx, INT dy,long* arr, long data,BOOL zm)
{
static int step = 0;
static float zoom=200.;
INT i,j=0,lim,dotY;
RECT gr;
HBRUSH hb;
HPEN hp;
hb=GetStockObject(DC_BRUSH);
hp=GetStockObject(DC_PEN);
SelectObject(mDC,hb);
SelectObject(mDC,hp);
//SetDCBrushColor(mDC,RGB(0x2,0x2,0x0));
gr.left=X+1;
gr.top=Y+1;
gr.right=dx-1;
gr.bottom=dy-1;
//FillRect(mDC,&gr,hb);

//SetDCPenColor(mDC,RGB(0x0,0xCC,0x0));
//Rectangle(mDC,gr.left,gr.top,gr.right,gr.bottom);
switch(step)
    {
    case 0: SetDCPenColor(mDC,RGB(0x0,0xFF,0xFF));break;
    case 1: SetDCPenColor(mDC,RGB(0xFF,0x80,0x80));break;
    case 2: SetDCPenColor(mDC,RGB(0xFF,0xFF,0x0)); step=-1; break;
    }
step++;
//SetDCPenColor(mDC,RGB(0x0,0xFF,0xFF));

if((data>zoom) && (zm))
    zoom=data+1000.;
else if(!zm)
    zoom=200.;


dotY =(INT) (float)gr.bottom - (((float)(gr.bottom-gr.top) / (zoom)) * ((float)arr[0]+50.));
if(gr.top > dotY)
    dotY=gr.top;

MoveToEx(mDC,X,dotY,(LPPOINT) NULL);
i=X+1;
lim=gr.right-gr.left;

for(j=0;j<lim;i++,j++)
    {
    dotY =(INT) (float)gr.bottom - (((float)(gr.bottom-gr.top) / (zoom)) * ((float)arr[j]+50.));
    if(gr.top > dotY)
        dotY=gr.top;
    LineTo(mDC,i,dotY);
    arr[j]=arr[j+1];
    }
arr[lim] = data;
return 1;
}

INT GdiPlusDot(GpGraphics *Gr,INT X, INT Y,INT W, INT H, BOOL b)
{
GpLineGradient *gpBrush;
GpRect gpRect;
GpStatus gpCC;

gpRect.X=X; gpRect.Y=Y;gpRect.Width=W;gpRect.Height=H;

if(b)
    gpCC = GdipCreateLineBrushFromRectI(&gpRect,0x88008800UL,0xFF00FF00UL,LinearGradientModeForwardDiagonal,WrapModeTile,&gpBrush);
else
    gpCC = GdipCreateLineBrushFromRectI(&gpRect,0xFFFF0000UL,0x88880000UL,LinearGradientModeBackwardDiagonal,WrapModeTile,&gpBrush);

//    gpCC = GdipDrawEllipseI(,GpPen*,INT,INT,INT,INT);
    gpCC = GdipFillEllipseI(Gr,gpBrush,X,Y,W,H);
    gpCC = GdipDeleteBrush((GpBrush *) gpBrush);
return (INT)gpCC;
}

INT _makeGraph(GpGraphics *Gr,float X, float Y,float W, float H, long *arr0,long *arr1,long *arrS,long data0,long data1,long dataS,BOOL busy)
{
static float zoom=200.f;
GpStatus gpCC;
RectF rf={0.},sh;
GpLineGradient *gpBrush;
GpPen *pen0,*pen1,*penS;
GpPointF *p0,*p1,*pS;
INT i, step=1;


rf.X=X;rf.Y=Y;
sh.Width=rf.Width=W;sh.Height=rf.Height=H;
sh.X=rf.X+50.;sh.Y=rf.Y+50.;
if(!busy)
    {
    gpCC = GdipCreateLineBrushFromRect(&rf,0x00000000UL,0x88002200UL,LinearGradientModeVertical,WrapModeTile,&gpBrush);
    zoom=200.f;
    }
else
    {
    gpCC = GdipCreateLineBrushFromRect(&rf,0x00000000UL,0x88220000UL,LinearGradientModeVertical,WrapModeTile,&gpBrush);
    zoom=max(zoom,(float)dataS);
    }

p0=(GpPointF*) GdipAlloc(0x1000*sizeof(GpPointF));
p1=(GpPointF*) GdipAlloc(0x1000*sizeof(GpPointF));
pS=(GpPointF*) GdipAlloc(0x1000*sizeof(GpPointF));

//gpCC = GdipFillRectangle(Gr,gpBrush,X+10,Y+5,W,H);
gpCC = GdipFillRectangle(Gr,gpBrush,X,Y,W,H);

for(i=0; i<((INT)W); i++)
    {
    pS[i].X=p1[i].X=p0[i].X=X+i;

    p0[i].Y=max(Y+2,(Y+H) - (H /zoom) * ((float)arr0[i]+5.));
    p0[i].Y=min(Y+H-2,p0[i].Y);

    p1[i].Y=max(Y+2,(Y+H) - (H /zoom) * ((float)arr1[i]+5.));
    p1[i].Y=min(Y+H-2,p1[i].Y);

    pS[i].Y=max(Y+2,(Y+H) - (H /zoom) * ((float)arrS[i]+5.));
    pS[i].Y=min(Y+H-2,pS[i].Y);

    arr0[i]=arr0[i+1];
    arr1[i]=arr1[i+1];
    arrS[i]=arrS[i+1];
    }
arr0[((INT)W)]=data0/*rand()%100*/;
arr1[((INT)W)]=data1/*rand()%100*/;
arrS[((INT)W)]=dataS;

GdipCreatePen1(0xFF00FF00,.125,UnitPixel,&pen0);
GdipCreatePen1(0xFF0000FF,.125,UnitPixel,&pen1);
GdipCreatePen1(0xFFFF0000,.5,UnitPixel,&penS);


GdipDrawCurve(Gr,penS,(GDIPCONST GpPointF*)pS,((INT)W));
GdipDrawCurve(Gr,pen0,(GDIPCONST GpPointF*)p0,((INT)W));
GdipDrawCurve(Gr,pen1,(GDIPCONST GpPointF*)p1,((INT)W));


GdipDeletePen(penS);
GdipDeletePen(pen1);
GdipDeletePen(pen0);
GdipFree(p0);
GdipFree(p1);
GdipFree(pS);

gpCC = GdipDeleteBrush((GpBrush *) gpBrush);
return (INT) gpCC;
}

INT BuildScreen(HWND hWnd,pWEGINFO pw)
{
GpRectF gpRect;
GpGraphics *gpGr;
GpLineGradient *gpBrush;
GpBitmap *gpBitmap;
GpStatus gpCC;
RECT r;
HDC Memhdc;
HDC hdc;
HBITMAP Membitmap;
HGDIOBJ hOldObj;
ARGB b=0xFF00FF00UL,e=0x88004400UL;
CHAR buf[0x40];
long weg,w0,w1,wS;
static BOOL udpLink=FALSE;
static time_t isNow;

if(!pw->ID)
    return 0;

GetClientRect(hWnd, &r);
hdc = GetDC(hWnd);
Memhdc = CreateCompatibleDC(hdc);
Membitmap = CreateCompatibleBitmap(hdc,r.right-r.left,r.bottom-r.top);
hOldObj = SelectObject(Memhdc, Membitmap);

gpCC = GdipCreateFromHDC(Memhdc,&gpGr);
//gpCC = GdipCreateFromHWND(hWnd,&gpGr);

GdipSetPageUnit(gpGr,UnitPixel);
gpCC = GdipSetSmoothingMode(gpGr,SmoothingModeHighQuality);
gpCC = GdipSetSmoothingMode(gpGr,SmoothingModeAntiAlias8x8);
gpCC = GdipSetTextRenderingHint(gpGr,TextRenderingHintClearTypeGridFit);

gpRect.X=(float)r.left;gpRect.Y=(float)r.top;
gpRect.Width=(float)r.right-(float)r.left;gpRect.Height=(float)r.bottom-(float)r.top;


gpCC = GdipCreateLineBrushFromRect(&gpRect,0x44002222UL,0xFF008888UL,LinearGradientModeForwardDiagonal,WrapModeTile,&gpBrush);
gpCC = GdipFillRectangle(gpGr,gpBrush,gpRect.X,gpRect.Y,gpRect.Width,gpRect.Height);
gpCC = GdipDeleteBrush((GpBrush *) gpBrush);

//  ������� ����� � ������
if(time(NULL) >= isNow)
    {
    isNow = time(NULL)+1UL;
    udpLink^=TRUE;
    }
gpCC = GdiPlusDot(gpGr,_getpercentXY(r,96.,TRUE),_getpercentXY(r,1.5,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE), udpLink);

sprintf(buf,"���� �%d",pw->ID);
gpCC = makeGpString(gpGr,"Arial Black",_getpercentF(r,2.5,FALSE),FontStyleBold,LinearGradientModeVertical,0x88CC0000UL,0xCCCC0000UL,4.,_getpercentF(r,.05f,FALSE)+5,_getpercentF(r,100.,TRUE), Center,buf);
gpCC = makeGpString(gpGr,"Arial Black",_getpercentF(r,2.5,FALSE),FontStyleBold,LinearGradientModeVertical,0x88FF00FFUL,0xFFFFFF00UL,0.,_getpercentF(r,.05f,FALSE),_getpercentF(r,100.,TRUE), Center,buf);
if(pw->isBusy)
    {
    e = 0x44FF0000UL;
    b = 0xFFFF0000UL;
    }
w0=(long)pw->Weight[0];
w1=(long)pw->Weight[1];
wS=(long)pw->WeightSUM;
if(pw->ID==42)
	{
	w0=(long)pw->CarWeight[0]>>1;
	w1=(long)pw->CarWeight[0]>>1;
	wS=(long)pw->CarSum;
	}
l2str(buf,(long) w0,1000L);
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,6,FALSE),FontStyleRegular,LinearGradientModeVertical,b ,e ,0.,_getpercentF(r,10.f,FALSE),_getpercentF(r,50.,TRUE), Left,buf);
l2str(buf,(long) w1,1000L);
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,6,FALSE),FontStyleRegular,LinearGradientModeVertical,b ,e ,_getpercentF(r,50.,TRUE),_getpercentF(r,10.f,FALSE),_getpercentF(r,50.,TRUE),Right,buf);
l2str(buf,(long) wS,1000L);
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,6,FALSE),FontStyleRegular,LinearGradientModeVertical,0x88002200 ,0x88002200 ,_getpercentF(r,25.,TRUE)+6.,_getpercentF(r,30.f,FALSE)+6,_getpercentF(r,50.,TRUE), Center,buf);
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,6,FALSE),FontStyleRegular,LinearGradientModeVertical,b ,e ,_getpercentF(r,25.,TRUE),_getpercentF(r,30.f,FALSE),_getpercentF(r,50.,TRUE),Center,buf);

_makeGraph(gpGr,_getpercentF(r,5,TRUE),_getpercentF(r,56,FALSE),_getpercentF(r,90,TRUE), _getpercentF(r,35,FALSE), p1,p2,wSum,w0,w1,wS,pw->isBusy);

_DrawRoundRectangle(gpGr,0xFFCC0000,0xFF0F0000,LinearGradientModeBackwardDiagonal,_getpercentF(r,85.,TRUE),_getpercentF(r,93.f,FALSE),_getpercentF(r,10.,TRUE),_getpercentF(r,5.f,FALSE),_getpercentF(r,10.f,FALSE));
wsprintf(buf,"�����");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,1.,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFFCCCC00,_getpercentF(r,85.,TRUE),_getpercentF(r,93.f,FALSE),_getpercentF(r,10.5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,5.,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->isLink);
wsprintf(buf,"WE");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,4.,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,8.5,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->lnkLogik);
wsprintf(buf,"LK");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,7.5,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,12,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->Motion^1);
wsprintf(buf,"%s",pw->Motion > 0 ? "MV":"ST");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,11.,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,19,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->Bell);
wsprintf(buf,"%s",pw->Bell > 0 ? "Z":"z");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,18.,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,22.5,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->Light);
wsprintf(buf,"%s",pw->Light > 0 ? "SP":"sp");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,21.5,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,26.,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->Semaphore);
wsprintf(buf,"%s",pw->Semaphore > 0 ? "SM":"sm");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,25.,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,33.,TRUE),_getpercentXY(r,93,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE),pw->CarKey);
wsprintf(buf,"%s",pw->CarKey > 0 ? "SV":"sv");
gpCC = makeGpString(gpGr,"Impact",_getpercentF(r,.75,FALSE),FontStyleRegular,LinearGradientModeVertical,0xFF000000,0xFF888800,_getpercentF(r,32.,TRUE),_getpercentF(r,93.75f,FALSE),_getpercentF(r,5,TRUE),Center,buf);

gpCC = GdipDeleteGraphics(gpGr);
BitBlt(hdc, 0, 0, r.right-r.left,r.bottom-r.top, Memhdc, 0, 0, SRCCOPY);

SelectObject(Memhdc, hOldObj);
DeleteObject(Membitmap);
DeleteDC    (Memhdc);
//	DeleteDC    (hdc);
ReleaseDC(hWnd,hdc);
return (INT) gpCC;
}

/*
INT UpdateDlg(HDC mDC, RECT r, pWEGINFO pw)
{
static time_t isNow;
static BOOL udpLink=FALSE;
int i;
GpRect gpRect;
GpGraphics *gpGr;
GpStatus gpCC;
GpLineGradient *gpBrush;
RECT wRect;
CHAR buf[0x40];



gpCC = GdipCreateFromHDC(mDC,&gpGr);
GdipSetPageUnit(gpGr,UnitPixel);
memmove(&gpRect,&r,sizeof(gpRect));
gpCC = GdipCreateLineBrushFromRectI(&gpRect,0x44008888UL,0x8800CCCCUL,LinearGradientModeForwardDiagonal,WrapModeTile,&gpBrush);
gpCC = GdipFillRectangleI(gpGr,gpBrush,gpRect.X,gpRect.Y,gpRect.Width,gpRect.Height);
gpCC = GdipDeleteBrush((GpBrush *) gpBrush);


sprintf(buf,"���� �%d",pw->ID);
gpCC = outGpString(gpGr,"Arial Black",_getpercentF(r,1.8,FALSE),FontStyleBold,0x88FFFF00UL,0xFFFFFF00UL,_getpercentF(r,39.,TRUE),_getpercentF(r,.05f,FALSE),buf);

//  ������� ����� � ������
if(time(NULL) >= isNow)
    {
    isNow = time(NULL)+2UL;
    udpLink^=TRUE;
    }
//PaintDot(mDC,_getpercentXY(r,98.5,TRUE),_getpercentXY(r,2.5,FALSE),_getpercentXY(r,1,TRUE),udpLink);
gpCC = GdiPlusDot(gpGr,_getpercentXY(r,96.,TRUE),_getpercentXY(r,1.5,FALSE),_getpercentXY(r,3,TRUE),_getpercentXY(r,5,FALSE), udpLink);

//PaintDot(mDC,_getpercentXY(r,5.,TRUE),_getpercentXY(r,94.5,FALSE),_getpercentXY(r,2.5,TRUE),pw->isLink^1);
gpCC = GdiPlusDot(gpGr,_getpercentXY(r,2.,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,6.,TRUE),_getpercentXY(r,5.5,FALSE), pw->isLink);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleBold,0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,1.75,TRUE),_getpercentXY(r,93.,FALSE),"WeLink");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,8.25,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,6.,TRUE),_getpercentXY(r,5.5,FALSE), pw->lnkLogik);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,8.5,TRUE),_getpercentXY(r,93.,FALSE),"ioLink");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,14.5,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,6.,TRUE),_getpercentXY(r,5.5,FALSE),pw->Motion^1);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,15.0,TRUE),_getpercentXY(r,93.,FALSE),"����.");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,21.,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,6.,TRUE),_getpercentXY(r,5.5,FALSE),pw->ZeroKey);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,21.,TRUE),_getpercentXY(r,93.,FALSE),"�����.");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,27.,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,6.,TRUE),_getpercentXY(r,5.5,FALSE),pw->CarKey);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,27.5,TRUE),_getpercentXY(r,93.,FALSE),"����.");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,33.,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,7,TRUE),_getpercentXY(r,5.5,FALSE),pw->Bell);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,33.5,TRUE),_getpercentXY(r,93.,FALSE),"������");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,40.,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,8.,TRUE),_getpercentXY(r,5.5,FALSE),pw->Light);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,40.25,TRUE),_getpercentXY(r,93.,FALSE),"�������");

gpCC = GdiPlusDot(gpGr,_getpercentXY(r,48.25,TRUE),_getpercentXY(r,92.,FALSE),_getpercentXY(r,10.,TRUE),_getpercentXY(r,5.5,FALSE),pw->Semaphore);
gpCC = outGpString(gpGr,"Verdana",_getpercentXY(r,0.75,FALSE),FontStyleRegular, 0xFFFFFF00UL,0xFFFF00FFUL,_getpercentXY(r,48.75,TRUE),_getpercentXY(r,93.,FALSE),"��������");

gpCC = GdipDeleteGraphics(gpGr);

//FillRect(mDC, &r, (HBRUSH) GetStockObject(DKGRAY_BRUSH));
SetBkMode(mDC,TRANSPARENT);
SetTextColor(mDC,RGB(0xFF,0xFF,0x0));

_mkRect(_getpercentXY(r,1,TRUE),_getpercentXY(r,50,FALSE),_getpercentXY(r,98.,TRUE),_getpercentXY(r,39.,FALSE), &wRect);
FillRect(mDC, &wRect, (HBRUSH) GetStockObject(BLACK_BRUSH));

PaintGraph(mDC,wRect.left, wRect.top,wRect.right, wRect.bottom,wSum,pw->WeightSUM,(BOOL)pw->isBusy);
PaintGraph(mDC,wRect.left, wRect.top,wRect.right, wRect.bottom,p1,pw->Weight[0],(BOOL)pw->isBusy);
PaintGraph(mDC,wRect.left, wRect.top,wRect.right, wRect.bottom,p2,pw->Weight[1],(BOOL)pw->isBusy);


//_mkRect(_getpercentXY(r,38.,TRUE),_getpercentXY(r,1.5,FALSE),_getpercentXY(r,25.,TRUE),_getpercentXY(r,10.,FALSE), &wRect);
/*sprintf(buf,"���� �%d",pw->ID);
SelectUserFont(mDC,_getpercentXY(r,8.0,TRUE),_getpercentXY(r,3.0,FALSE),0,700,RUSSIAN_CHARSET,"Arial Black",FALSE);
i = DrawText(mDC,buf,-1,&wRect,DT_SINGLELINE | DT_CENTER | DT_VCENTER);
*/
/*if(pw->isBusy)
    SetTextColor(mDC,RGB(0xFF,0x40,0x40));
else
    SetTextColor(mDC,RGB(0x40,0xFF,0x40));

_mkRect(_getpercentXY(r,1.,TRUE),_getpercentXY(r,1.8,FALSE),_getpercentXY(r,43.,TRUE),_getpercentXY(r,25.,FALSE), &wRect);
//sprintf(buf,"%ld.%03lu",pw->Weight[0]/1000,pw->Weight[0]%1000);
sprintf(buf,"%ld.%03d",(long)pw->Weight[0]/1000L,(long)abs(pw->Weight[0])%1000L);
SelectUserFont(mDC,_getpercentXY(r,18.,TRUE),_getpercentXY(r,10,FALSE),0,700,RUSSIAN_CHARSET,"Verdana",FALSE);
i = DrawText(mDC,buf,-1,&wRect,DT_SINGLELINE | DT_CENTER | DT_VCENTER);

_mkRect(_getpercentXY(r,58.,TRUE),_getpercentXY(r,1.5,FALSE),_getpercentXY(r,42.,TRUE),_getpercentXY(r,25.,FALSE), &wRect);
sprintf(buf,"%ld.%03ld",pw->Weight[1]/1000,pw->Weight[1]%1000);
SelectUserFont(mDC,_getpercentXY(r,18.,TRUE),_getpercentXY(r,10,FALSE),0,700,RUSSIAN_CHARSET,"Verdana",FALSE);
i = DrawText(mDC,buf,-1,&wRect,DT_SINGLELINE | DT_CENTER | DT_VCENTER);

_mkRect(_getpercentXY(r,27.,TRUE),_getpercentXY(r,25,FALSE),_getpercentXY(r,45.,TRUE),_getpercentXY(r,25.,FALSE), &wRect);
sprintf(buf,"%ld.%03ld",pw->WeightSUM/1000,pw->WeightSUM%1000);
SelectUserFont(mDC,_getpercentXY(r,18.,TRUE),_getpercentXY(r,10,FALSE),0,700,RUSSIAN_CHARSET,"Verdana",FALSE);
i = DrawText(mDC,buf,-1,&wRect,DT_SINGLELINE | DT_CENTER | DT_VCENTER);

SelectUserFont(mDC,12,6,900,100,RUSSIAN_CHARSET,"Verdana",TRUE);

return 1;
}
*/
void setRegion(HWND hwnd, BOOL isRect)
{
    RECT Rect;
    HRGN hRgn,hRgnNew;
    GetClientRect(hwnd,&Rect);
    hRgn = CreateRectRgn(0,0,Rect.right,Rect.bottom);

    if(isRect)
        hRgnNew = CreateRoundRectRgn(0,0,Rect.right,Rect.bottom,25,25);
    else
        hRgnNew = CreateEllipticRgn(Rect.left+1,Rect.top+1,Rect.right-1,Rect.bottom-1);
    CombineRgn(hRgn,hRgn,hRgnNew,RGN_AND);
    SetWindowRgn(hwnd,hRgn,TRUE);
    DeleteObject((HGDIOBJ)hRgn);
    DeleteObject((HGDIOBJ)hRgnNew);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam)
{
PAINTSTRUCT ps;
int cc;
HBITMAP hBmp;
HDC hdc;
COLORREF cData;
RECT Rect;
HGDIOBJ hMemDC;
DWORD t;
WORD len,isX,isY,rX,rY,dX,dY;
BOOL bVar;
pWEGINFO wi;
INT16 X,Y,W,H;
PDRAWITEMSTRUCT dis;
static BYTE err=0;
static BOOL swWin=FALSE;
static time_t isNow;

      switch (messg)
      {
	    case WM_CREATE:
            hIcon = LoadIcon(GetWindowInstance(hWnd), MAKEINTRESOURCE(ID_ICON));
            SetClassLong(hWnd, GCL_HICON, (LONG) hIcon);
            SetTimer(hWnd,1,100,NULL);
            GetClientRect(hWnd,&Rect);
            X=_getpercentXY(Rect,85,TRUE);Y=_getpercentXY(Rect,90,FALSE);
            W=_getpercentXY(Rect,14.5,TRUE);H=_getpercentXY(Rect,9.5,FALSE);
            /*ExitHwnd = CreateWindowEx(WS_EX_WINDOWEDGE,"BUTTON",NULL  , WS_VISIBLE | WS_CHILD | BS_OWNERDRAW | WS_CLIPSIBLINGS,X,Y,W,H,hWnd,(HMENU)ID_EXIT,(HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE), NULL);
            setRegion(ExitHwnd,FALSE);*/
        break;
        case WM_PAINT:
            hdc = BeginPaint(hWnd, &ps);
            t=UDP_Receive_Timed(port,sizeof(WEGINFO), (PVOID) WrkArea,50UL,&len);
            wi = (pWEGINFO) WrkArea;
            if(len == sizeof(WEGINFO))
                {
                    t++;
                    err=0;
                    isNow = time(NULL)+2;
                    cc = BuildScreen(hWnd,wi);
                }
                else
                    t--;
            EndPaint(hWnd, &ps);
        break;
        case WM_TIMER:
                GetClientRect(hWnd, &Rect);
                InvalidateRect(hWnd,&Rect,FALSE);
                UpdateWindow(hWnd);
        break;
        /*case WM_DRAWITEM:
                {
                    if(wParam!=ID_EXIT) break;
                    dis = (PDRAWITEMSTRUCT)lParam;
                    if(dis->itemAction & ODA_DRAWENTIRE)
                        {
                        W = (dis->rcItem.right-dis->rcItem.left)>>3;
                        PaintDot(dis->hDC,dis->rcItem.left+54,dis->rcItem.top+15,55,(BOOL)(dis->itemAction ^ ODA_SELECT));
                        Rect=dis->rcItem;

                        SetBkMode(dis->hDC,TRANSPARENT);
                        SetTextColor(dis->hDC,RGB(0xFF,0xFF,0x0));
                        SelectUserFont(dis->hDC,20,10,0,700,RUSSIAN_CHARSET,"Verdana",FALSE);
                        DrawText(dis->hDC,"�����",-1,&Rect,DT_SINGLELINE | DT_CENTER | DT_VCENTER);
                        SelectUserFont(dis->hDC,20,10,0,700,RUSSIAN_CHARSET,"Verdana",TRUE);
                        }
                    if(dis->itemState&ODS_SELECTED)
                        {
                        KillTimer(hWnd,1);
                        wi=NULL;
                        PostQuitMessage(0);
                        break;
                        }
                }
        break;*/
        case WM_LBUTTONDOWN:
                GetClientRect(hWnd,&Rect);
                isX = lParam &0xFFFF;
                isY =  lParam>>16;
                rX=_getpercentXY(Rect,85,TRUE);
                dX=rX+_getpercentXY(Rect,10,TRUE);
                bVar = ((isX > rX) && (isX < dX));
                if(bVar)
                    {
                    rY=_getpercentXY(Rect,93,FALSE);
                    dY=rY+_getpercentXY(Rect,5,FALSE);
                    bVar = ((isY > rY) && (isY < dY));
                    if(bVar)
                        {
                        KillTimer(hWnd,1);
                        wi=NULL;
                        PostQuitMessage(0);
                        }
                    }
        break;
        case WM_KEYDOWN:
            if(wParam == VK_ADD)
            	{
            	if(bAlpha < 0xFF)
            		bAlpha+=0x10;
					SetLayeredWindowAttributes( hWnd, /*GetSysColor(COLOR_DESKTOP)*/ cData, bAlpha, LWA_COLORKEY |  LWA_ALPHA);
            	}
            if(wParam == VK_SUBTRACT)
            	{
            	if(bAlpha > 0xF)
            		bAlpha-=0x10;
            //		cData = GetPixel(hdc,2,2);
					SetLayeredWindowAttributes( hWnd, /*GetSysColor(COLOR_DESKTOP)*/ cData, bAlpha, LWA_COLORKEY |  LWA_ALPHA);

            	}
            if(wParam == VK_TAB)
                {
                GetWindowRect(hWnd,&Rect);
                if(!swWin)
                {
                    MoveWindow(hWnd,0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN),TRUE);
                    GetWindowRect(hWnd,&Rect);
                    X=_getpercentXY(Rect,92,TRUE);Y=_getpercentXY(Rect,90,FALSE);
                    W=_getpercentXY(Rect,7.0,TRUE);H=_getpercentXY(Rect,6.0,FALSE);

                }

                else
                {
                    MoveWindow(hWnd,Xtop,Ytop,GetSystemMetrics(SM_CXSCREEN)>>1,GetSystemMetrics(SM_CYSCREEN)>>1,TRUE);
                    GetWindowRect(hWnd,&Rect);
                    X=_getpercentXY(Rect,85,TRUE);Y=_getpercentXY(Rect,88,FALSE);
                    W=_getpercentXY(Rect,13.0,TRUE);H=_getpercentXY(Rect,9.5,FALSE);
                }
                //MoveWindow(ExitHwnd,X,Y,W,H,TRUE);
                //setRegion(ExitHwnd,FALSE);
                swWin^=TRUE;
                memset((PVOID)wSum,0,(size_t)0x7FFF);
                memset((PVOID)p1,0,(size_t)0x7FFF);
                memset((PVOID)p2,0,(size_t)0x7FFF);
                }

			GetClientRect(hWnd, &Rect);
			InvalidateRect(hWnd,&Rect,FALSE);
            break;
        case WM_DESTROY:
            KillTimer(hWnd,1);
            wi=NULL;
            PostQuitMessage(0);
        break;
        default:
                return (DefWindowProc(hWnd, messg, wParam, lParam));
      }
return 0;
}
#endif // HOOKSPY_C
