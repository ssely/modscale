#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\modbus.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"



CHAR DioString[0x100];

// ������ �������
modbus_t * mb_close(modbus_t *mb)
{
if(mb!=(modbus_t *)NULL)
	{
	modbus_close(mb);
	modbus_free(mb);
	}
mb=NULL;
return mb;
}

modbus_t * mb_connect( char * ipaddr,int p,int slave)
{
modbus_t * mb;
mb=modbus_new_tcp(ipaddr, p);
	if (modbus_connect(mb) == -1)
		{
	    modbus_free(mb);
		mb=(modbus_t *)NULL;
    	return mb;
		}
modbus_set_slave(mb,slave);
return mb;
}

//������ ������ � ioLogik
int write_ioLogik(char * ipaddr,int reg, int val)
{
int rc=0;
uint8_t bits[8];
modbus_t *ctx;

	if((ctx = mb_connect(ipaddr,502,1))==NULL)
			return rc;

	modbus_write_bit(ctx,reg,val);
	rc = modbus_read_bits(ctx, 0, 6, bits);
	mb_close(ctx);
	if(rc==6)
		rc=(uint8_t)bits[reg];
	else
		rc=0;
return rc;
}

// �������� ����� � ioLogik
int isLink_ioLogik(char * ipaddr,int p,int slave,DWORD tmr)
{
static DWORD bTimer=0UL,Diff;
static int rc=0,err=0;
modbus_t *ctx;
Diff=GetTickCount()-bTimer;
	if(Diff > tmr)
		{
		bTimer=GetTickCount();
		if((ctx = mb_connect(ipaddr,p,slave))==NULL)
			{
			if((++err) > 3)
				{
				err=0;
				rc=0;
				}
			return rc;
			}
		mb_close(ctx);
		rc=1;
		err=0;
		}
return rc;
}

int modbus_wave(char * ipaddr,int p,int slave,int reg,DWORD tmr,BOOL start)
{
static int isWork=0;
static DWORD bTimer=0UL;
BOOL isUp;
modbus_t *ctx;

	if(!start)
      return start;

	if((!isWork) && (start))
			{
			bTimer=GetTickCount();
			if((ctx = mb_connect(ipaddr,p,slave))==NULL)
				return (int) start;
				modbus_write_bit(ctx,reg,1);
				mb_close(ctx);
				isWork=1;
			}
   	isUp = ((GetTickCount()-bTimer) < tmr);
	if((!isUp) && (isWork))
		{
			if((ctx = mb_connect(ipaddr,p,slave))==NULL)
				return (int) start;

				modbus_write_bit(ctx,reg,0);
				mb_close(ctx);
				isWork=0;
		}

return (int)isUp;
}

int UpDownFlag(int dst, int src)
{
    if(src > dst) return 1;
    if(src < dst) return -1;
return 0;
}

int _get_param(PCHAR ip_addr,PINT bell,PINT light, PINT sema, PINT zr, PBYTE number, PDWORD tmr, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
static BOOL isFirst=FALSE;
CHAR ip[0x10]={0};  // ip ����� ioLogic
PCHAR CurDir;
int rc=0,cc;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("dio","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*number=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
*bell=GetPrivateProfileInt("dio","DOBell",-1,(LPCSTR)CurDir);
*light=GetPrivateProfileInt("dio","DOLight",-1,(LPCSTR)CurDir);
*sema=GetPrivateProfileInt("dio","DOSemaphore",-1,(LPCSTR)CurDir);
*zr=GetPrivateProfileInt("dio","DOZero",-1,(LPCSTR)CurDir);
*tmr=(DWORD)GetPrivateProfileInt("dio","TMRBell",5000,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("dio","debug",0,(LPCSTR)CurDir);

if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("dio","exit",0,(LPCSTR)CurDir);

if(!isFirst)
{
	wsprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) DioString );
	printf("������������ DIO �� �����:\t%s\n",(PCHAR)CurDir);
	printf("IP ����� DIO:\t\t\t\t %s\n",(PCHAR)ip);
    printf("����� �����:\t\t\t\t%03d\n",(INT) *number);
	printf("����� ������ ������:\t\t\t%d\n",(INT) *bell);
	printf("����� ������ ����������:\t\t%d\n",(INT) *light);
	printf("����� ������ ���������:\t\t\t%d\n",(INT) *sema);
    printf("����� ������ ��������� �����:\t\t%d\n",(INT) *zr);
	printf("����� ������ ������:\t\t\t%lu ms\n", *tmr);
	printf("���������� �������:\t\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
	_closeConsole();
}
isFirst=*debug;
cc=0;
free_w(CurDir);
return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    DWORD ttick,summ=0UL,isNow;
    CHAR ipaddr[0x10]; // ����� ����
    BYTE num; // ����� �����
    int diobell,diolight,diosema,diozero;    //���� ��� ������
    DWORD timeout,BellBegin; // ����� ������ ������ � �������������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc;
    FileMap fm;
    pWEGINFO pWi;
    WEGINFO wi;
    modbus_t *mbc;
    PBYTE dataExchange;
    CHAR cfg[0x20];
	HANDLE isRun;



if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    return FALSE;

cc=_get_param((PCHAR) ipaddr,&diobell,&diolight,&diosema,&diozero,&num,&timeout,&debug,&quit,(PCHAR) cfg);
debug=0;

wsprintf((LPSTR)DioString,"Local\\Dio%03d",num);
if((isRun = _chkIsRun((LPSTR)DioString))==NULL)
    {
		wsprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) DioString );
        printf("��������� Dio -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pWi=(pWEGINFO)fm.dataPtr;

if(pWi->ID!=num)
	InterlockedExchange16((volatile SHORT *) &pWi->ID,(SHORT)num);

    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&diobell,&diolight,&diosema,&diozero,&num,&timeout,&debug,&quit,(PCHAR) cfg);
            if(cc & 0x2){
                if(debug){
					sprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) DioString );
                    printf("DIO �����:%s DIO ����:%u\n",ipaddr,502);
                }
                else
					_closeConsole();
            }
        }
    cc = isLink_ioLogik(ipaddr,502,1,5000UL);
    InterlockedExchange8((PBYTE) &pWi->lnkLogik,(CHAR) cc);
    isNow=time(NULL);
    if(UpDownFlag((int) wi.lnkLogik, (int) pWi->lnkLogik))
            printf("%lu | ����:%02u | ����� � %s %s\n",isNow,num,ipaddr, pWi->lnkLogik ? "�������������" : "���������");

// ���������� �������
    if(wi.Bell)
        if((isNow-BellBegin)>=(timeout/1000UL))
            InterlockedExchange8((PBYTE) &pWi->Bell,0);

    if(UpDownFlag((int) wi.Bell, (int) pWi->Bell))
        {
            printf("%lu | ����:%02u | ������: %s\n",isNow,num,pWi->Bell ? "���." : "����");
            if(pWi->Bell) BellBegin=isNow;
			if(diobell > -1)
	            write_ioLogik(ipaddr,(int) diobell, (int) pWi->Bell);
        }

//-------------------------------
// ���������� �����������
    if(UpDownFlag((int) wi.Light, (int) pWi->Light))
        {
            printf("%lu | ����:%02u | ���������: %s\n",isNow,num,pWi->Light ? "���." : "����");
			if(diolight > -1)
	            write_ioLogik(ipaddr,(int) diolight, (int) pWi->Light);
        }
//-------------------------------

// ���������� ����������
    if(UpDownFlag((int) wi.Semaphore, (int) pWi->Semaphore))
        {
            printf("%lu | ����:%02u | ��������: %s\n",isNow,num,pWi->Semaphore ? "���." : "����");
			if(diosema > -1)
	            write_ioLogik(ipaddr,(int) diosema, (int) pWi->Semaphore);
        }
//-------------------------------

// ����� ����� ��������� � ��. �����
	if(wi.ZeroKey &&(diozero > -1))
		InterlockedExchange8((PBYTE) &pWi->ZeroKey,0);

// ��������� �����

    if(UpDownFlag((int) wi.ZeroKey, (int) pWi->ZeroKey))
        {
            printf("%lu | ����:%02u | ��������� �����: %s\n",isNow,num,pWi->ZeroKey ? "���." : "����");
			if(diozero > -1)
	            write_ioLogik(ipaddr,(int) diozero, (int) pWi->ZeroKey);
        }
//------------------------------
    memmove((void *) &wi, (const void *) pWi, (size_t) sizeof(wi));
    Sleep(50UL);

	wsprintf((LPSTR)DioString,".dio%02d",num);
    if((GetFileAttributes(DioString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)DioString);
        quit=1;
        }
    }
if(!debug)
	_closeConsole();

	InterlockedExchange8((PBYTE) &pWi->lnkLogik,0);
	InterlockedExchange8((PBYTE) &pWi->Bell,0);
	InterlockedExchange8((PBYTE) &pWi->Light,0);
	InterlockedExchange8((PBYTE) &pWi->Semaphore,0);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
