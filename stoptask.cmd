@echo off
rem @echo %~dp0
set PATH=%~dp0;%PATH%
set SCALE=04

cd %~dp0
@echo "%DATE% %TIME% %PATH% Scale=%SCALE%" >>%~dp0Scale%SCALE%.pid

echo '1' > .asisoap%SCALE%
%SystemRoot%\System32\ping -n 1 -w 1000 8.8.8.8 >nul

echo '1' > .http%SCALE% 
%SystemRoot%\System32\ping -n 1 -w 1000 8.8.8.8 >nul

echo '1' > .quit%SCALE%
%SystemRoot%\System32\ping -n 1 -w 1000 8.8.8.8 >nul

del /Q *.exe