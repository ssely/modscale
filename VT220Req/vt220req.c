#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define VT_WEIGHT 1
#define VT_ZERO 2


BYTE Query[7]= {0x2,0x41,0x3F,0x30,0x3C,0x34,0x3};
BYTE Zero[9]= {0x2,0x41,0x4B,0x30,0x33,0x30,0x3C,0x33,0x3};
CHAR ReqString[0x100];

WORD _getBCS(PBYTE pb,BYTE cmd)
{
    PWORD pq;
    WORD BCS;
    BYTE bcs;
    if(cmd==1)
    {
        pq=(PWORD)&pb[4];
        bcs=((pb[0] ^ pb[1])^pb[2])^pb[3];
    }
    if(cmd==2)
    {
        pq=(PWORD)&pb[6];
        bcs=((((pb[0] ^ pb[1])^pb[2])^pb[3])^pb[4])^pb[5];
    }
    BCS=(((WORD)(bcs &0xF0))<<4) | (bcs &0xF);
    BCS|=0x3030;
    *pq=BCS;
    return BCS;
}

int _isMove(long Summ, long limit, DWORD timeout)
{
static BOOL isFirst=TRUE;
static DWORD to;
static long Sm;
DWORD isNow;
BOOL Interval;
int cc=0;
if(isFirst)
	{
	to=GetTickCount();
	Sm=Summ;
	isFirst=FALSE;
	}
isNow =  GetTickCount()-to;
Interval = (isNow > timeout);
if(abs(Sm-Summ) > limit)
	{
	if(!Interval)
		cc=1;

	to=GetTickCount();
	Sm=Summ;
	}
return cc;
}

int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];
/*
int cmdport;    //���� ��� ������ ������ �� ������ VT220
int dataport;  // ���� ��� ��������� ������ � VT220
DWORD timeout; //������� �������� ������ � ����� � �������������
DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
BYTE num;   //����� ������������ �����
BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
*cmdport=GetPrivateProfileInt("scale","cmdport",8001,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
*vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ VT220 Auto �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t %s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("������� �������� �������:\t%lu ms\n", *cmdtimeout);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
	
}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
return rc;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
	    {
    	    cc = i+1;
        	break;
	    }
return cc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD ttick,summ=0UL,isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ VT220
    int dataport;  // ���� ��� ��������� ������ � VT220
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i=0,ErrorLink=0,b,e;
    BYTE Idx,prn=0,pkt=0x30;
    FileMap fm;
	pWEGINFO wi;
    long weg_1,weg_2;
	long value[3];
    PBYTE dataExchange;
    CHAR cfg[0x40];
	HANDLE isRun;




if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug, &quit,&limit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Local\\VT220Req%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� VT220Req -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
wi=(pWEGINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(wi->ID!=num)
	InterlockedExchange16((PWORD) &wi->ID,(WORD)num);

lstrcpy(wi->devName,"VT-220");

	cc=UDP_Init();
if(debug)
	printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
if(debug)
	printf("Udp Send Init:%d\n",cc);

    cc=UDP_OpenReceive();
if(debug)
	printf("Udp Reseive Init:%d\n",cc);

	InterlockedExchange16((volatile PWORD) &wi->VT_Num,(WORD)vtnum);
	InterlockedExchange8((volatile PCHAR) &wi->Request,1);
	Query[1]=Zero[1]=(BYTE)vtnum;

while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug,&quit,&limit,cfg);
            Zero[1]=Query[1]=vtnum;
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("MOXA �����:%s MOXA ���� ������:%u MOXA ��������� ����:%u\n",ipaddr,dataport,cmdport);
                }
                else
					_closeConsole();
            }
        }

		cc=_getBCS(Query,VT_WEIGHT);
        cc=UDP_Send(ipaddr,cmdport, (PVOID) Query, 7);
        ttick=GetTickCount();
        Idx=0;
 		memset((PVOID)ReqString,0,36);
        while((GetTickCount()-ttick) < timeout)
        {
            cc=UDP_Receive(dataport,36,(PVOID) (ReqString+Idx));

            if(cc>0)
                Idx+=cc;

            if(Idx>36)
                    Idx=0;

            if(Idx > 12)
            {
                b=_find_chr(ReqString, '\x2');
                e=_find_chr(ReqString, '\x3');
                if((b) && (e>b))
		{
	        prn++;
		i = ReqString[b+11]-0x30;
		ReqString[b+11]=0;
/*					weg_1=atol(ReqString+(DWORD)(b+4));
					if(!i)
						InterlockedExchange((volatile LONG *) &wi->WeightSUM,weg_1); // ��������� ��������� �����
					else
						InterlockedExchange((volatile LONG *) &wi->Weight[i-1],weg_1); //��������� ����� �� ����������
*/
// �������� �������� ����, ��� �������������� ����� �������� �� 2-�� ���������
// i=0 - ����� ��� ,i=1 - ��� �� 1-�� ���������,i=2 - ��� �� 2-�� ���������
			if(i < 3)
			{
			value[i] = atol(ReqString+(DWORD)(b+4));
				if(!i)
					InterlockedExchange((volatile LONG *) &wi->WeightSUM,(long)value[0]); 
				else {
					InterlockedExchange((volatile LONG *) &wi->Weight[0],(long)value[1]);
					InterlockedExchange((volatile LONG *) &wi->Weight[1],(long)value[2]); 
				}
			}
             		break;
		}
            }

        }

		Zero[5]=0x30;
        cc=_getBCS(Zero,VT_ZERO);
        cc=UDP_Send(ipaddr,cmdport, (PVOID) Zero, 9);
        ttick=GetTickCount()-ttick;
        summ+=ttick;
// ��������� ������� ��� ���� � 2 ���		
		if(!wi->CarKey)
	        Sleep(cmdtimeout);
		else		
			Sleep(2000UL);

		isNow=time(NULL);

		if(prn > 30)
        {
			summ/=30UL;
			InterlockedExchange16((PWORD) &wi->TimeVT,(WORD)summ);
            if(debug)
                printf("%lu | ����:%2u | ������:%2u | ���������:%1u | ���������:%7.7s | ������� ������: %u ms\n",isNow,num,vtnum,i,ReqString+5,(int)summ);
            summ=0UL;
            prn=0;
        }
	if(ttick > (timeout>>1))
		{
        if(debug)
            printf("%lu | ����:%u | ������:%u (%i) | �������:%lu ms\n",isNow,num,vtnum,Idx,ttick);
		ErrorLink++;
		}
	else{
   		ErrorLink=0;
		}

		InterlockedExchange((volatile LONG *) &weg_1,wi->WeightSUM);
		InterlockedExchange8((PBYTE) &wi->Motion,_isMove((long) weg_1, 10UL, 500UL));
		if(ErrorLink<10)
            InterlockedExchange8((PBYTE) &wi->isLink,1);
        else
            InterlockedExchange8((PBYTE) &wi->isLink,0);

		if((long)wi->WeightSUM > (long)limit)
            InterlockedExchange8((PBYTE) &wi->isBusy,1);
        else
            InterlockedExchange8((PBYTE) &wi->isBusy,0);

		if(wi->ZeroKey)
			{
			Zero[5]=0x31;
			cc=_getBCS(Zero,VT_ZERO);
			for(cc=0;cc<20;cc++){
				Idx=UDP_Send(ipaddr,cmdport, (PVOID) Zero, 9);
				Sleep(cmdtimeout);
				}
			InterlockedExchange8((volatile PBYTE) &wi->ZeroKey,0);
			}
	wsprintf((LPSTR)ReqString,".vtreq%02d",num);
    if((GetFileAttributes(ReqString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)ReqString);
        quit=1;
        }
	}

if(!debug)
		_closeConsole();

	InterlockedExchange8((PBYTE) &wi->isLink,0);
	cc=UDP_Close();
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
