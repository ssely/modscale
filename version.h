#ifndef VERSION_H 
#define VERSION_H 
   
	//Date Version Type 
		static const char SC_DATE[] = "07"; 
		static const char SC_MONTH[] = "05"; 
		static const char SC_YEAR[] = "2024"; 
		static const char SC_UBUNTU_VERSION_STYLE[] = "07.05"; 
		static const char SC_UBUNTU_TIME_STYLE[] = "13:59:09.55"; 
		static const char SC_UBUNTU_FULLVERSION[] = "1.0-a1 Build 526 (07.05.2024 13:59:09.57)"; 
   
	//Software Status  
   
	//Standard Version Type  
		static const long SC_MAJOR = 1; 
		static const long SC_MINOR = 0; 
		static const long SC_BUILD = 0; 
		static const long SC_REVISION = 1; 
   
	//Miscellaneous Version Type  
		static const long SC_BUILDS_COUNT = 526; 
		#define SC_RC_FILEVERSION 1,0,0,1 
		#define SC_RC_FILEVERSION_STRING "1,0,0,1\0" 
		static const char SC_FULLVERSION_STRING[] = "1.0.0.1"; 
   
	//These values are to keep track of your versioning state, don't modify them.  
		static const long SC_BUILD_HISTORY = 526; 
   
#endif //VERSION_H   
