#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef MK210_C
#define MK210_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\mqtt.h"
#include "..\inc\posix_sockets.h"
#include "..\inc\version.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define READ_HOLD_REGISTERS 0x3
#define WRITE_HOLD_REGISTERS 0x10

#define DOCHANNEL 0x1E
#define LINKCHANNEL 0x1F

#pragma pack(push,1)
typedef struct{
WORD id_transaction;
WORD id_protocol;
WORD packet_len;
BYTE adsu;
BYTE fc;
BYTE data;
}mb_packet,*pmb_packet;
BYTE _data[0x80],_subst[0x80];
#pragma pack(pop)

int sockfd;
struct mqtt_client client;
PBYTE sendbuf=NULL; /* sendbuf should be large enough to hold multiple whole mqtt messages */
PBYTE recvbuf=NULL; /* recvbuf should be large enough any whole mqtt message expected to be received */
UINT8 connect_flags = MQTT_CONNECT_CLEAN_SESSION;//|MQTT_CONNECT_USER_NAME|MQTT_CONNECT_PASSWORD;
UINT8 qos = MQTT_PUBLISH_QOS_0;
enum MQTTErrors ms,session;
char ip_mqtt[0x20], client_id[0x80],port_mqtt[0x8];

UINT8 enableMQ=0;

CHAR DioString[0x100];

pWEGINFO pWi; // ���������� ��������� ��� ������ ����� ��������
WEGINFO wi; // ��������� ��������� ��� MQTT
BYTE subscrabe=0; // ������ �������� �� MQTT �������.

// SET/RESET ��� � �����
WORD rs_bit(WORD mask, UINT8 nBit, BOOL set)
{
WORD data=1;
    nBit&=0xF;
    data<<=nBit;
    if(set)
        return(mask | data);
    else
        return(mask & (data^0xFFFF));
}
// Test BIT
UINT8 test_bit(WORD mask, UINT8 nBit)
{
WORD data = 1;
    nBit&=0xF;
    data<<=nBit;
return ((UINT8)((mask & data) > 0));
}

PCHAR _getBinary(DWORD val, UINT8 cnt)
{
static CHAR Buffer[0x21];
int i;
DWORD data = 1;
cnt&=0x1F;
Buffer[cnt]=0;
for(i=cnt;i>0;i--,data<<=1)
    Buffer[i-1]='0'+(BYTE)((val & data)>0);
return Buffer;
}
// ������ �������
SOCKET mb_connect(LPSTR Addr, UINT Port,int *Error)
{
WSADATA wsa;
static BOOL isFirst=TRUE;
int rc;
DWORD iTime = 1UL,Timed;
SOCKET s;
SOCKADDR_IN sa;
*Error = 0;

// ���� ������ ������
    if(isFirst)
    {
     if((rc = WSAStartup(MAKEWORD(0x2,0x2),&wsa)))
       	{
       	*Error = WSAGetLastError();
       	return INVALID_SOCKET;
       	}
    else
        isFirst=FALSE;
    }

// ______________________
	if((s = socket(AF_INET,SOCK_STREAM,PF_UNSPEC))==INVALID_SOCKET)
		{
       	*Error = WSAGetLastError();
       	return INVALID_SOCKET;
		}
	sa.sin_family=AF_INET;
	sa.sin_port=(WORD) htons(Port);
	sa.sin_addr.s_addr = inet_addr(Addr);

rc = ioctlsocket(s, FIONBIO, &iTime); // non block socket
//iTime = 100UL;
//rc = setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char *)&iTime,sizeof(DWORD));

Timed = GetTickCount()+200UL;

    while(Timed > GetTickCount())
    {
        rc = connect(s,(SOCKADDR *) &sa,sizeof(SOCKADDR));
        *Error = WSAGetLastError();
        if(*Error==WSAEISCONN)
            {
            Timed -= GetTickCount();
            Timed -=200UL;
            break;
            }
        else
            Sleep(100UL);
    }
	if(*Error!=WSAEISCONN)
		{
   		*Error = WSAGetLastError();
   		shutdown(s,SD_BOTH);
   		closesocket(s);
   		return INVALID_SOCKET;
		}
    else{
        iTime=0;
        rc = ioctlsocket(s, FIONBIO, &iTime); // non block socket
    }

*Error = WSAGetLastError();
return s;
}

void mb_close(SOCKET s, BOOL s_only)
{
if(s_only){
    shutdown(s,SD_BOTH);
    closesocket(s);
}else
    WSACleanup();
}

INT mb_request(LPSTR adsu_ip, UINT adsu_port, BYTE adsu, BYTE fc, WORD Reg, WORD Size, PWORD Packet,PWORD data, BOOL Reverse)
{
static SOCKET s = INVALID_SOCKET;
INT rc=0,receive_bytes,Err,cc;
pmb_packet mb_p;
PBYTE bData;
PWORD wData;

DWORD KeepAlive=100UL;
WORD tid;
    if( s == INVALID_SOCKET )
        if((s = mb_connect(adsu_ip,adsu_port,&Err))==INVALID_SOCKET)
                return -1;
	srand((UINT)GetTickCount());
	tid =  (WORD) (((float)rand()) / ((float)RAND_MAX) * 255.);
	tid <<=8;
    ZeroMemory((PVOID)Packet,sizeof(mb_packet));
	mb_p = (pmb_packet) Packet;
    mb_p->id_transaction = tid;
    mb_p->adsu=adsu;
    mb_p->fc=fc;

	switch(fc)
		{
		case READ_HOLD_REGISTERS:
				  wData = bData = &mb_p->data;
  				  bData[0] = (Reg>>8);
				  bData[1] = (Reg & 0xFF);
                  bData[2] = 0;
                  bData[3] =(Size & 0x7F);
                  mb_p->packet_len = 0x0600;
				  receive_bytes =  (mb_p->packet_len>>8)+0x6;
				  if((cc = send(s,(const char *) mb_p,receive_bytes,0)) == SOCKET_ERROR)
				  	{
				  	rc = -2;
				  	s = INVALID_SOCKET;
				  	break;
				  	}
		  		setsockopt(s,SOL_SOCKET,SO_RCVTIMEO, (char *)&KeepAlive,sizeof(DWORD));
				  if((receive_bytes = recv(s,(char *) mb_p,sizeof(mb_packet),0)) < 1)
				  	{
				  	rc = -3;
				  	s = INVALID_SOCKET;
				  	break;
				  	}
				  if((mb_p->id_transaction!=tid) || ((Size<<1)!=mb_p->data))
				  	{
				  	rc = -4;
				  	break;
				  	}
//				  mb_a3 = (pmb_answ_fc3) &mb_p->data;
                ZeroMemory((PVOID)Packet,0x7F);
				  if((receive_bytes = recv(s,(char *)Packet,(Size<<1),0)) < 1)
				  	{
				  	rc = -5;
				  	s = INVALID_SOCKET;
				  	break;
				  	}
				  if(Reverse)
				       _swab((char *) Packet, (char *) Packet,receive_bytes);
                if(data!=NULL)
                    memmove((PVOID)data,(const void *)Packet,receive_bytes);
		  		  rc = (INT)(receive_bytes>>1);
		break;
		case WRITE_HOLD_REGISTERS:
                    wData = bData = &mb_p->data;
                    bData[0] = (Reg>>8);
                    bData[1] = (Reg & 0xFF);
                    bData[2] = 0;
                    bData[3] =(Size & 0x7F);
                    bData[4] = (Size & 0x7F) << 1;
                    mb_p->packet_len = 0x07+bData[4];
                    mb_p->packet_len<<=8;
                    memmove((PVOID)(bData+5),(const void *) data,(size_t)bData[4]);
                    if(Reverse)
				       _swab((char *) (bData+5), (char *) (bData+5),(size_t)bData[4]);
                    receive_bytes =  bData[4]+13;
                    if((cc = send(s,(const char *) mb_p,receive_bytes,0)) == SOCKET_ERROR)
				  	{
				  	rc = -2;
				  	s = INVALID_SOCKET;
				  	break;
				  	}
		  		setsockopt(s,SOL_SOCKET,SO_RCVTIMEO, (char *)&KeepAlive,sizeof(DWORD));
				  if((receive_bytes = recv(s,(char *) mb_p,sizeof(mb_packet)+1,0)) < 1)
				  	{
				  	rc = -3;
				  	s = INVALID_SOCKET;
				  	break;
				  	}
				  if(mb_p->id_transaction!=tid)
				  	{
				  	rc = -4;
				  	break;
				  	}
                if((receive_bytes = recv(s,(char *)Packet,2,0)) < 1)
				  	{
				  	rc = -5;
				  	s = INVALID_SOCKET;
				  	break;
				  	}
				  if(Reverse)
				       _swab((char *) Packet, (char *) Packet,receive_bytes);
                if(data!=NULL)
                    memmove((PVOID)data,(const void *)Packet,receive_bytes);
		  		  rc = (INT)(receive_bytes>>1);
		break;
		default: break;
		}
if(s == INVALID_SOCKET)
        mb_close(s,TRUE);
return rc;
}



//������ ������ � ��210_311
int write_mk210_bit(char * ipaddr, UINT8 bit,int val)
{
int rc=0;
WORD msk;
 if((rc = mb_request(ipaddr,502, 1, READ_HOLD_REGISTERS, 468, 1, (PWORD) _data,&msk, 1)) < 0)
    return rc;
 msk = rs_bit(msk, bit, (BOOL) val);
 rc=mb_request(ipaddr,502, 1, WRITE_HOLD_REGISTERS, 470, 1, (PWORD) _data,&msk, 1);
return rc;
}

// �������� ����� � ioLogik
int isLink_mk210(char * ipaddr,PWORD mask,DWORD tmr)
{
static DWORD bTimer=0UL,bNow;
static int rc=0;
int cc;

bNow = GetTickCount();

if(!bTimer)
    bTimer=bNow+tmr;

	if(bNow > bTimer)
		{
		bTimer=bNow+tmr;
		cc =  mb_request(ipaddr,502, 1, READ_HOLD_REGISTERS, 468, 1, (PWORD) _data,mask, 1);
		rc = (int)(cc > 0);
        }
return rc;
}

int _get_param(PCHAR ip_addr,PINT bell,PINT light, PINT sema, PINT zr, PBYTE number, PDWORD tmr, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
static BOOL isFirst=FALSE;
CHAR ip[0x10]={0};  // ip ����� ioLogic
PCHAR CurDir;
int rc=0,cc;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("dio","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*number=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
*bell=GetPrivateProfileInt("dio","DOBell",-1,(LPCSTR)CurDir);
*light=GetPrivateProfileInt("dio","DOLight",-1,(LPCSTR)CurDir);
*sema=GetPrivateProfileInt("dio","DOSemaphore",-1,(LPCSTR)CurDir);
*zr=GetPrivateProfileInt("dio","DOZero",-1,(LPCSTR)CurDir);
*tmr=(DWORD)GetPrivateProfileInt("dio","TMRBell",5000,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("dio","debug",0,(LPCSTR)CurDir);

if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("dio","exit",0,(LPCSTR)CurDir);

if(!isFirst)
{
	wsprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) DioString );
	printf("������������ DIO �� �����:\t%s\n",(PCHAR)CurDir);
	printf("IP ����� DIO:\t\t\t\t%s\n",(PCHAR)ip);
    printf("����� �����:\t\t\t\t%03d\n",(INT) *number);
	printf("����� ������ ������:\t\t\t%d\n",(INT) *bell);
	printf("����� ������ ����������:\t\t%d\n",(INT) *light);
	printf("����� ������ ���������:\t\t\t%d\n",(INT) *sema);
    printf("����� ������ ��������� �����:\t\t%d\n",(INT) *zr);
	printf("����� ������ ������:\t\t\t%lu ms\n", *tmr);
	printf("���������� �������:\t\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
	_closeConsole();
}
isFirst=*debug;
cc=0;
free_w(CurDir);
return rc;
}

void publish_callback(void** unused, struct mqtt_response_publish *published)
{
    UINT16 hc;
    PCHAR topic_name,val;

    topic_name = (char*) malloc_w(published->topic_name_size + 1);
    memcpy(topic_name, published->topic_name, published->topic_name_size);
    topic_name[published->topic_name_size] = '\0';
    val = (PCHAR)published->application_message;
    hc = hash_rev((PCHAR) topic_name);
//    if(!lstrcmpi((LPCSTR)topic_name,"/Scale50/HTTP/SetZero"))
//       topic_name[0]='/';
    switch(hc)
    {
        case 294: wi.Semaphore=(val[0]-'0')&1; //HTTP/TrafficLight
        break;
        case 367: wi.Light =(val[0]-'0')&1; //HTTP/Light
        break;
        case 851: if(((val[0]-'0')&1))
                    wi.ZeroKey =(val[0]-'0')&1; //HTTP/SetZero
        break;
        case 1010: if(((val[0]-'0')&1))
                    wi.Bell=(val[0]-'0')&1; //HTTP/BellOn
        break;
        default: hc=hc;
    }
    //printf("Received publish('%s'): %s\n", topic_name, (const char*) published->application_message);

    free_w(topic_name);
}

UINT16 _mkpublish(struct mqtt_client *mqc,UINT8 qos, INT8 num,WORD dio,pWEGINFO pw)
{
PCHAR tp,_wrk,topic;
PCHAR bImage;
struct tm *nt;
time_t lt;

	topic=malloc_w(0x100);
    tp=malloc_w(0x100);
    _wrk=malloc_w(0x100);
			wsprintf(topic,"/Scale%02d/DIO",num);
		    wsprintf(tp,"%s/Module",topic);
		    wsprintf(_wrk,"MK210");
		    mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

    lt =  time(NULL);
    nt = localtime(&lt);
	wsprintf(tp,"%s/Time",topic);
    wsprintf(_wrk,"%02d.%02d.%04d %02d:%02d:%02d",nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec);
    mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

		    wsprintf(tp,"%s/DO",topic);
            bImage = _getBinary((DWORD) dio, 8);
            wsprintf(_wrk,"%s",bImage);
            mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

		    wsprintf(tp,"%s/BellOn",topic);
            wsprintf(_wrk,"%u",pw->Bell);
            mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

		    wsprintf(tp,"%s/ZeroKey",topic);
            wsprintf(_wrk,"%u",pw->ZeroKey);
            mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

		    wsprintf(tp,"%s/TrafficLight",topic);
            wsprintf(_wrk,"%u",pw->Semaphore);
            mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

		    wsprintf(tp,"%s/Light",topic);
            wsprintf(_wrk,"%u",pw->Light);
            mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

		    wsprintf(tp,"%s/ioLogik",topic);
            wsprintf(_wrk,"%u",pw->lnkLogik);
            mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

free_w(_wrk);
free_w(tp);
free_w(topic);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    DWORD ttick,summ=0UL,MQTime=0UL;
    CHAR ipaddr[0x10]; // ����� ����
    BYTE num; // ����� �����
    int diobell,diolight,diosema,diozero;    //���� ��� ������
    DWORD timeout,BellBegin; // ����� ������ ������ � �������������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc;
    FileMap fm;
    PBYTE dataExchange;
    CHAR cfg[0x20];
    HANDLE isRun,hCon=NULL;
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    time_t isNow;
	struct tm *nt;
	PWORD wReg;
	WORD dReg=0xFF,doReg=0;
	BYTE refresh=0; //���������� MQTT



if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    return FALSE;

cc=_get_param((PCHAR) ipaddr,&diobell,&diolight,&diosema,&diozero,&num,&timeout,&debug,&quit,(PCHAR) cfg);
debug=0;

wsprintf((LPSTR)DioString,"Local\\Dio%03d",num);
if((isRun = _chkIsRun((LPSTR)DioString))==NULL)
    {
		wsprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) DioString );
        printf("��������� Dio -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pWi=(pWEGINFO)fm.dataPtr;

if(pWi->ID!=num)
	InterlockedExchange16((volatile SHORT *) &pWi->ID,(SHORT)num);

if((cc = mb_request(ipaddr,502, 1, READ_HOLD_REGISTERS, 468, 1, (PWORD) _data,&doReg, 1))>0)
    {
    InterlockedExchange8((PBYTE) &pWi->Bell,test_bit(doReg, (UINT8) diobell));
    InterlockedExchange8((PBYTE) &pWi->Light,test_bit(doReg, (UINT8) diolight));
    InterlockedExchange8((PBYTE) &pWi->Semaphore,test_bit(doReg, (UINT8) diosema));
    InterlockedExchange8((PBYTE) &pWi->ZeroKey,test_bit(doReg, (UINT8) diozero));
    }

// -- Check MQTT Enable
GetCurrentDirectory(0x100,DioString);
lstrcat((LPSTR)DioString,"\\");
lstrcat((LPSTR)DioString,(LPCSTR)cfg);
if(enableMQ=GetPrivateProfileInt("dio","EnableMQ",0,(LPCSTR)DioString))
    {
    GetPrivateProfileString("mqtt","ipMQ","127.0.0.1:1883",_data,0x40,(LPCSTR)DioString);
    cc = _copy_str_until((PCHAR) ip_mqtt,(PCHAR) _data, ':');
    cc = _copy_str_until((PCHAR) port_mqtt,(PCHAR) _data+cc+1, '\x0');
    sendbuf = malloc_w(0x100000);
    recvbuf = malloc_w(0x80000);
    wsprintf((LPSTR)client_id,"MQTT_Publisher_%03u",num);
    if((sockfd = open_nb_socket(ip_mqtt, port_mqtt))>0)
        {
        ms = mqtt_init(&client, sockfd, sendbuf, 0x8000, recvbuf, 0x7FFF, publish_callback);
        ms = mqtt_connect(&client, client_id, NULL, NULL, 0, NULL, NULL, connect_flags, 1);
        if(ms==MQTT_OK)
            {
            wsprintf(_subst,"/Scale%02d/HTTP/#",num);
            mqtt_subscribe(&client, _subst, 0);
            ms = mqtt_ping	(&client);
            _mkpublish(&client,(UINT8) 0, (INT8) num,doReg,(pWEGINFO) &wi);
            ms = mqtt_sync(&client);
            }
        }
    else
        enableMQ=0;
    }
// -------------------------
MQTime = GetTickCount()+250UL;
ZeroMemory((PVOID) &wi,sizeof(WEGINFO));
    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&diobell,&diolight,&diosema,&diozero,&num,&timeout,&debug,&quit,(PCHAR) cfg);
            if(cc & 0x2){
                if(debug){
					sprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) DioString );
                    printf("DIO �����:%s DIO ����:%u\n",ipaddr,502);
                }
                else
					_closeConsole();
            }
        }
    ttick = GetTickCount();
	if(debug)
		{
			hCon=GetStdHandle(STD_OUTPUT_HANDLE);
			GetConsoleScreenBufferInfo(hCon,&csbi);
		}

    cc = isLink_mk210(ipaddr,&doReg,5000UL);
    if(!enableMQ)
        InterlockedExchange8((PBYTE) &pWi->lnkLogik,(CHAR) cc);
    else
        wi.lnkLogik=cc;

    isNow=time(NULL);
    nt = localtime((const time_t *) &isNow);

    cc = mb_request(ipaddr,502, 1, READ_HOLD_REGISTERS, 468, 1, (PWORD) _data,&dReg, 1);
    if(UpDownFlag(DOCHANNEL, (UINT32) dReg))
        {
        if(debug)
            printf("\t%02d:%02d:%02d | ����:%02u | MK210:%s ��������  DIO:%sB\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,ipaddr,_getBinary((DWORD) dReg, 8) );
        wi.Bell=test_bit(dReg, (UINT8) diobell);
        wi.Light=test_bit(dReg, (UINT8) diolight);
        wi.Semaphore=test_bit(dReg, (UINT8) diosema);
        wi.ZeroKey=test_bit(dReg, (UINT8) diozero);
        refresh=1;
        }

    if(UpDownFlag(LINKCHANNEL, (UINT32) wi.lnkLogik))
        {
        if(debug)
            printf("\t%02d:%02d:%02d | ����:%02u | ����� � %s %s\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,ipaddr, wi.lnkLogik ? "�������������" : "���������");
        refresh=1;
        }

// ���������� �������
wi.Bell = TOF((UINT8)diobell, (UINT32) wi.Bell, (UINT32) timeout);
if(UpDownFlag((UINT8) diobell, (UINT32) wi.Bell))
    {
        if(debug)
            printf("\t%02d:%02d:%02d | ����:%02u | ������: %s\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,wi.Bell ? "���." : "����");
			if(diobell > -1)
	            write_mk210_bit(ipaddr,(UINT8) diobell, (BOOL) wi.Bell);
        InterlockedExchange8((PBYTE) &pWi->Bell,wi.Bell);
        refresh=1;
    }
//-------------------------------
// ���������� �����������
    if(UpDownFlag((UINT8) diolight, (UINT32) wi.Light))
        {
        if(debug)
            printf("\t%02d:%02d:%02d | ����:%02u | ���������: %s\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,wi.Light ? "���." : "����");
			if(diolight > -1)
	            write_mk210_bit(ipaddr,(UINT8) diolight, (BOOL) wi.Light);
        InterlockedExchange8((PBYTE) &pWi->Light,wi.Light);
        refresh=1;
        }
//-------------------------------

// ���������� ����������
    if(UpDownFlag((UINT8) diosema, (UINT32) wi.Semaphore))
        {
        if(debug)
            printf("\t%02d:%02d:%02d | ����:%02u | ��������: %s\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,wi.Semaphore ? "���." : "����");
			if(diosema > -1)
	            write_mk210_bit(ipaddr,(UINT8) diosema, (BOOL) wi.Semaphore);
        InterlockedExchange8((PBYTE) &pWi->Semaphore,wi.Semaphore);
        refresh=1;
        }
//-------------------------------

// ��������� �����
wi.ZeroKey = TOF((UINT8)diozero, (UINT32) wi.ZeroKey, (UINT32) 1000UL);
    if(UpDownFlag((UINT8) diozero, (UINT32) wi.ZeroKey))
        {
        if(debug)
            printf("\t%02d:%02d:%02d | ����:%02u | ��������� �����: %s\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,wi.ZeroKey ? "���." : "����");
			if(diozero > -1)
	            write_mk210_bit(ipaddr,(UINT8) diozero, (BOOL) wi.ZeroKey);
        refresh=1;
        }
//------------------------------
    Sleep(100UL);

	wsprintf((LPSTR)DioString,".dio%02d",num);
    if((GetFileAttributes(DioString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)DioString);
        quit=1;
        }
	if(debug && refresh)
		{
		ScrollScreen(hCon,csbi,15);
        SetConsoleCursorPosition(hCon,csbi.dwCursorPosition);
		}
// -- MQTT Cycle ------------
    if(enableMQ)
    {
        if((ms = mqtt_sync(&client)) != MQTT_OK)
            {
            close(sockfd);
            mqtt_disconnect (&client);
            client.inspector_callback=NULL;
            if((sockfd = open_nb_socket(ip_mqtt, port_mqtt))>0)
                {
                ms = mqtt_init(&client, sockfd, sendbuf, 0x8000, recvbuf, 0x7FFF, publish_callback);
                ms = mqtt_connect(&client, client_id, NULL, NULL, 0, NULL, NULL, connect_flags, 1);
                if(ms==MQTT_OK)
                    {
                    wsprintf(_subst,"/Scale%02d/HTTP/#",num);
                    mqtt_unsubscribe(&client, _subst);
                    mqtt_subscribe(&client, _subst, 0);
                    ms = mqtt_ping	(&client);
                    _mkpublish(&client,(UINT8) 0, (INT8) num,doReg,(pWEGINFO) &wi);
                    ms = mqtt_sync(&client);
                    }
                }
            }
        if(ttick > MQTime)
            refresh=1;
        if(refresh)
            {
            mqtt_ping(&client);
            _mkpublish(&client,(UINT8) 0, (INT8) num,dReg,(pWEGINFO) &wi);
            MQTime=ttick+5000UL;
            refresh=0;
            }
    }
    else
        memmove((void *) &wi, (const void *) pWi, (size_t) sizeof(wi));
// --------------------------
//
    }
if(!debug)
	_closeConsole();
    if(enableMQ)
        {
        close(sockfd);
        ms = mqtt_disconnect (&client);
        }
    if(sendbuf!=NULL)
        {
        free_w(recvbuf);
        free_w(sendbuf);
        }

    mb_close(0,FALSE);
	InterlockedExchange8((PBYTE) &pWi->lnkLogik,0);
	InterlockedExchange8((PBYTE) &pWi->Bell,0);
	InterlockedExchange8((PBYTE) &pWi->Light,0);
	InterlockedExchange8((PBYTE) &pWi->Semaphore,0);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // MK210_C
