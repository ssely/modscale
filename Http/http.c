#ifndef HTTP_C_
#define HTTP_C_
#define  _WIN32_WINNT  0x0501
#define  WINVER  0x0501
#define _WIN32_IE  0x0600


#include <winsock2.h>
#include <windns.h>
#include <stdio.h>
#include <string.h>
#include "..\inc\mongoose.h"
#include "..\inc\scale.h"
#include "..\inc\version.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"



PCHAR acl,Buf;
BOOL debug=FALSE; //������/������� (1/0) ����������� ����

pWEGINFO pWi=NULL;
WEGINFO wi;
long addTZ = 7*3600L;
struct mg_mgr mgr;
static const char *s_http_port;
static struct mg_serve_http_opts s_http_server_opts;

INT _CountFile(PCHAR mask)
{
WIN32_FIND_DATA ffd;
HANDLE hFind;
INT rc=0;
   hFind = FindFirstFile(mask, &ffd);
   if (hFind == INVALID_HANDLE_VALUE)
      return rc;
	rc++;

while(FindNextFile(hFind, &ffd))
		rc++;
FindClose(hFind);
return rc;
}

INT file_list(struct mg_connection *nc, int snum,char * buffer)
{
WIN32_FIND_DATA ffd;
HANDLE hFind;
INT rc=1;
PCHAR pattern;

pattern=buffer+0x100;

wsprintf(pattern,"%02d/%2.2s%2.2s%2.2s\?\?\?\?.xml",snum,buffer+6,buffer+3,buffer);
mg_printf(nc,"HTTP/1.1 200 OK\r\nContent-Type: text/xml\r\nTransfer-Encoding: chunked\r\n\r\n");
mg_printf_http_chunk(nc,"<?xml version=\"1.0\" encoding=\"windows-1251\"?>");
mg_printf_http_chunk(nc,"<Feeds_List id=\"%8.8s\">",buffer);
   hFind = FindFirstFile(pattern, &ffd);
   if (hFind == INVALID_HANDLE_VALUE)
      rc=0;
	if(rc){
			do{
				mg_printf_http_chunk(nc,"<Feed>%10.10s</Feed>",ffd.cFileName);
				rc++;
	    	}while(FindNextFile(hFind, &ffd) != 0);
		FindClose(hFind);
		}
mg_printf_http_chunk(nc,"</Feeds_List>");
mg_send_http_chunk(nc, "", 0);
return rc;
}
///
/// ### �������������� DNS ��� � IP �����
///
void make_acl(PCHAR ac,BYTE cmd)
{
LPHOSTENT	lpHostEntry;
SOCKADDR_IN saServer;
PCHAR cAcl,data=NULL;
BOOL isFirst=TRUE;

if(cmd)
    {
    lstrcpy((LPSTR)(ac+0x800),(LPCSTR)ac);
    return;
    }
    cAcl=(PCHAR)malloc_w(0x1000);
    lstrcpy((LPSTR)cAcl,(LPCSTR)(ac+0x800));

    while(TRUE)
    {
        if(isFirst)
            data=strtok(cAcl,",");
        else
            data=strtok(NULL,",");
        if(data==NULL)
            break;

        if(isdigit(data[1]))
            {   if(!isFirst)
                    {
                    lstrcat(ac,",");
                    lstrcat(ac,data);
                    }
                else{
                    lstrcpy(ac,data);
                    isFirst=FALSE;
                    }
            }

        else if((lpHostEntry=gethostbyname(data+1))!=NULL)
            {
                saServer.sin_addr = *((LPIN_ADDR)*lpHostEntry->h_addr_list);
                if(isFirst)
                    {
                    wsprintf(cAcl+0x800,"%c%u.%u.%u.%u",data[0],saServer.sin_addr.S_un.S_un_b.s_b1,saServer.sin_addr.S_un.S_un_b.s_b2,saServer.sin_addr.S_un.S_un_b.s_b3,saServer.sin_addr.S_un.S_un_b.s_b4);
                    lstrcpy(ac,cAcl+0x800);
                    isFirst=FALSE;
                    }
                else{
                    wsprintf(cAcl+0x800,",%c%u.%u.%u.%u",data[0],saServer.sin_addr.S_un.S_un_b.s_b1,saServer.sin_addr.S_un.S_un_b.s_b2,saServer.sin_addr.S_un.S_un_b.s_b3,saServer.sin_addr.S_un.S_un_b.s_b4);
                    lstrcat(ac,cAcl+0x800);
                }

            }
        else
            break;
    }
    free_w(cAcl);
}

void print_state(struct mg_connection *nc, char * buffer, pWEGINFO wi)
{
struct tm *nt;
time_t lt;
int i,cc;

lt =  time(NULL);
nt = localtime(&lt);
if(wi->TimeVT>1000U)
    lt =  time(NULL);
wsprintf(buffer,"%02d/%02u%02u%02u*.xml",wi->ID,nt->tm_year-100,nt->tm_mon+1,nt->tm_mday);
mg_printf(nc,"HTTP/1.1 200 OK\r\nContent-Type: text/xml\r\nTransfer-Encoding: chunked\r\n\r\n");
mg_printf_http_chunk(nc,"<?xml version=\"1.0\" encoding=\"windows-1251\"?>");
mg_printf_http_chunk(nc,"<RailWay_Scale_State State=\"%d\" VesID=\"%d\"",wi->isBusy,wi->ID);
mg_printf_http_chunk(nc," VagonNumber=\"%d\"",wi->isBusy ? wi->CarCount : wi->CarCount);
mg_printf_http_chunk(nc," Ves=\"%.3f\" Ves1=\"%d\" Ves2=\"%d\" Error=\"%lu\" Speed=\"%.2f\"",(float)((long)wi->WeightSUM /1000.f), wi->Weight[0], wi->Weight[1],wi->SensError,(float)wi->isSpeed / 10.f);
mg_printf_http_chunk(nc," DeviceType=\"%s\" Mode=\"%s\"",wi->devName[0] ? wi->devName : "UNKNOWN",wi->Request ? "Request" : "Send");
mg_printf_http_chunk(nc," Motion=\"%d\" \
SystemTime=\"%lu\" DateTimeStamp=\"%02d.%02d.%04d %02d:%02d:%02d:000\" CycleTime=\"%lu\" VTlink=\"%u\"",
wi->Motion,lt+addTZ,nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec,wi->mCycle,wi->isLink);
mg_printf_http_chunk(nc," VTTime=\"%u\" ZeroKey=\"%u\" BellOn=\"%u\" TrafficLight=\"%u\" Light=\"%u\" ioLogik=\"%u\" Version=\"%s\" HttpVer=\"%s\">",wi->TimeVT,wi->ZeroKey,wi->Bell,wi->Semaphore,wi->Light,wi->lnkLogik,SC_UBUNTU_FULLVERSION, MG_VERSION);
mg_printf_http_chunk(nc,"<File_List>");
mg_printf_http_chunk(nc,"<File Name=\"%02d-%02d-%02d.xml\" FeedCount=\"%d\"/>",
nt->tm_mday,nt->tm_mon+1,nt->tm_year-100,_CountFile(buffer));
for(i=0;i<3;i++)
	{
	lt-=86400UL;
	nt = localtime(&lt);
	wsprintf(buffer,"%02d/%02u%02u%02u*.xml",wi->ID,nt->tm_year-100,nt->tm_mon+1,nt->tm_mday);
	cc=_CountFile(buffer);
	if(!cc) break;
	mg_printf_http_chunk(nc,"<File Name=\"%02d-%02d-%02d.xml\" FeedCount=\"%d\"/>",
	nt->tm_mday,nt->tm_mon+1,nt->tm_year-100,cc);
	}
mg_printf_http_chunk(nc,"</File_List></RailWay_Scale_State>");
mg_send_http_chunk(nc, "", 0);
}


static void ev_handler(struct mg_connection *nc, int ev, void *p) {

WEGINFO WI;
struct http_message * hm;
char * page, * req;
int cc,num,isError=0;


hm = (struct http_message *) p;

make_acl(acl,0);

if(ev == MG_EV_ACCEPT)
	{
	// ��������� ������ ����������� �������
	cc=mg_check_ip_acl((const char *) acl, htonl(nc->sa.sin.sin_addr.S_un.S_addr));
	if(cc < 1)
		{
		nc->flags |= MG_F_SEND_AND_CLOSE;
		cc = mg_conn_addr_to_str(nc, Buf, 0x20,MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_REMOTE);
		if(debug)
            printf("%10lu | %s ���������� ���������\n",(DWORD)time(NULL),Buf);
        lstrcat(Buf," ���������� ��������� ");
        lstrcat(Buf," acl=");
        lstrcat(Buf,acl);
		WriteLogWeb((LPSTR) Buf, pWi->ID, TRUE);
		}
	return;
	}
  if (ev == MG_EV_HTTP_REQUEST) {
// ��������� ������ ����������� �������
	cc=mg_check_ip_acl((const char *) acl, htonl(nc->sa.sin.sin_addr.S_un.S_addr));
// ���� �� �������� ������, �� �� �����
	if(cc < 1)
		{
	    nc->flags |= MG_F_SEND_AND_CLOSE;
   		cc = mg_conn_addr_to_str(nc, Buf, 0x20,MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_REMOTE);
		if(debug)
            printf("%10lu | %s acl=%s ���������� ���������\n",(DWORD)time(NULL),Buf,acl);
		lstrcat(Buf," ���������� ���������");
		WriteLogWeb((LPSTR) Buf, pWi->ID, TRUE);
		return;
		}
// ��������� ����� �����
	num=atoi(hm->uri.p+1);
	if((!num) && (!hm->query_string.len)){
//���� ����� �� ��������, �� �������
	mg_http_send_error(nc, 404, (const char *)"Scale not found.");
	if(debug)
        printf("%10lu | %s Scale %u not found.\n",(DWORD)time(NULL),Buf,num);

	lstrcat(Buf," ���� �� �������");
    WriteLogWeb((LPSTR) Buf, num, TRUE);
	return;
	}
//--------------------------
// ������ ������
    Buf[0]=0;
	page=(char *)malloc_w(0x1000);
	cc = mg_conn_addr_to_str(nc, page, 0x20,MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_REMOTE);
	sprintf(Buf,"%10lu | �����:%s\t| ������:%.*s\n",(DWORD)time(NULL),page,(int) hm->uri.len,hm->uri.p);
	if(debug)
        printf(Buf);

    sprintf(Buf,"�����:%s\t������:%.*s",page,(int) hm->uri.len,hm->uri.p);
    WriteLogWeb((LPSTR) Buf, num, TRUE);

	if(hm->query_string.len)
		{
	//	cc = lstrlen(WI.Buffer+0x100)+0x100;
		sprintf(Buf,"%10lu | �����:%s\t| ������:\?%.*s\n",(DWORD)time(NULL),page,(int) hm->query_string.len,hm->query_string.p);
    	if(debug)
            printf(Buf);
        sprintf(Buf,"�����:%s\t������:\?%.*s",page,(int) hm->query_string.len,hm->query_string.p);
        WriteLogWeb((LPSTR) Buf, num, TRUE);
		}
	wsprintf(page,"/%02d/",num);
	wsprintf(page+40,"/%02d/state.xml",num);
	req=page+100;
	lstrcpyn(req,hm->uri.p+2,hm->uri.len);
	req=strtok(page+100,"/");
	req=strtok(NULL," ");
	if(req==NULL)
		req=page+100;
//---------------------
	if(!mg_vcmp(&hm->uri,page))
	    mg_serve_http(nc, hm, s_http_server_opts);
	else if(!mg_vcmp(&hm->uri,page+40))
		{
		if(num!=pWi->ID)
			{
             mg_http_send_error(nc, 404, (const char *)"Scale not found.");
             	if(debug)
                        printf("%10lu | Scale %u not found.\n",(DWORD)time(NULL),num);
			free_w(page);
			return;
			}
		print_state(nc, page, &wi);
		}
	else if((req[2]=='-')&&(req[5]=='-')&&(req[8]=='.'))
		{
		lstrcpyn(page,hm->uri.p+1,hm->uri.len);
		file_list(nc, num, req);
		}
	else if(hm->query_string.len)
		{
		if(lstrcmp(req,"scales?"))
			{
            mg_http_send_error(nc, 404, (const char *)"Command not found.");
           	if(debug)
                  printf("%10lu | Command %s not found.\n",(DWORD)time(NULL),req);
			free_w(page);
			return;
			}
		if((mg_get_http_var(&hm->query_string, "command", page, 128)) > 0)
			{
			 if(!lstrcmp(page,"takeweight"))
             {
				InterlockedExchange8((volatile PCHAR) &pWi->CarKey,1);
                WriteLogWeb((LPSTR) page, num, TRUE);
             }
			else if(!lstrcmp(page,"setzero"))
            {
				InterlockedExchange8((volatile PCHAR) &pWi->ZeroKey,1);
                WriteLogWeb((LPSTR) page, num, TRUE);
            }

			else if(!lstrcmp(page,"bell"))
            {
				InterlockedExchange8((volatile PCHAR) &pWi->Bell,1);
                WriteLogWeb((LPSTR) page, num, TRUE);
            }

			else if(!lstrcmp(page,"quit"))
            {
				InterlockedExchange8((volatile PCHAR) &pWi->Quit,1);
                WriteLogWeb((LPSTR) page, num, TRUE);
            }

			else if(!lstrcmp(page,"settrafficlight"))
				{
                WriteLogWeb((LPSTR) page, num, TRUE);
				if((mg_get_http_var(&hm->query_string, "state", page, 128)) > 0)
						InterlockedExchange8((volatile PCHAR) &pWi->Semaphore,(page[0]-0x30)&1);
				else{
		            mg_http_send_error(nc, 404, (const char *)"Command not found.");
		            if(debug)
                        printf("%10lu | Command not found.\n",(DWORD)time(NULL));
					isError=1; // ������
					}
				}
			else if(!lstrcmp(page,"setlight"))
				{
                WriteLogWeb((LPSTR) page, num, TRUE);
				if((mg_get_http_var(&hm->query_string, "state", page, 128)) > 0)
						InterlockedExchange8((volatile PCHAR) &pWi->Light,(page[0]-0x30)&1);
				else{
		            mg_http_send_error(nc, 404, (const char *)"Command not found.");
		            if(debug)
                        printf("%10lu | Command not found.\n",(DWORD)time(NULL));
					isError=1; //������
					}
				}
			 else{
				//mg_serve_http(nc, hm, s_http_server_opts);
				// ������
	            mg_http_send_error(nc, 404, (const char *)"Query error.");
	            sprintf(Buf,"Query error:%s",page);
	            WriteLogWeb((LPSTR) Buf, num, TRUE);
	            if(debug)
                    printf("%10lu | Query error.\n",(DWORD)time(NULL));
				isError=1;
				}
		    }
		// ���� ��� ������ ��� ��������� ������
		// ��������� �������� ��������, �� ������� ���������
		if(!isError)
			print_state(nc, page, &wi);

		}
	else
	    mg_serve_http(nc, hm, s_http_server_opts);

	free_w(page);
  }
}




int _get_param(PCHAR ac,PCHAR port, PCHAR list, PBYTE num, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
static BOOL isFirst=FALSE;
int rc=0,cc;
PCHAR CurDir;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
*num=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
GetPrivateProfileString("http","acl","-0.0.0.0",ac,0x400,(LPCSTR)CurDir);

addTZ=(long)GetPrivateProfileInt("logic","tzone",7,(LPCSTR)CurDir)*3600L;
/// ���������� ���������
make_acl(acl,1);

GetPrivateProfileString("http","listing","no",list,0x10,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("http","port",80,(LPCSTR)CurDir);
wsprintf(port,"%i",cc);
cc=GetPrivateProfileInt("http","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("http","exit",0,(LPCSTR)CurDir);

if(!isFirst)
{
	wsprintf(Buf,"Debug HTTP Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) Buf );
	printf("������������ HTTP �� �����: %s\n",(PCHAR)CurDir);
	printf("����� �����:\t\t%03d\n",(INT) *num);
	printf("������ ������� ACL:\t%s\n", ac);
	printf("���� HTTP �������:\t%s\n", port);
	printf("���������� ��������:\t%s\n",list);
	printf("���������� �������:\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
}
if(!(isFirst=*debug))
		_closeConsole();
free_w(CurDir);
return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
FileMap fm;
PCHAR docRoot,listing;
BYTE num;
CHAR cfg[0x20];
BOOL quit=FALSE;
int cc;
HANDLE isRun;

struct mg_connection *nc;

Buf=(PCHAR) malloc_w(0x1000);
acl=(PCHAR)malloc_w(0x1000);
s_http_port=(PCHAR)malloc_w(0x10);
listing=(PCHAR)malloc_w(0x10);
docRoot=(PCHAR)malloc_w(0x100);

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    {
    free_w(docRoot);
    free_w(listing);
    free_w(s_http_port);
    free_w(acl);
    free_w(Buf);
    return 1;
    }

cc=_get_param(acl,(PCHAR)s_http_port,listing, &num, &debug, &quit,cfg);
//debug=FALSE;

wsprintf((LPSTR)Buf,"Local\\Http%03d",num);
if((isRun = _chkIsRun((LPSTR)Buf))==NULL)
    {
		wsprintf(Buf,"Debug HTTP Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) Buf );
        printf("��������� HTTP -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    free_w(docRoot);
    free_w(listing);
    free_w(s_http_port);
    free_w(acl);
    free_w(Buf);
    return FALSE;
    }
// ������ ������� ��� ������� � Web �������
	wsprintf(docRoot,".\\%02d",num);
	if((GetFileAttributes(docRoot)==INVALID_FILE_ATTRIBUTES))
			CreateDirectory(docRoot, NULL);

// �������������� HTTP ������
  mg_mgr_init(&mgr, NULL);

  nc = mg_bind(&mgr, s_http_port, ev_handler);


  if (nc == NULL) {
    free_w(docRoot);
    free_w(listing);
    free_w(s_http_port);
    free_w(acl);
    free_w(Buf);
	CloseHandle(isRun);
    return 1;
  }

  // Set up HTTP server parameters
  mg_set_protocol_http_websocket(nc);
  s_http_server_opts.document_root = ".";  // Serve current directory
  s_http_server_opts.enable_directory_listing = listing;

pWi=(pWEGINFO)_createExchangeObj(num, &fm);

if(pWi==NULL)
{
mg_mgr_free(&mgr);
free_w(docRoot);
free_w(listing);
free_w(s_http_port);
free_w(acl);
free_w(Buf);
CloseHandle(isRun);
return 2;
}

if(!pWi->ID)
	InterlockedExchange16((PWORD) &pWi->ID,(WORD)num);

while(!quit)
{
 memcpy((PVOID) &wi,(const void *) pWi,sizeof(wi));
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param(acl,(PCHAR)s_http_port,listing, &num, &debug, &quit,cfg);
            if(cc & 0x2){
                if(debug){
					wsprintf(Buf,"Debug HTTP Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) Buf);
                    printf("HTTP ���� :%s ������� ��������:\"%s\" ����:%u\n",(PCHAR)s_http_port,listing, (unsigned short) num);
                }
                else
					_closeConsole();
            }
        }
mg_mgr_poll(&mgr, 200);
if(pWi->Quit) break;

	wsprintf((LPSTR)Buf,".http%02d",num);
	if((GetFileAttributes(Buf)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)Buf);
        quit=1;
        }
}

if(debug)
    _closeConsole();

// ���� ��������� ����������
InterlockedExchange8((PBYTE) &pWi->Quit,0);
InterlockedExchange8((PBYTE) &pWi->CarKey,0);
InterlockedExchange8((PBYTE) &pWi->ZeroKey,0);
InterlockedExchange8((PBYTE) &pWi->Bell,0);
InterlockedExchange8((PBYTE) &pWi->Semaphore,0);
InterlockedExchange8((PBYTE) &pWi->Light,0);
//--------------------------

mg_mgr_free(&mgr);
_destroyExchangeObj(num, &fm);

free_w(docRoot);
free_w(listing);
free_w(s_http_port);
free_w(acl);
free_w(Buf);
CloseHandle(isRun);
return 0;
}
#endif //HTTP_C_
