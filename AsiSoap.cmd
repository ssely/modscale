@echo off
del /Q *.obj
del /Q *.o

rem gcc -c -s -shared -m64 -D HAVE_CONFIG_H -D PCRE2_CODE_UNIT_WIDTH=8 -D PCRE2_STATIC @filelist -Wl,--subsystem,windows,--export-all-symbols -static-libgcc 

SET PROJ=AsiSoap
SET SRC=.\%PROJ%
SET INC=.\inc
SET LIB=.\lib
SET COMMON=.\common

SET CFLAGS= -fno-ident -fno-stack-protector -fomit-frame-pointer -fno-unwind-tables 
SET CFLAGS= %CFLAGS% -fno-asynchronous-unwind-tables -falign-functions=1
SET CFLAGS= %CFLAGS% -mpreferred-stack-boundary=4 -falign-jumps=1 -falign-loops=1

rem SET CFLAGS=

rem SET CFLAGS= %CFLAGS% -nostdlib
rem -nostartfiles -nodefaultlibs -nostdlib 

if [%1] == [] (
@echo "Build Release..."
call mkres.cmd  %PROJ%64X
gcc -s -m64 -O2 -I%INC% -L%LIB% %CFLAGS%  -o %PROJ%64X.exe %SRC%\%PROJ%.c %COMMON%\misc.c  resource.res -Wl,--subsystem,windows,--file-alignment,512 -lws2_32 -lkernel32 -luser32 -lmsvcrt -lwininet >compile.log
) else (
@echo "Build Debug..."
gcc -g -m64 -O2 -I %INC% -L %LIB% %CFLAGS% -DMULTICORE -o %PROJ%64D.exe %SRC%\%PROJ%.c %COMMON%\misc.c -Wl,--subsystem,windows,--file-alignment,512 -lws2_32 -lkernel32 -luser32 -lmsvcrt -lwininet >compile.log
)
for /f "tokens=1" %%a in ('dir /b "compile.log"' ) do @set FSIZE=%%~za
if [%FSIZE%] == [0] (
call version.cmd
copy /b /y version.h .\inc\version.h
rem copy /b /y scale64X.exe scale64X.new
)