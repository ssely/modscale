#ifndef MISC_C
#define MISC_C

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "..\inc\scale.h"

#define BUF_SIZE 0x1000
#define HEIGHT 30
#define WIDTH	114


PCHAR szName="Global\\ExchangeFileMappingObject";
CHAR Buffer[0x1000];

PVOID _createExchangeObj(int ScaleNum, pFileMap fm)
{
wsprintf(Buffer,"%s_%u",szName,ScaleNum);

fm->hMapping=OpenFileMapping(FILE_MAP_READ|FILE_MAP_WRITE, FALSE,Buffer);
if(fm->hMapping==NULL)
	fm->hMapping=CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,BUF_SIZE,Buffer);
if(fm->hMapping==NULL)
	return NULL;
fm->dataPtr = (LPBYTE) MapViewOfFile(fm->hMapping,FILE_MAP_ALL_ACCESS,0,0,BUF_SIZE);

if(fm->dataPtr == NULL)
   {
      CloseHandle(fm->hMapping);
      return NULL;
   }
return (PVOID) fm->dataPtr;
}

void _destroyExchangeObj(int ScaleNum, pFileMap fm)
{
UnmapViewOfFile(fm->dataPtr);
CloseHandle(fm->hMapping);
}

BOOL _chkConfig(LPSTR mask)
{
static FILETIME ft={0};
FILETIME ftWrite;
WIN32_FIND_DATA findData;
HANDLE f;

f=FindFirstFile(mask,&findData);
FindClose(f);

if(( f = CreateFile((LPCTSTR) findData.cFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL))==INVALID_HANDLE_VALUE)
		return FALSE;
	if(!GetFileTime(f, NULL, NULL, &ftWrite))
		{
		CloseHandle(f);
		return FALSE;
		}
if((ftWrite.dwLowDateTime == ft.dwLowDateTime) && (ftWrite.dwHighDateTime == ft.dwHighDateTime))
		{
		CloseHandle(f);
		return FALSE;
		}

ft.dwLowDateTime=ftWrite.dwLowDateTime;
ft.dwHighDateTime=ftWrite.dwHighDateTime;
CloseHandle(f);
return TRUE;
}

int _copy_str_until(PCHAR dst,PCHAR src, CHAR chr)
{
 int i, rc=0;
        for(i=0;i<64; i++)
        {
            if((src[i]==chr) || (!src[i]))
            {
                dst[i]=0;
                rc=i;
                break;
            }
         dst[i]=src[i];
        }
return rc;
}

int  _find_cmd_variable(PCHAR data, PCHAR var, PCHAR value)
{
int Idx=0,rc=0,sLen;
CHAR test[0x20];
if(*((char *)data))
	{
//--���� ����, �� ������ �� ���� �� ��������� ������--
	if(*((char *)data)=='/' || *((char *)data)=='-' || *((char *)data)=='\\')
		Idx++;
    sLen=lstrlen((LPCSTR) var);
    lstrcpyn((LPSTR) test,(LPCSTR) data+Idx,sLen+1);
	if(!lstrcmpi((LPCSTR) (test), (LPCSTR) var))
        {
        Idx+=sLen;
        if(data[Idx]=='=')
            rc=_copy_str_until(value,data+Idx+1,' ');
        }
	}
return rc;
}

void _openConsole(LPSTR head)
{
HANDLE hsoh;
CONSOLE_FONT_INFOEX cfi;
SMALL_RECT conSize={WIDTH-1,HEIGHT-1};
COORD bufSize={WIDTH,HEIGHT};

                    AllocConsole();
                    freopen("CONOUT$","w",stdout);
                    SetConsoleOutputCP(1251);
                    SetConsoleTitle(head);
					hsoh=GetStdHandle(STD_OUTPUT_HANDLE);
                    SetConsoleTextAttribute(hsoh, FOREGROUND_GREEN | /*FOREGROUND_BLUE |*/ FOREGROUND_INTENSITY /*| FOREGROUND_RED*/);
			        SetConsoleWindowInfo(hsoh,TRUE,&conSize);
					SetConsoleScreenBufferSize(hsoh,bufSize);
					DeleteMenu(GetSystemMenu(GetConsoleWindow(),FALSE),SC_CLOSE,MF_BYCOMMAND);
					cfi.cbSize = sizeof(cfi);
				    cfi.nFont = 0;
				    cfi.dwFontSize.X = 10;
				    cfi.dwFontSize.Y = 26;
				    cfi.FontFamily = FF_DONTCARE;
				    cfi.FontWeight = FW_BOLD;
					lstrcpy((LPSTR) cfi.FaceName, "Lucida Console");
//					lstrcpy((LPSTR) cfi.FaceName, "Verdana");
					SetCurrentConsoleFontEx(hsoh, FALSE, &cfi);

}
void _closeConsole(void)
{
					fclose(stdout);
                    FreeConsole();

}
#endif // MISC_C
