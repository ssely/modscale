#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"



CHAR PolString[0x100];

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
    {
        cc = i+1;
        break;
    }
return cc;
}

int _isMove(long Summ, long limit, DWORD timeout)
{
static BOOL isFirst=TRUE;
static DWORD to;
static long Sm;
DWORD isNow;
BOOL Interval;
int cc=0;
if(isFirst)
	{
	to=GetTickCount();
	Sm=Summ;
	isFirst=FALSE;
	}
isNow =  GetTickCount()-to;
Interval = (isNow > timeout);
if(abs(Sm-Summ) > limit)
	{
	if(!Interval)
		cc=1;
	to=GetTickCount();	    
	Sm=Summ;
	}
return cc;
}

int _get_param(PCHAR ip_addr,int* dataport,PDWORD timeout, PBYTE num, BOOL* debug, BOOL* quit, PDWORD lim,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
PCHAR CurDir;
/*
int cmdport;    //���� ��� ������ ������ �� ������ VT220
int dataport;  // ���� ��� ��������� ������ � VT220
DWORD timeout; //������� �������� ������ � ����� � �������������
DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
BYTE num;   //����� ������������ �����
BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;
CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);

if(!isFirst)
{
	wsprintf(PolString,"Debug VT220 Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) PolString );
	printf("������������ VT220 Auto �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t %s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
	_closeConsole();
}
isFirst=*debug;
free_w(CurDir);
return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    DWORD ttick,summ=0UL,isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ VT220
    int dataport;  // ���� ��� ��������� ������ � VT220
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i,ErrorLink=0;
    BYTE Idx,prn=0,pkt=0x30;
    FileMap fm;
    pWEGINFO wi;
    long weg_1,weg_2;
    PBYTE dataExchange;
    CHAR cfg[0x20];
	HANDLE isRun;


    cc=UDP_Close();
    cc=UDP_Init();
    cc=UDP_OpenReceive();



if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    {
    cc=UDP_Close();
    return FALSE;
    }


cc=_get_param((PCHAR) ipaddr,&dataport,&timeout,&num,&debug,&quit,&limit,cfg);
debug=0;

wsprintf((LPSTR)PolString,"Local\\VT220Pol%03d",num);
if((isRun = _chkIsRun((LPSTR)PolString))==NULL)
    {
		wsprintf(PolString,"Debug VT220 Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) PolString );
        printf("��������� VT220Pol -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
wi=(pWEGINFO)fm.dataPtr;

if(wi->ID!=num)
	InterlockedExchange16((PWORD) &wi->ID,(WORD)num);

lstrcpy(wi->devName,"VT-220");

    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&dataport,&timeout,&vtnum,&debug,&quit,&limit,cfg);
            if(cc & 0x2){
                if(debug){
					sprintf(PolString,"Debug VT220 Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) PolString );
                    printf("MOXA �����:%s MOXA ���� ������:%u\n",ipaddr,dataport);
                }
                else
					_closeConsole();
            }
        }
        ttick=GetTickCount();
        Idx=0;
        i=0;
        PolString[Idx]=0;
        while((GetTickCount()-ttick) < timeout)
        {
            cc=UDP_Receive(dataport,36,(PVOID) (PolString+Idx));

            if(cc>0)
            {
             Idx+=cc;
             PolString[Idx]=0;
            }
            if(Idx>36)
                    Idx=0;
            i=_find_chr((PBYTE) PolString, '\r');
            if(i==18)
            {
                prn++;
                weg_1=atol(PolString+1);
				weg_2=atol(PolString+10);
				InterlockedExchange((LONG *) &wi->Weight[0],weg_1); //��������� ����� �� ���������� 19-22
				InterlockedExchange((LONG *) &wi->Weight[1],weg_2); //��������� ����� �� ���������� 19-22
				InterlockedExchange((LONG *) &wi->WeightSUM,(weg_1+weg_2)); // ��������� ��������� ����� 23-24
                break;
            }

        }
        ttick=GetTickCount()-ttick;
        summ+=ttick;
		isNow=time(NULL);
        if(prn > 100)
        {
		summ/=100;
		InterlockedExchange16((PWORD) &wi->TimeVT,(WORD)summ);
            if(debug)
                printf("%lu | ����:%02u | ���������:%+07li | %+07li | %08lu | ������� ������: %u ms\n",isNow,num,wi->Weight[0],wi->Weight[1],wi->WeightSUM,(int)summ);
            summ=0UL;
            prn=0;
        }
	if(ttick > (timeout>>1))
		{
        if(debug)
            printf("%lu | ����:%02u | �������:%lu ms\n",isNow,num,ttick);
		ErrorLink++;
		}
	else{
   		ErrorLink=0;
		}
		InterlockedExchange((LONG *) &weg_1,wi->WeightSUM);
		InterlockedExchange8((PBYTE) &wi->Motion,_isMove((long) weg_1, 10UL, 500UL));
		if(ErrorLink<10)
            InterlockedExchange8((PBYTE) &wi->isLink,1);
        else
            InterlockedExchange8((PBYTE) &wi->isLink,0);
		if((long)wi->WeightSUM > (long)limit)
            InterlockedExchange8((PBYTE) &wi->isBusy,1);
        else
            InterlockedExchange8((PBYTE) &wi->isBusy,0);
    Sleep(10UL);

	wsprintf((LPSTR)PolString,".vtpol%02d",num);
    if((GetFileAttributes(PolString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)PolString);
        quit=1;
        }
    }
if(!debug)
	_closeConsole();

	InterlockedExchange8((PBYTE) &wi->isLink,0);
	cc=UDP_Close();
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
