#ifndef FMAP_C
#define FMAP_C

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "..\inc\scale.h"

#define BUF_SIZE 0x1000


PCHAR szName="Global\\ExchangeFileMappingObject";
CHAR Buffer[0x1000];

PVOID _createExchangeObj(int ScaleNum, pFileMap fm)
{
wsprintf(Buffer,"%s_%u",szName,ScaleNum);

fm->hMapping=OpenFileMapping(FILE_MAP_READ|FILE_MAP_WRITE, FALSE,Buffer);
if(fm->hMapping==NULL)
	fm->hMapping=CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,BUF_SIZE,Buffer);
if(fm->hMapping==NULL)
	return NULL;
fm->dataPtr = (LPBYTE) MapViewOfFile(fm->hMapping,FILE_MAP_ALL_ACCESS,0,0,BUF_SIZE);

if(fm->dataPtr == NULL)
   {
      CloseHandle(fm->hMapping);
      return NULL;
   }
return (PVOID) fm->dataPtr;
}

void _destroyExchangeObj(int ScaleNum, pFileMap fm)
{
UnmapViewOfFile(fm->dataPtr);
CloseHandle(fm->hMapping);
}

BOOL _chkConfig(LPSTR mask)
{
static FILETIME ft={0};
FILETIME ftWrite;
WIN32_FIND_DATA findData;
HANDLE f;

f=FindFirstFile(mask,&findData);
FindClose(f);

if(( f = CreateFile((LPCTSTR) findData.cFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL))==INVALID_HANDLE_VALUE)
		return FALSE;
	if(!GetFileTime(f, NULL, NULL, &ftWrite))
		{
		CloseHandle(f);
		return FALSE;
		}
if((ftWrite.dwLowDateTime == ft.dwLowDateTime) && (ftWrite.dwHighDateTime == ft.dwHighDateTime))
		{
		CloseHandle(f);
		return FALSE;
		}

ft.dwLowDateTime=ftWrite.dwLowDateTime;
ft.dwHighDateTime=ftWrite.dwHighDateTime;
CloseHandle(f);
return TRUE;
}

int _copy_str_until(PCHAR dst,PCHAR src, CHAR chr)
{
 int i, rc=0;
        for(i=0;i<64; i++)
        {
            if((src[i]==chr) || (!src[i]))
            {
                dst[i]=0;
                rc=i;
                break;
            }
         dst[i]=src[i];
        }
return rc;
}

int  _find_cmd_variable(PCHAR data, PCHAR var, PCHAR value)
{
int isSearch=0,Idx=0,rc=0,sLen;
CHAR test[0x20];
if(*((char *)data))
	{
//--���� ����, �� ������ �� ���� �� ��������� ������--
	if(*((char *)data)=='/' || *((char *)data)=='-' || *((char *)data)=='\\')
		Idx++;
    sLen=lstrlen((LPCSTR) var);
    lstrcpyn((LPSTR) test,(LPCSTR) data+Idx,sLen+1);
	if(!lstrcmpi((LPCSTR) (test), (LPCSTR) var))
        {
        Idx+=sLen;
        if(data[Idx]=='=')
            rc=_copy_str_until(value,data+Idx+1,' ');
        }
	}
return rc;
}
#endif // FMAP_C
