@echo off
setlocal disabledelayedexpansion

set PERFIX=SC_
set CONST_CHAR=static const char
set CONST_LONG=static const long
set DAY=%date:~0,2%
set MON=%date:~3,2%
set YEAR=%date:~6,4%

FOR /F "tokens=1,2,3,4,5,6,7,8,9,10,11 delims=:" %%i in (version.inf) do (
set MAJOR=%%i
set MAJOR_VAR=%%j
set MINOR=%%k
set MINOR_VAR=%%l
set BUILD=%%m
set BUILD_VAR=%%n
set REV=%%o
set REV_VAR=%%p
set BUILD_COUNT=%%q
set BUILD_COUNT_VAR=%%r
set RELEASE=%%s
)
rem @echo :%MAJOR%-%MAJOR_VAR%-%MINOR%-%MINOR_VAR%-%BUILD%-%BUILD_VAR%-%REV%-%REV_VAR%:%BUILD_COUNT%-%BUILD_COUNT_VAR%-%RELEASE%:
rem exit

set /a "BUILD_COUNT_VAR+=1"

if [%BUILD_COUNT_VAR%%%100] == [0] (
set /a "BUILD_VAR+=1"
)

if [%BUILD_VAR%] == [0] set RELEASE=a
if [%BUILD_VAR%] == [1] set RELEASE=b
if [%BUILD_VAR%] == [2] set RELEASE=rc
if [%BUILD_VAR%] == [3] set RELEASE=r

if [%BUILD_COUNT_VAR%] == [9999] (
set BUILD_COUNT_VAR=0
)

if [%BUILD_COUNT_VAR%%%10] == [0] (
set /a "REV_VAR+=1"
)

set Compare=0
if [%RELEASE%] == [a] set Compare=1
if [%RELEASE%] == [b] set Compare=1
if [%RELEASE%] == [rc] set Compare=1

if [%Compare%] == [1] (
	if [%REV_VAR%%%2] == [0] (
		set /a "REV_VAR+=1"		
	)
) else (
	if [%REV_VAR%%%2] == [1] (
		set /a "REV_VAR+=1"		
	)
)

@echo :%MAJOR%:%MAJOR_VAR%:%MINOR%:%MINOR_VAR%:%BUILD%:%BUILD_VAR%:%REV%:%REV_VAR%:%BUILD_COUNT%:%BUILD_COUNT_VAR%:%RELEASE%: >version.inf

@echo.#ifndef VERSION_H > version.h
@echo.#define VERSION_H >> version.h
@echo.   >> version.h
@echo.	//Date Version Type >> version.h
@echo.		%CONST_CHAR% %PERFIX%DATE[] = "%DAY%"; >> version.h
@echo.		%CONST_CHAR% %PERFIX%MONTH[] = "%MON%"; >> version.h
@echo.		%CONST_CHAR% %PERFIX%YEAR[] = "%YEAR%"; >> version.h
@echo.		%CONST_CHAR% %PERFIX%UBUNTU_VERSION_STYLE[] = "%DAY%.%MON%"; >> version.h
@echo.		%CONST_CHAR% %PERFIX%UBUNTU_TIME_STYLE[] = "%time:~0,8%.%time:~9,3%"; >> version.h
@echo.		%CONST_CHAR% %PERFIX%UBUNTU_FULLVERSION[] = "%MAJOR_VAR%.%MINOR_VAR%-%RELEASE%%REV_VAR% Build %BUILD_COUNT_VAR% (%DAY%.%MON%.%YEAR% %time:~0,8%.%time:~9,3%)"; >> version.h
@echo.   >> version.h
@echo.	//Software Status  >> version.h

if [%RELEASE%] == [rc] (
@echo.		%CONST_CHAR% %PERFIX%STATUS[] = "Release Candidate"; >> version.h
@echo.		%CONST_CHAR% %PERFIX%STATUS_SHORT[] = "rc"; >> version.h
)

if [%RELEASE%] == [r] (
@echo %CONST_CHAR% %PERFIX%STATUS[] = "Release"; >> version.h
@echo %CONST_CHAR% %PERFIX%STATUS_SORT[] = "r"; >> version.h
)

@echo.   >> version.h
@echo.	//Standard Version Type  >> version.h
@echo.		%CONST_LONG% %PERFIX%%MAJOR% = %MAJOR_VAR%; >> version.h
@echo.		%CONST_LONG% %PERFIX%%MINOR% = %MINOR_VAR%; >> version.h
@echo.		%CONST_LONG% %PERFIX%%BUILD% = %BUILD_VAR%; >> version.h
@echo.		%CONST_LONG% %PERFIX%%REV% = %REV_VAR%; >> version.h

@echo.   >> version.h
@echo.	//Miscellaneous Version Type  >> version.h
@echo.		%CONST_LONG% %PERFIX%%BUILD_COUNT% = %BUILD_COUNT_VAR%; >> version.h
@echo.		#define %PERFIX%RC_FILEVERSION %MAJOR_VAR%,%MINOR_VAR%,%BUILD_VAR%,%REV_VAR% >> version.h
@echo.		#define %PERFIX%RC_FILEVERSION_STRING "%MAJOR_VAR%,%MINOR_VAR%,%BUILD_VAR%,%REV_VAR%\0" >> version.h
@echo.		%CONST_CHAR% %PERFIX%FULLVERSION_STRING[] = "%MAJOR_VAR%.%MINOR_VAR%.%BUILD_VAR%.%REV_VAR%"; >> version.h

@echo.   >> version.h
@echo.	//These values are to keep track of your versioning state, don't modify them.  >> version.h
@echo.		%CONST_LONG% %PERFIX%BUILD_HISTORY = %BUILD_COUNT_VAR%; >> version.h
@echo.   >> version.h
@echo.#endif //VERSION_H   >> version.h