#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"
#include "..\curl\curl.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define VT_WEIGHT 1
#define VT_ZERO 2


CHAR ReqString[0x100];

static const char *url = "/soap/Iscales_controllerserver_host";
static const char *urn = "urn:scales_controllerserver_hostIntf-Iscales_controllerserver_host#";
static const char *wrapper ="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" \
xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\" \
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> <SOAP-ENV:Header/> \
<SOAP-ENV:Body>  <m:%s xmlns:m=\"urn:scales_controllerserver_hostIntf-Iscales_controllerserver_host#%s\">  \
<m:name xsi:type=\"xsd:string\">Test ASI - Kemska Volost</m:name></m:%s> </SOAP-ENV:Body></SOAP-ENV:Envelope>";
/*
static const char *wrapint ="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" \
xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\" \
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> <SOAP-ENV:Header/> \
<SOAP-ENV:Body>  <m:%s xmlns:m=\"urn:scales_controllerserver_hostIntf-Iscales_controllerserver_host#%s\">  \
<m:name xsi:type=\"xsd:int\">%s</m:name></m:%s> </SOAP-ENV:Body></SOAP-ENV:Envelope>";
*/

DWORD writefunc(void *ptr, size_t size, size_t nmemb, PVOID buf)
{
  size_t new_len,str_end,Idx;
  PCHAR cBuf;

    cBuf=(PCHAR)buf;
    str_end = (size_t)(cBuf+lstrlen(cBuf));
    new_len =  str_end + size*nmemb;
    Idx = (new_len-(size_t)cBuf);
  if( Idx >= 0x1000)
        return 0;
  memcpy((PVOID) str_end, (const void *)ptr, size*nmemb);
  cBuf[Idx] = '\0';
  return (size*nmemb);
}

long _getVariable(PCHAR ip,int port, PCHAR url, PCHAR wrap, PCHAR Var,DWORD tout)
{
CURL *curl;
CURLcode ccode;
PCHAR Request,Post;
long cc=-1L;
int i,j,s=0,l,m=1,Mode;

            curl_global_init(CURL_GLOBAL_ALL);
            curl = curl_easy_init();
            Request = malloc_w(0x1000);
            *Request=0;
            if(curl)
                {
                curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
                curl_easy_setopt(curl, CURLOPT_USERAGENT, "Hubba=Bubba PCLab-agent/17.11.21");
                wsprintf(Request,"http://%s:%lu%s",ip,(DWORD)port,url);
                curl_easy_setopt(curl, CURLOPT_URL,Request);

                Post = malloc_w(0x1000);
                wsprintf(Post,wrapper,Var,Var,Var);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS,Post);

                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);

                *Request=0;
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, Request);
                curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, tout);

                ccode = curl_easy_perform(curl);
                curl_easy_cleanup(curl);

                free_w(Post);
                }
            if(ccode==CURLE_OK)
                {
                i = lstrlen(Request);
                j = i-(i>>1);
                l = lstrlen("</return");
                i -= l;
                    for(i;i>=j;i--)
                    {
                    *(Request+i+l)=0;
                    if(!lstrcmpi((Request+i),"</return"))
                        {

                        if(isdigit(Request[i-1]))
                            Mode=1;
                        if(Request[i-1]=='e')
                            Mode=2;

                        s=i-1;
                        break;
                        }
                    }
                }
            if(s)
            {
            cc=0;
            while(Mode==1)
                {
                if(isdigit(Request[s]))
                    {
                    cc+=(long)((Request[s]-0x30)*m);
                    m*=10;
                    }
                if((Request[s]=='>') || (Request[s]==' '))
                    break;
                if(Request[s]=='-')
                    {
                    cc*=-1;
                    break;
                    }
                if((--s)<=j)
                    break;
                }
            while(Mode==2)
                {
                s-=4;
                if(Request[s]=='f')                    {
                    cc=1L;
                    break;
                    }
                s++;
                if(Request[s]=='t')
                    {
                    cc=2L;
                    break;
                    }
                }
            }

curl_global_cleanup();
free_w(Request);
return cc;
}

int _isMove(long Summ, long limit, DWORD timeout)
{
static BOOL isFirst=TRUE;
static DWORD to;
static long Sm;
DWORD isNow;
BOOL Interval;
int cc=0;
if(isFirst)
	{
	to=GetTickCount();
	Sm=Summ;
	isFirst=FALSE;
	}
isNow =  GetTickCount()-to;
Interval = (isNow > timeout);
if(abs(Sm-Summ) > limit)
	{
	if(!Interval)
		cc=1;

	to=GetTickCount();
	Sm=Summ;
	}
return cc;
}

int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];
/*
int cmdport;    //���� ��� ������ ������ �� ������ VT220
int dataport;  // ���� ��� ��������� ������ � VT220
DWORD timeout; //������� �������� ������ � ����� � �������������
DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
BYTE num;   //����� ������������ �����
BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
*cmdport=GetPrivateProfileInt("scale","cmdport",8001,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
*vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ AsiSoap �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
//	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
    printf("������� �������� �������:\t%lu ms\n", *cmdtimeout);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
//	printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
return rc;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
	    {
    	    cc = i+1;
        	break;
	    }
return cc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD ttick,summ=0UL;
    time_t isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ VT220
    int dataport;  // ���� ��� ��������� ������ � VT220
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i=0,ErrorLink=0;
    BYTE lock=0;
	struct tm *nt;
    FileMap fm;
	pWEGINFO wi;
	DWORD period;
    long weg_1;
    PBYTE dataExchange;
    CHAR cfg[0x40];
	HANDLE isRun;
    long ccode;




if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug, &quit,&limit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Global\\AsiSoap%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug AsiSoap Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� AsiSoap -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
wi=(pWEGINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(wi->ID!=num)
	InterlockedExchange16((PWORD) &wi->ID,(WORD)num);

	InterlockedExchange16((volatile PWORD) &wi->VT_Num,(WORD)vtnum);
	InterlockedExchange8((volatile PCHAR) &wi->Request,1);
summ=0UL;
while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug,&quit,&limit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug AsiSoap Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("ASI �����:%s ASI ��������� ����:%u\n",ipaddr,cmdport);
                    }
                else
					_closeConsole();
                }
            }

        ttick=GetTickCount();
        while((GetTickCount()-ttick) < timeout)
        {

            ErrorLink=0;
            if((ccode = _getVariable(ipaddr,cmdport,url,wrapper, "Weight",(DWORD)(timeout>>2)))==-1L)
                ErrorLink|=1;
            else
                InterlockedExchange((volatile LONG *) &wi->WeightSUM,ccode);

            if((ccode = _getVariable(ipaddr,cmdport,url,wrapper, "WeightLeftBogie",(DWORD)(timeout>>2)))==-1L)
                ErrorLink|=1;
            else
                InterlockedExchange((volatile LONG *) &wi->Weight[0],ccode);

            if((ccode = _getVariable(ipaddr,cmdport,url,wrapper, "WeightRightBogie",(DWORD)(timeout>>2)))==-1L)
                ErrorLink|=1;
            else
                InterlockedExchange((volatile LONG *) &wi->Weight[1],ccode);

            if((ccode = _getVariable(ipaddr,cmdport,url,wrapper, "Motion",(DWORD)(timeout>>2)))==-1L)
                ErrorLink|=1;
            else
                InterlockedExchange8((PBYTE) &wi->Motion,(BYTE)(ccode >> 1));
        period=(GetTickCount()-ttick);
        summ+=period;

        if(cmdtimeout>period )
            period = cmdtimeout-period;
        else
            period = 20L;

        Sleep(period);
        isNow=time(NULL);

            if((!(isNow % 10)) && (!lock))
            {
                summ/=10UL;
                InterlockedExchange16((PWORD) &wi->TimeVT,(WORD)summ);
                if(debug)
                    {
					nt = localtime(&isNow);
					printf("%02d:%02d:%02d | ����:%03u | ���������:%03u | ���������:%05d | ������� ������: %u ms\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,i,(i<2) ?(long)wi->Weight[i] : (long)wi->WeightSUM,(int)summ);
//					printf("%lu | ����:%03u | ���������:%03u | ���������:%d | ������� ������: %u ms\n",isNow,num,i,(i<2) ?(long)wi->Weight[i] : (long)wi->WeightSUM,(int)summ);
                    if((++i) > 2) i=0;
                    }
                summ=0UL;
                lock=1;
            }
            else
                lock=0;

            if(ErrorLink)
                InterlockedExchange8((PBYTE) &wi->isLink,0);
            else
                {
                InterlockedExchange8((PBYTE) &wi->isLink,1);
                break;
                }


        }
    ttick = GetTickCount()-ttick;
	if( ttick > (timeout>>1))
		if(debug)
			{
			nt = localtime(&isNow);
            printf("%02d:%02d:%02d | ����:%03u | �������:%lu ms\n",nt->tm_hour,nt->tm_min,nt->tm_sec,num,ttick);
			}

//		InterlockedExchange((volatile LONG *) &weg_1,wi->WeightSUM);
//		InterlockedExchange8((PBYTE) &wi->Motion,_isMove((long) weg_1, 10UL, 500UL));

		if((long)wi->WeightSUM > (long)limit)
            InterlockedExchange8((PBYTE) &wi->isBusy,1);
        else
            InterlockedExchange8((PBYTE) &wi->isBusy,0);

		if(wi->ZeroKey)
			{
            ccode = _getVariable(ipaddr,cmdport,url,wrapper, "SetZero",(DWORD)(timeout>>2));
			Sleep(cmdtimeout);
            InterlockedExchange8((volatile PBYTE) &wi->ZeroKey,0);
			}

	}

if(!debug)
		_closeConsole();

	InterlockedExchange8((PBYTE) &wi->isLink,0);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
