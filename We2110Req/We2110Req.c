///
/// <meta lang="ru" charset="windows-1251" emacsmode='-*- markdown -*-'>
///

#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#define COF8
#define INTERVAL 5UL

#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"


#pragma GCC diagnostic ignored "-Wmisleading-indentation"


#define VT_WEIGHT 1
#define VT_ZERO 2

/// # ������
///
/// ������ ������ ������������ ��� ������ � �������� �������� WE21xx
/// �� 485 ���������� � ������� �������.
/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// ����������� � �� �������������� �� �� �����
///
/// ************************************************************************
/// *                            .-.
/// *                         .-+   |
/// *                     .--+       '---.                   .-----------.
/// *                    | ���� ��������� |                 |    WE21xx   |
/// *                     '--------------'                   '-----------'
/// *                          ^   ^                               ^
/// *                          |   |                               |
/// *  .------.   Ethernet     |   | Ethernet  .---------.  RS-485 |
/// *  |  ��  |<--------------'	    '-------->| MOXA-5150 |<-------'
/// *  '------'                                '---------'
/// *
/// *
/// ************************************************************************


/// ## �������������
///
/// ������ ������ ������������ �� ��������� ������
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
///  We21R64X.exe -conf=���_ini_�����
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// � __ini__ ����� ������ ������������ ������ __[scale]__.
/// ������ ������ ������� ����:
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// ip="10.9.2.100"
/// dataport=4001
/// cmdport=4011
/// timeout=2000
/// cmdtimeout=100
/// num=1
/// vtnum=1
/// limit=1000
/// dbgtime=1
/// debug=1
/// exit=0
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
///
/// �������� � ������ | ��������
/// ------------------|--------------------------------------------------------------
/// __ip__            | ����� ���������� ���������� MOXA/IDS-448
/// __dataport__      | UDP ���� ����� ������� ������� ��� � ��
/// __cmdport__       | UDP ���� ����� ������� ���������� ������� � �������� �������
/// __timeout__       | ������� �������� ������ � ����� � �������������
/// __cmdtimeout__    | ������� ������ ������ �� ������� ������ � �������������
/// __num__           | ����� ������������ �����
/// __vtnum__         | ����� ������� WE21xx ��� ������ �� �������
/// __limit__         | ��� (��) ��� ���������� �������� ���� ��������� ��������.
/// __dbgtime__       | �������� ������ (���) ���������� ���������� ���� ���������� ���� debug
/// __debug__         | ������/������� (1/0) ����������� ����
/// __exit__          | 1 - ���������� ������ ������
///
CHAR ReqString[0x100];

/// ## __�������__
///
///
/// ### _������� �������������� ������ �� ������� WE21xx_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// long _str2long(PBYTE Buffer)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|-------------------------------------------------------------------------------------
/// __Buffer__  | ��������� �� ����� ������ �� ������� WE21��
///
/// __����������__: ��������� ������� WE21xx � ���� �������� ������
///

long _str2long(PBYTE Buffer)
{
long rc=0, pw=10,sign=1;
int i=0;
char cc;

    while((Buffer[i]!=',') && (Buffer[i]!=0xD) && (Buffer[i]!=0xA) && (Buffer[i]))
        {
        cc=Buffer[i++];
        if(cc == '-')
            {
            sign*=-1L;
            continue;
            }
        if(isdigit(cc))
            {
            rc*=pw;
            rc+=(cc-0x30);
            }
        }
return (rc*sign);
}

///
/// ### _������� ���������� ���������� ��������� ������� WE21xx �� ini �����_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim, PDWORD dt,PCHAR IniFile)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// �������� � ������ | ��������
/// ------------------|--------------------------------------------------------------
/// __ip_addr__       | ����� ���������� ����������� MOXA/IDS-448
/// __cmdport__       | UDP ���� ����� ������� ���������� ������� � �������� �������
/// __dataport__      | UDP ���� ����� ������� ������� ��� � ��
/// __timeout__       | ������� �������� ������ � ����� � �������������
/// __cmdtimeout__    | ������� ������ ������ �� ������� ������ � �������������
/// __num__           | ����� ������������ �����
/// __vtnum__         | ����� ������� WE21xx ��� ������ �� �������
/// __debug__         | ������/������� (1/0) ����������� ����
/// __exit__          | 1 - ���������� ������ ������
/// __lim__           | ��� (��) ��� ���������� �������� ���� ��������� ������.
/// __dt__            | �������� ������ (���) ���������� ���������� ���� ���������� ���� debug
/// __IniFile__       | ��� ini ����� ��� ��������� �����
///
/// __���������__ ������������� ���������� ���������� �� _ini_ �����, _��. &sect;1.1_
///

int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim,PDWORD dt,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];

static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
*cmdport=GetPrivateProfileInt("scale","cmdport",8001,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
*vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
*dt=(DWORD)GetPrivateProfileInt("scale","dbgtime",5,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug VT220 Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ VT220 Auto �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("������� �������� �������:\t%lu ms\n", *cmdtimeout);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("�������� ������ ���������� ���:\t%lu ���.\n", *dt);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
return rc;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
	    {
    	    cc = i+1;
        	break;
	    }
return cc;
}

///
/// ### _������� �������������� ������ �� ������� WE21xx_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// INT16 _we_request(PCHAR ip,UINT16 wenum,DWORD cmd,WORD lst,long *w0,long *w1,long *w2, PBYTE mv, PBYTE zero)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|-------------------------------------------------------------------------------------
/// __ip__      | ����� ���������� ����������� MOXA/IDS-448
/// __wenum__   | ����� ������� WE21xx
/// __cmd__     | ����� UDP ����� ��� ������ ������ �� ������ WE21xx
/// __lst__     | ����� UDP ����� ��� ��������� ������ � ������� WE21xx
/// __w0__      | ��������� �� �������� ���������� ���� �� ����������
/// __w1__      | ��������� �� �������� ���� �� ��������� �1
/// __w2__      | ��������� �� �������� ���� �� ��������� �2
/// __mv__      | ��������� �� ���� motion
/// __zero__    | ��������� �� �������� ����� ��������� ��������� ������� We21xx
///
/// __����������__: ������������� �������� ��� ���������� ����� � �������� WE21xx � ������������� �������� � ������ ��������� ������ �� ������� WE21xx
///

INT16 _we_request(PCHAR ip,UINT16 wenum,DWORD cmd,WORD lst,DWORD tout,long *w0,long *w1,long *w2, PBYTE mv, PBYTE zero)
{
PCHAR _cmd,_answ;
INT16 cc=0,len,rc,slen,sign=1;

_cmd = (PCHAR) malloc_w(0x100);
_answ = (PCHAR) malloc_w(0x100);
if(*zero)
    {
    slen = wsprintf(_cmd,"S%02u;CDL;",wenum);
    for(rc=0; rc < 10;rc++)
        {
        UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);
        cc += (INT16) UDP_Receive_Timed(lst,3, (PVOID) _answ, tout,(PWORD) &len);
        if(_answ[0]==0x30)
			{
			*zero=0;
            break;
			}
        if(_answ[0]==0x32) // ��� ���������
            break;
        }
    }
#ifdef COF8
slen = wsprintf(_cmd,"S%02u;COF8;",wenum);
#else
slen = wsprintf(_cmd,"S%02u;COF9;",wenum);
#endif // COF8

UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);
// Moxa Force transmit 25 ms ����������
cc += (INT16) UDP_Receive_Timed(lst,3, (PVOID) _answ, tout,(PWORD) &len);
if(_answ[0]==0x30)
    {
    slen = wsprintf(_cmd,"MSV?;"); //COF3;MSV?;
    UDP_Send(ip,cmd, (PVOID) _cmd, (int) slen);
#ifdef COF8
        rc = (INT16) UDP_Receive_Timed(lst,6, (PVOID) _answ, tout,(PWORD) &len);
        if((rc > 0) && (len==6))
        {
            cc+=rc;
            if(_answ[0]==0xFF)
                sign=-1;
            _answ[0] = _answ[2];
            //rc = *((PINT16)_answ);
            *w0 = (long)(*((PINT16)_answ))*sign*10L;
            *w1 = *w0-((*w0)>>1);
            *w2 = *w0 - *w1;
            *mv = (((_answ[3] & 2) ^ 2)>0);
        }
#else
        rc = (INT16) UDP_Receive_Timed(lst,17, (PVOID) _answ, tout,(PWORD) &len);
        if((rc>0) && (_answ[8]==','))
            {
            cc+=rc;
            rc = _copy_str_until( _cmd,_answ,',');
            *w0 = _str2long((PBYTE)_cmd);
            *w1 = *w0-((*w0)>>1);
            *w2 = *w0 - *w1;
            rc = _copy_str_until( _cmd,_answ+12,'\n');
            rc= (INT16)_str2long((PBYTE)_cmd);
            *mv = ((rc&0x2)^2) > 0;
            }
#endif // COF8
    }
else
    cc = -1;
free_w(_answ);
free_w(_cmd);
return cc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    //DWORD isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ VT220
    int dataport;  // ���� ��� ��������� ������ � VT220
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� VT220 � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� VT220 ��� ������ �� �������
    DWORD dbgtime; // �������� ������ ���������� ����������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,ErrorLink=0;
    SYSTEMTIME lt;
    time_t isNow, isRelease;
    BYTE _mv,_z;
    UINT32 _cnt=0,_sum=0;
    FileMap fm;
	pWEGINFO wi;
	long value[3];
    PBYTE dataExchange;
    CHAR cfg[0x40];
	HANDLE isRun;
	BOOL isLock=FALSE,setZero;




if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug, &quit,&limit,&dbgtime,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Local\\We2110Req%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug WE2110 Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� We2110Req -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
wi=(pWEGINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(wi->ID!=num)
	InterlockedExchange16((SHORT volatile *) &wi->ID,(SHORT)num);

lstrcpy(wi->devName,"WE2110");

	cc=UDP_Init();
if(debug)
	printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
if(debug)
	printf("Udp Send Init:%d\n",cc);

    cc=UDP_OpenReceive();
if(debug)
	printf("Udp Reseive Init:%d\n",cc);

	InterlockedExchange16((SHORT volatile *) &wi->VT_Num,(WORD)vtnum);
	InterlockedExchange8((CHAR volatile *) &wi->Request,1);

_we_request(ipaddr,num,cmdport,dataport,timeout,&value[0],&value[1],&value[2], &_mv, &_z);
while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug,&quit,&limit,&dbgtime,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug WE2110 Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("MOXA �����:%s MOXA ���� ������:%u MOXA ��������� ����:%u\n",ipaddr,dataport,cmdport);
                }
                else
					_closeConsole();
            }
        }

        InterlockedExchange8((CHAR volatile *) &_z,wi->ZeroKey);
        setZero=(BOOL)(_z>0);
        if((cc = _we_request(ipaddr,num,cmdport,dataport,timeout,&value[0],&value[1],&value[2], &_mv, &_z))>0)
            {
            InterlockedExchange((volatile LONG *) &wi->WeightSUM,(long)value[0]);
			InterlockedExchange((volatile LONG *) &wi->Weight[0],(long)value[1]);
			InterlockedExchange((volatile LONG *) &wi->Weight[1],(long)value[2]);
			InterlockedExchange8((CHAR volatile *) &wi->Motion,_mv);
            InterlockedExchange16((SHORT volatile *) &wi->TimeVT,(WORD)cc);
			if((!_z) && setZero)
               InterlockedExchange8((CHAR volatile *) &wi->ZeroKey,0);
			ErrorLink=0;
			_sum+=cc;
			_cnt++;
            }
        else
            ErrorLink++;

            InterlockedExchange8((CHAR volatile *) &wi->isLink,(BYTE)(ErrorLink<10));

// ��������� ������� ��� ���� � 2 ���
		if(!wi->CarKey)
			{
			if(cmdtimeout>cc)
		        Sleep(cmdtimeout-cc);
			else
		        Sleep(cmdtimeout);
			}
		else{
            Sleep(2000UL);
            InterlockedExchange8((CHAR volatile *) &wi->CarKey,0);
		}

// ������ � ���������� ���� ���� ���������� ���� debug
        if(dbgtime && debug)
        {
        isNow=time(NULL);
		if(!(isNow % dbgtime))
            {
            if(!isLock)
                {
                    GetLocalTime(&lt);
                    printf("%02u:%02u:%02u | ����:%2u | ������:%2u | ���������:%05ld:%05ld:%05ld:%u | ������� ������: %d ms| ���-�� ������� � ���: %lu\n",lt.wHour,lt.wMinute,lt.wSecond,num,vtnum,value[0],value[1],value[2],_mv,_cnt ? (INT16)(_sum/_cnt): -1,_cnt/dbgtime);
                    isRelease=isNow;
                    _cnt=_sum=0;
                }
            }
        isLock = (BOOL) (isNow == isRelease);
        }
//_______________________________________________________

		if((long)wi->WeightSUM > (long)limit)
            InterlockedExchange8((CHAR volatile *) &wi->isBusy,1);
        else
            InterlockedExchange8((CHAR volatile *) &wi->isBusy,0);
		wsprintf((LPSTR)ReqString,".vtreq%02d",num);
    	if((GetFileAttributes(ReqString)!=INVALID_FILE_ATTRIBUTES))
        	{
	        DeleteFile((LPCTSTR)ReqString);
	        quit=1;
       		 }
		}


if(debug)
		_closeConsole();

	InterlockedExchange8((CHAR volatile *) &wi->isLink,0);
	cc=UDP_Close();
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
