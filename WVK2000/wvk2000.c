#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\udp2mat.h"
#include "..\inc\scale.h"
#include "..\inc\wvk_2000.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define VT_WEIGHT 1
#define VT_ZERO 2

CHAR ReqString[0x100];

int _isMove(long Summ, long limit, DWORD timeout)
{
static BOOL isFirst=TRUE;
static DWORD to;
static long Sm;
DWORD isNow;
BOOL Interval;
int cc=0;
if(isFirst)
	{
	to=GetTickCount();
	Sm=Summ;
	isFirst=FALSE;
	}
isNow =  GetTickCount()-to;
Interval = (isNow > timeout);
if(abs(Sm-Summ) > limit)
	{
	if(!Interval)
		cc=1;

	to=GetTickCount();
	Sm=Summ;
	}
return cc;
}

int _get_param(PCHAR ip_addr,int* cmdport,int* dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,BOOL* debug, BOOL* quit,PDWORD lim,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];
/*
int cmdport;    //���� ��� ������ ������ �� ������ WVK2000
int dataport;  // ���� ��� ��������� ������ � WVK2000
DWORD timeout; //������� �������� ������ � ����� � �������������
DWORD cmdtimeout; //������� ������ ������ �� WVK2000 � �������������
BYTE num;   //����� ������������ �����
BYTE vtnum; //����� ������� WVK2000 ��� ������ �� �������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
*cmdport=GetPrivateProfileInt("scale","cmdport",8001,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
*vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug WVK2000 Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ WVK2000 Auto �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("������� �������� �������:\t%lu ms\n", *cmdtimeout);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
return rc;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<36;i++)
        if(Buff[i]==chr)
	    {
    	    cc = i+1;
        	break;
	    }
return cc;
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD ttick,summ=0UL,isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ WVK2000
    int dataport;  // ���� ��� ��������� ������ � WVK2000
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� WVK2000 � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� WVK2000 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i=0,ErrorLink=0;
    FileMap fm;
	pWEGINFO wi;
    long weg_1,weg_2;
    PBYTE dataExchange;
    CHAR cfg[0x40];
	HANDLE isRun;
    ScaleCommand cScale;
    ScaleData dScale;
    DWORD timeReceive;

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug, &quit,&limit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Local\\WVK2000%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug WVK2000 Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� WVK2000 -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
wi=(pWEGINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(wi->ID!=num)
	InterlockedExchange16((volatile SHORT *) &wi->ID,(WORD)num);

	cc=UDP_Init();
if(debug)
	printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
if(debug)
	printf("Udp Send Init:%d\n",cc);

    cc=UDP_OpenReceive();
if(debug)
	printf("Udp Reseive Init:%d\n",cc);

	InterlockedExchange16((volatile SHORT *) &wi->VT_Num,(WORD)vtnum);
	InterlockedExchange8((volatile PCHAR) &wi->Request,1);

lstrcpy(wi->devName,"WVK2000");

cScale.uiTime=(unsigned long) time(NULL);
cScale.iCommand=CmdStopScale;
dScale.uiTime=0UL;
cc=UDP_Send(ipaddr,cmdport, (PVOID) &cScale, sizeof(cScale));
timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);

cScale.uiTime=(unsigned long) time(NULL);
cScale.iCommand=CmdSendZero;
dScale.uiTime=0UL;
cc=UDP_Send(ipaddr,cmdport, (PVOID) &cScale, sizeof(cScale));
timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);

cScale.uiTime=(unsigned long) time(NULL);
cScale.iCommand=CmdStartScale;
dScale.uiTime=0UL;
cc=UDP_Send(ipaddr,cmdport, (PVOID) &cScale, sizeof(cScale));
timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);


while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&debug,&quit,&limit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug WVK2000 Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("WVK2000 �����:%s WVK2000 ���� ������:%u WVK2000 ��������� ����:%u\n",ipaddr,dataport,cmdport);
                }
                else
					_closeConsole();
            }
        }

if(ErrorLink<6)
        ErrorLink++;

isNow=time(NULL);

if(!wi->ZeroKey){
    timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);
     if((timeReceive<timeout) && (!dScale.iErrorCode)){
               wi->WeightSUM=dScale.iBrutto;
               wi->Weight[0]=dScale.iBrutto_C[0];
               wi->Weight[1]=dScale.iBrutto_C[1];
               ErrorLink=0;
               wi->TimeVT=(WORD)timeReceive;
               weg_2=isNow-dScale.uiTime;
            if(debug)
                  printf("%lu|����:%02u|����:%03lu (%02li)|�������:%04lu ms|��:%06lu|�1:%05lu|�2:%05lu|��:%05lu|��:%05lu\n",
                         isNow,num,dScale.iState,weg_2,timeReceive+cmdtimeout,dScale.iBrutto,dScale.iBrutto_C[0],dScale.iBrutto_C[1],dScale.iBrutto_B[0],dScale.iBrutto_B[1]);
     }
     else
           printf("%lu|����:%02u|����:%03lu|�������:%04lu|Error:%li\n",
                         isNow,num,dScale.iState,timeReceive+cmdtimeout,dScale.iErrorCode);

}
    else{
        // Restart Scale--------------------------------------
                cScale.uiTime=(unsigned long) time(NULL);
                cScale.iCommand=CmdStopScale;
                dScale.uiTime=0UL;
                cc=UDP_Send(ipaddr,cmdport, (PVOID) &cScale, sizeof(cScale));
                timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);

                cScale.uiTime=(unsigned long) time(NULL);
                cScale.iCommand=CmdStartScale;
                dScale.uiTime=0UL;
                cc=UDP_Send(ipaddr,cmdport, (PVOID) &cScale, sizeof(cScale));
                timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);

                cScale.uiTime=(unsigned long) time(NULL);
                cScale.iCommand=CmdSendZero;
                dScale.uiTime=0UL;
                cc=UDP_Send(ipaddr,cmdport, (PVOID) &cScale, sizeof(cScale));
                timeReceive=UDP_Receive_Timed(dataport,sizeof(dScale),(PVOID)&dScale,timeout);
        // --------------------------------------------------
        if((!dScale.iErrorCode) && (dScale.uiTime))
                InterlockedExchange8((volatile CHAR *) &wi->ZeroKey,0);
        else{
            wi->TimeVT=(WORD)timeReceive+cmdtimeout;
            wi->SensError=(DWORD)dScale.iState;
        }
        printf("%lu|����:%02u|����:%03lu|�������:%04lu|Error:%li|ZeroKey:%u\n",
                         isNow,num,dScale.iState,timeReceive+cmdtimeout,dScale.iErrorCode,wi->ZeroKey);

    }

        Sleep(cmdtimeout);
		InterlockedExchange((volatile LONG *) &weg_1,wi->WeightSUM);
		InterlockedExchange8((volatile CHAR *) &wi->Motion,_isMove((long) weg_1, 10UL, 500UL));
		if(ErrorLink<5)
            InterlockedExchange8((volatile CHAR *) &wi->isLink,1);
        else
            InterlockedExchange8((volatile CHAR *) &wi->isLink,0);

		if((long)wi->WeightSUM > (long)limit)
            InterlockedExchange8((volatile CHAR *) &wi->isBusy,1);
        else
            InterlockedExchange8((volatile CHAR *) &wi->isBusy,0);

// ����� ����� �������� �������
	wsprintf((LPSTR)ReqString,".wvk%02d",num);
    if((GetFileAttributes(ReqString)!=INVALID_FILE_ATTRIBUTES))
        {
        DeleteFile((LPCTSTR)ReqString);
        quit=1;
        }

	}

if(!debug)
		_closeConsole();

	InterlockedExchange8((volatile CHAR *) &wi->isLink,0);
	InterlockedExchange8((volatile CHAR *) &wi->ZeroKey,0);
	cc=UDP_Close();
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
