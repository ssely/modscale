//---------------------------------------------------------------------------
#ifndef WVK2000ControllerInterfaceH
#define WVK2000ControllerInterfaceH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
namespace IWVK2000Controller {
  enum {
    //-------------------------------------------------------------------------
    // ����������� � �������
    //-------------------------------------------------------------------------
    VK2000_IO_STATWEIGH_START       = 201,
    // ��� ����� (Windows) --> ��2000 (��� ������� ����������� � �������)

    VK2000_IO_STATWEIGH_STOP        = 202,
    // ��� ����� (Windows) --> ��2000 (��� �������� ����������� � �������)

    VK2000_IO_STATWEIGH_DONE        = 203,
    // ��2000 --> ��� ����� (Windows) (��� ���������� ����������� � �������)

    VK2000_IO_STATWEIGH_ZERO        = 204,
    // ��� ����� (Windows) --> ��2000 (��� ������� ���������)

    VK2000_IO_STATWEIGH_ZERO_DONE   = 205,
    // ��2000 --> ��� ����� (Windows) (��� ���������� ���������)
    // TWZeroData

    VK2000_IO_STATWEIGH_RESET       = 206,
    // ��� ����� (Windows) --> ��2000 (��� ������ ����������� ����������� � �������)

    VK2000_IO_STATWEIGH_SAVE        = 207,
    // ��� ����� (Windows) --> ��2000 (��� ���������� ����������� ����������� � �������)
    // TMVanData

    VK2000_IO_STATWEIGH_INIT        = 208,
    // ��� ����� (Windows) --> ��2000 (��� ��������� ������ ��������, ��, �������, ��������� ��� ����������� � ������� ��� ��������� � �������)
    // ��������� ������������� � TWInitData

    VK2000_IO_STATWEIGH_PARAMS      = 209,
    // ��2000 --> ��� ����� (Windows) (��� ������� ����������� � ��������)
    // ������� ����������� � TWeighParams

    VK2000_IO_STATWEIGH_RESULT      = 210,
    // ��2000 --> ��� ����� (Windows) (� �������� ����������� � �������)
    // ��������� ����������� ������ � TVanWeighResult

    VK2000_IO_STATWEIGH_VANPARAMS   = 211,
    // ��� ����� (Windows) --> ��2000 (����� ������������ � �������)
    // ��������� ������ � TVanStatParams

    VK2000_IO_STATWEIGH_CLOSE       = 220,
    // ��� ����� (Windows) --> ��2000 (��� ���������� ������ ��2000 � ������ ����������� � �������)

    VK2000_IO_STATWEIGH_DISCR       = 230,
    // ��2000 --> ��� ����� (Windows)
    // �������� ���������� ������� TDiscrData

    //-------------------------------------------------------------------------
    // �����
    //-------------------------------------------------------------------------
    VK2000_IO_SYSTEM_STATUS         = 301,
    // �� --> ��� ����� (Windows) (��������� �����������)
    // wParam = 0  - ����������� �����������
    // wParam = 1  - ���������� ����������� (����������, ��������� ����������, ���������)
    // wParam = 2  - ����������� ����������
    // wParam = -1 - ������ ��� �����������

    VK2000_IO_WEIGH_STATUS          = 302,
    // ��2000 --> ��� ����� (Windows)
    // wParam - ��������� ���� TWeighStatus (����� 32 ����)

    VK2000_IO_DIAGN_MESSAGE         = 303,
    // ��2000 --> ��� ����� (Windows) (� �������� ����������� � �������� � � �������)
    // ��������� ��������� � TDiagnMessage

    VK2000_IO_RESTART_WEIGH_MODULE  = 304
    // ��� ����� (Windows) --> ��2000 ���������� ������ ����������� �� ��2000
  };

  // ���� ����������� (VK2000_IO_WEIGH_STATUS, ��������� TWeighStatus)
  enum {
    VK2000_WEIGH_PHASE_IDLE         = 100,        // ������� �������� �� �������� (�������� ���)
    VK2000_WEIGH_PHASE_NORM         = 101,        // ����������
    VK2000_WEIGH_PHASE_ZERO         = 102,        // ���������
    VK2000_WEIGH_PHASE_TEMP         = 103,        // ��������� ����������
    VK2000_WEIGH_PHASE_NORMTEMP     = 104,        // ���������� � ��������� ����������
    VK2000_WEIGH_PHASE_TEMPZERO     = 105,        // ��������� ���������� � ���������
    VK2000_WEIGH_PHASE_WEIGHING     = 106         // �����������
  };

  // ��� ������� VK2000_IO_STATWEIGH_INIT, VK2000_IO_STATWEIGH_ZERO
  typedef struct {                          // ��������� ������������� �����������
    int iWGroup;                            // ����� ������ �������� (��������, ...
                                            //                       (�������, ��� ������ ������ n iWGroup = n,..., iWUnit = 0, iSensor = 0)
    int iWUnit;                             // ����� �� (�������, ��� ������ �� n iWUnit = n,..., ��� ���� ������ = 0)
    int iSensor;                            // ����� ������� (�������, ��� ������ ������� n iSensor = n,..., ��� ���� ������ = 0)
    int iNObject;                           // ��� ������������� ������� (��������)
    int iLocPos;                            // ��������� ���������� (��������)
    int iSaveAfterStop;                     // ������� ���������� ���������� ����������� ����������� ����� ����� �������� (1)
  } TWInitData;

  // ��� ������� VK2000_IO_STATWEIGH_ZERO_DONE
  typedef struct {                          // ���� ��������
    int   StatZero [MAX_MCH];               // ������� ���� � �������
    int   InitZero [MAX_MCH];               // ��������� ���� � �������
  } TWZeroData;

  // ��� ������� VK2000_IO_STATWEIGH_PARAMS -- ���� ��� �� ������������
  typedef struct {                          // ������� �����������
    int                 iScales;            // ����� �����
    int                 iWGroup;            // ����� ������ ��������
    int                 iWUnit;             // ����� ��
    int                 iSensor;            // ����� �������

    float               faTemp [12];        // ����������� ��
    float               fP1;                // ����������� ���������
    float               fQ1;                // ����������� ���������
    float               fTbase1;            // ����������� ���������
    float               fP2;                // ����������� ���������
    float               fQ2;                // ����������� ���������
    float               fTbase2;            // ����������� ���������
  } TWeighParams;

  // ��� ������� VK2000_IO_STATWEIGH_RESULT
  typedef struct {                                // ���������� ����������� ������
    unsigned int        uiTrainBWtime;            // Unix-����� ������ ����������� ������

    int                 iLocCount;                // ������� �����������
    int                 iVanCount;                // ������� �������

    int                 iObjectNum;               // � ������� � ������� �������
    int                 iObjectCode;              // ��� ������� � ������� �������

    unsigned int        uiBWtime;                 // Unix-����� ������ ����������� ������
    unsigned int        uiEWtime;                 // Unix-����� ��������� ����������� ������
    //char                caBdatetime [20];       // ����� ������ �����������
    //char                caEdatetime [20];       // ����� ��������� �����������

    unsigned int        uiBrutto;                 // ������ (��)
    unsigned int        uiBrutto_b1;              // ������, ������� ����, ��
    unsigned int        uiBrutto_b2;              // ������, ������� ����, ��
    unsigned int        uiBrutto_c1;              // ������, ������� 1 (��)
    unsigned int        uiBrutto_c2;              // ������, ������� 2 (��)
    unsigned int        uiBrutto_Ax [NAX8];       // ������ �� ��� (���� ���� ������ ������) (��) (NAX8 = 8)
    unsigned int        uiBrutto_Wheels [NAX8 * 2];// ������ �� ������ (��)
    float               fMass;                    // ����� ��� �������������

    float               fVelocity;                // ��������, > 0 - �������� ������, < 0 - �������� ����� (��/�)
    float               fAcceleration;            // ���������� (�/�2)

    float               faTemp [12];              // ����������� ��

    unsigned int        uiNaxis;                  // ����� ����
    unsigned int        uiStatus;                 // ������
  } TVanWeighResult;

  // ��� ������ VK2000_IO_WEIGH_STATUS
  typedef struct {                          // ��������� �����������
    unsigned int uiPhase;                   // ���� �����������
                                            // 101 - ����������
                                            // 102 - ���������
                                            // 103 - ��������� ����������
                                            // 104 - ���������� � ��������� ����������
                                            // 105 - ��������� ���������� � ���������
                                            // 106 - �����������

  } TWeighStatus;

  // ��� ������ VK2000_IO_STATWEIGH_VANPARAMS
  typedef struct {                          // ��������� ����������� � �������
    char caInvNum [9];                      // ����� ������
    int iTare;                              // ����� ���� ������ (��)
    int iTareIndex;                         // ��� ���� (0 - �� ���������, 1 - ��������, 2 - �������)
    int iCarrying;                          // ���������������� (��)
    int iLoadNorm;                          // ����� �������� (��)
    int iHBase;                             // ������� ���� ������ (��)
  } TVanStatParams;

  // ��� ������ VK2000_IO_DIAGN_MESSAGE
  typedef struct {                          // ��������������� ���������
    int  iCode;                             // ���
    char caMessage [1024];                  // �����
  } TDiagnMessage;                          //
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif


